<?php

namespace backend\controllers;

use backend\models\RepAdv2Search;
use backend\models\RepAdvSummary;
use backend\models\VcsAdvertisement;
use Yii;
use backend\models\RepAdv;
use backend\models\RepAdvSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



/**
 * RepAdvController implements the CRUD actions for RepAdv model.
 */
class RepAdvController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RepAdv models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'export') {
            return $this->exportExcel(Yii::$app->request->queryParams);
        }

        $searchModel = new RepAdvSearch();
        $searchModel->report_date = date('d/m/Y', strtotime("-1days")). ' - '. date('d/m/Y');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Summary
//        $summaryModel = new RepAdvSummary();
//        $summaryModel->report_date = date('d/m/Y', strtotime("-1days")). ' - '. date('d/m/Y');
//
//        $summaryProvider = $summaryModel->search(Yii::$app->request->queryParams);
//        $summaryResults = $summaryProvider->query->asArray()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'summaryResults' => $summaryResults,
        ]);
    }

    public function actionList()
    {
        $searchModel = new RepAdv2Search();
        $searchModel->report_date = date('d/m/Y', strtotime("-30days")). ' - '. date('d/m/Y');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RepAdv model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new RepAdv model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RepAdv();
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa
        $imageFieldName = 'image_path';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);
        if ($model->load($form_values) && $model->save()) {
            $images = Slim::getImages($imageFieldName);

            if($images != false && sizeof($images) > 0) {

                $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);

                $model->$imageFieldName = $imagePath;
                $model->save();
            }

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing RepAdv model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $imageFieldName = 'image_path';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);

        if ($model->load($form_values) && $model->save()) {

            if ($form_values['change_'. $imageFieldName] == '1') {
                $oldImagePath = ($model->$imageFieldName)? Yii::getAlias('@backend_web'). DIRECTORY_SEPARATOR. $model->$imageFieldName: null;
                $images = Slim::getImages($imageFieldName);

                if($images != false && sizeof($images) > 0) {
                    $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);
                    $oldAltPath = $model->$imageFieldName;

                    $model->$imageFieldName = $imagePath;
                    $model->save();

                    if ($oldAltPath != $imagePath) {
                        // Xoa file anh cu
                        if ($oldImagePath && file_exists($oldImagePath))
                            unlink($oldImagePath);

                    }
                } else {
                    $model->$imageFieldName = null;

                    $model->save();
                    // Xoa file anh cu
                    if ($oldImagePath && file_exists($oldImagePath))
                        unlink($oldImagePath);
                }
            }

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RepAdv model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->delete();

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the RepAdv model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RepAdv the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RepAdv::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function exportExcel($queryParams) {

        if (!strpos($queryParams['report_date'], ' - ')) {
            return $this->redirect(['index']);
        }

        $request_times = \common\helpers\Helpers::splitDate($queryParams['report_date'], 'd/m/Y');

        $result = RepAdv::find()
            ->select('adv_id, SUM(total_watching_time) total_watching_time, SUM(total_view) total_view, SUM(total_click) total_click')
            ->andWhere(['BETWEEN', 'report_date', $request_times[0], $request_times[1] ])
            ->andFilterWhere([
                'video_id' => (isset($queryParams['video_id']) && $queryParams['video_id'])? $queryParams['video_id']: null,
                'adv_id' => (isset($queryParams['adv_id']) && $queryParams['adv_id'])? $queryParams['adv_id']: null,

            ])
            ->groupBy('adv_id')
            ->orderBy('total_view DESC')
            ->asArray()
            ->all()
        ;


        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $headers = [
            Yii::t('backend', 'Adv ID'),
            Yii::t('backend', 'Adv'),
            Yii::t('backend', 'Total watching time'),
            Yii::t('backend', 'Total view'),
            Yii::t('backend', 'Total click'),
        ];

        $activeSheet->mergeCells("B1:F1");
        $activeSheet->setCellValue('B1', Yii::t('backend', 'ADV ANALYSIS'));

        $activeSheet->setCellValue('B3', Yii::t('backend', 'Report date'));
        $activeSheet->setCellValue('C3', $queryParams['report_date']);

        $activeSheet->setCellValue('B4', Yii::t('backend', 'Total rows'));
        $activeSheet->setCellValue('C4', "". count($result));

        $activeSheet
            ->fromArray(
                $headers,  // The data to set
                NULL,        // Array values with this value will not be set
                'B6'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
            );
        $activeSheet->getRowDimension('1')->setRowHeight(30);
        $activeSheet->getStyle('B1')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 16
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ]
        ]);

        $activeSheet->getStyle('B6:F6')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 11
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ]
        ]);

        $r = 7;
        if (count($result)) {
            foreach ($result as $item) {
                $activeSheet->setCellValue('B'. strval($r), $item['adv_id']);
                $adv = VcsAdvertisement::findOne($item['adv_id']);
                $activeSheet->setCellValue('C' . strval($r), ($adv)? $adv->title: "");

                $activeSheet->setCellValue('D'. strval($r), $item['total_watching_time']);
                $activeSheet->setCellValue('E'. strval($r), $item['total_view']);
                $activeSheet->setCellValue('F'. strval($r), $item['total_click']);
                $r ++;
            }

            $activeSheet->getColumnDimension('B')->setAutoSize(true);
            $activeSheet->getColumnDimension('C')->setAutoSize(true);
            $activeSheet->getColumnDimension('D')->setAutoSize(true);
            $activeSheet->getColumnDimension('E')->setAutoSize(true);
            $activeSheet->getColumnDimension('F')->setAutoSize(true);

        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,

                ],
            ],
        ];

        $activeSheet->getStyle('B6:'.
            $activeSheet->getHighestColumn() .
            $activeSheet->getHighestRow()
        )->applyFromArray($styleArray);

        $fileName = 'adv-analysis.xlsx';
        $writer = new Xlsx($spreadsheet);
//        header('Content-Type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment;filename="adv-analysis' . '.xlsx"');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        ob_end_clean();
        $writer->save('php://output');
        die;
        //return $this->redirect('index', 200);

        /*
        foreach (range('A', 'E') as $columnID) {
            $activeSheet->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        //set header
        $activeSheet->setCellValue('A1', 'ADV REPORT');
        $activeSheet->mergeCells('A1:F1');
        $activeSheet->getRowDimension('1')->setRowHeight(30);
        $activeSheet->getStyle('A1')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 16
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ]);


        $activeSheet->getStyle('B2:B5')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 11
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ]);

        $activeSheet->setCellValue('B2', Yii::t('backend', 'Report date:'));
        $activeSheet->setCellValue('C2', $queryParams['report_date']);

        $activeSheet->setCellValue('B3', Yii::t('backend', 'Total items'));
        $activeSheet->setCellValue('C3', "". count($result));

        //set table header
        $activeSheet->setCellValue('A6', 'No');
        $activeSheet->setCellValue('B6', Yii::t('backend', 'Adv ID'));
        $activeSheet->setCellValue('C6', Yii::t('backend', 'Adv'));
        $activeSheet->setCellValue('D6', Yii::t('backend', 'Total watching time'));
        $activeSheet->setCellValue('E6', Yii::t('backend', 'Total view'));
        $activeSheet->setCellValue('F6', Yii::t('backend', 'Total click'));

        $activeSheet->getStyle('A6:F6')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 11
            ],
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ]
        ]);

        //set data
        $r = 7;

        foreach ($result as $item) {

            $activeSheet->SetCellValue('A' . strval($r), strval( ($r - 6)));
            $activeSheet->SetCellValue('B' . strval($r), strval($item['adv_id']));
            $adv = VcsAdvertisement::findOne($item['adv_id']);
            $activeSheet->SetCellValue('C' . strval($r), ($adv)? $adv->title: "");
            $activeSheet->setCellValueExplicit('D' . strval($r), ($item['total_watching_time']), PHPExcel_Cell_DataType::TYPE_NUMERIC)->getStyle('D' . strval($r))->getNumberFormat()->setFormatCode('#,##');
            $activeSheet->setCellValueExplicit('E' . strval($r), ($item['total_view']), PHPExcel_Cell_DataType::TYPE_NUMERIC)->getStyle('E' . strval($r))->getNumberFormat()->setFormatCode('#,##');
            $activeSheet->setCellValueExplicit('F' . strval($r), ($item['total_click']), PHPExcel_Cell_DataType::TYPE_NUMERIC)->getStyle('F' . strval($r))->getNumberFormat()->setFormatCode('#,##');

            $r++;
        }

        $activeSheet->getColumnDimension('B')->setAutoSize(true);
        $activeSheet->getColumnDimension('C')->setAutoSize(true);
        $activeSheet->getColumnDimension('D')->setAutoSize(true);
        $activeSheet->getColumnDimension('E')->setAutoSize(true);
        $activeSheet->getColumnDimension('F')->setAutoSize(true);

        $activeSheet->getStyle(
            'A6:' .
            $activeSheet->getHighestColumn() .
            $activeSheet->getHighestRow()
        )->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);


        //out file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="adv-analysis' . '.xlsx"');

        $objWriter->save('php://output');
        return $this->redirect('index', 200);
        */
    }
}
