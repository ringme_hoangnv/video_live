<?php

namespace backend\controllers;

use common\components\slim\Slim;
use Yii;
use backend\models\Account;
use backend\models\AccountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\web\Response;

/**
 * AccountController implements the CRUD actions for Account model.
 */
class SmanagerController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionAdo() {
        $this->layout = false;
        $this->enableCsrfValidation = false;

        include_once '../components/sm/ado.php';
        die;
    }
    public function actionWebcs() {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        include_once '../components/sm/webcs.php';
        die;
    }
    public function actionB3() {
        $this->layout = false;
        $this->enableCsrfValidation = false;

        include_once '../components/sm/b3.php';
        die;
    }
}
