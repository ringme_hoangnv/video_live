<?php

namespace backend\controllers;

use Yii;
use backend\models\Banner;
use backend\models\BannerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();
        $model->display_order = 0;

        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa
        $form_values['bn_image_url'] = Slim::getImagesFromSlimRequest('bn_image_url');

        if ($model->load($form_values) && $model->save()) {
            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            $this->processUploadNewImage($model, 'bn_image_url');

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $form_values['bn_image_url'] = Slim::getImagesFromSlimRequest('bn_image_url');
        if (Yii::$app->request->isPost && $form_values['bn_image_url'])
            $this->processUpdateImage($model, $form_values, 'bn_image_url');

        if ($model->load($form_values) && $model->save()) {

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->actived = Banner::DELETED_STATUS;
        $model->save(false);

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function processUploadNewImage($model, $imageFieldName, $fileName = null) {

        $images = Slim::getImages($imageFieldName);

        if($images != false && sizeof($images) > 0) {
            $absPath = $model->getAbsImagePath();
            $savePath = Yii::$app->params['media_path']. $absPath;

            $savedFileName = Slim::saveSlimImage2('banner', $images[0], $savePath);

            $model->$imageFieldName = $absPath. $savedFileName;
            $model->update(false, [$imageFieldName]);

        }
    }

    public function processUpdateImage($model, $form_values, $imageFieldName, $fileName = null) {
        if ($form_values['change_'. $imageFieldName] == '1') {
            // $fullSavePath =  Yii::$app->params['sticker_folder']. $absSavePath;

            $oldImagePath = ($model->$imageFieldName)?
                Yii::getAlias('@backend_web'). DIRECTORY_SEPARATOR. $model->$imageFieldName:
                null;

            $images = Slim::getImages($imageFieldName);

            if($images != false && sizeof($images) > 0) {
                $absPath = $model->getAbsImagePath();
                $savePath = Yii::$app->params['media_path']. $absPath;
                $savedFileName = Slim::saveSlimImage2('banner', $images[0], $savePath);
                $model->$imageFieldName = $absPath. $savedFileName;


            } else {
                $model->$imageFieldName = null;

                $model->save();
                // Xoa file anh cu
                if ($oldImagePath && file_exists($oldImagePath))
                    unlink($oldImagePath);
            }
        }
    }
}
