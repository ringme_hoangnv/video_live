<?php

namespace backend\controllers;

use common\core\SuperAppApiGw;
use common\models\VcsChannelBase;
use yii;
use backend\models\VideoItem;
use backend\models\VideoItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use yii\web\UploadedFile;

/**
 * VideoItemController implements the CRUD actions for VideoItem model.
 */
class VideoItemController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VideoItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        //VcsChannelBase::updateVideoNums([101, 102, 108, 109]);
        $searchModel = new VideoItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApproval()
    {
        $searchModel = new VideoItemSearch();
        $searchModel->actived = VideoItem::WAIT_APPROVAL_STATUS;
        $searchModel->showStatusFilter = false;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('waitApprove', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionListDeleted()
    {
        $searchModel = new VideoItemSearch();
        $searchModel->actived = VideoItem::DELETED_STATUS;
        $searchModel->showStatusFilter = false;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VideoItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new VideoItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VideoItem();
        $model->total_likes = 0;
        $model->total_views = 0;
        $model->total_comments = 0;
        $model->total_shares = 0;
        $model->is_adaptive = null;
        $model->encode_type = null;
        $model->actived = VideoItem::WAIT_CONVERT_STATUS;
        $model->belong = 'cms';

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa
        $imageFieldName = 'video_image';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName); //lay du lieu anh

        if ($model->load($form_values)) {
            $model->video_media = $videoMedia = UploadedFile::getInstance($model, 'video_media'); //lay du lieu video
            if ($form_values[$imageFieldName] && Yii::$app->request->isPost) {
                $model->video_image = 'processing';
            }
            if ($model->save())
            {
                Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', 'Video')]));

                $this->processUploadNewImage($model, $imageFieldName);

                if ($model->uploadVideoMedia()) {
                    // upload thanh cong, goi api convert
                    SuperAppApiGw::convertVideo($model->id);
                }

                // update cache
                SuperAppApiGw::updateCache('create', 'video', $model->id);

                // bo sung redirect
                if (Yii::$app->request->post('save_and_back')) {
                    return $this->redirect(['index']);
                } elseif (Yii::$app->request->post('save_and_add')) {
                    return $this->redirect(['create']);
                } else {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }



        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing VideoItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $imageFieldName = 'video_image';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);

        if (Yii::$app->request->isPost && $form_values[$imageFieldName])
            $this->processUpdateImage($model, $form_values, $imageFieldName);

        if ($model->load($form_values)) {
            if (!$model->video_media) {
                $model->video_media = $model->getOldAttribute('video_media');
            }
            if ($model->save()) {
                $model->video_media = UploadedFile::getInstance($model, 'video_media');
                if ($model->video_media) {

                    if ($model->uploadVideoMedia()) {
                        // upload thanh cong, goi api convert
                        SuperAppApiGw::convertVideo($model->id);
                    }
                }

                Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

                // update cache
                SuperAppApiGw::updateCache('update', 'video', $model->id);

                return $this->redirect(['update', 'id' => $model->id]);
            }
        }  else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VideoItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->actived = VideoItem::DELETED_STATUS;
        $model->save(false);

        VcsChannelBase::updateVideoNums([$model->channel_id]);

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        // update cache
        SuperAppApiGw::updateCache('delete', 'video', $model->id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the VideoItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VideoItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VideoItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function processUploadNewImage($model, $imageFieldName, $fileName = null) {

        $images = Slim::getImages($imageFieldName);
        $absPath = $model->getAbsVideoImagePath();
        $savePath = Yii::$app->params['media_path']. $absPath;

        if($images != false && sizeof($images) > 0) {

            $savedFileName = Slim::saveSlimImage2('video', $images[0], $savePath);

            $model->$imageFieldName = $absPath. $savedFileName;
            $model->update(false, [$imageFieldName]);
        }
    }

    public function processUpdateImage($model, $form_values, $imageFieldName, $fileName = null) {
        if ($form_values['change_'. $imageFieldName] == '1') {


            $oldImagePath = ($model->$imageFieldName)?
                Yii::$app->params['media_path']. $model->$imageFieldName:
                null;

            $images = Slim::getImages($imageFieldName);

            if($images != false && sizeof($images) > 0) {
                $absPath = $model->getAbsVideoImagePath();
                $savePath = Yii::$app->params['media_path']. $absPath;
                $savedFileName = Slim::saveSlimImage2('video', $images[0], $savePath);

                $model->$imageFieldName = $absPath. $savedFileName;
                //$model->update(false, [$imageFieldName]);

            } else {
                $model->$imageFieldName = null;

                //$model->update(false, [$imageFieldName]);
                // Xoa file anh cu
                if ($oldImagePath && file_exists($oldImagePath))
                    unlink($oldImagePath);
            }
        }
    }

    public function actionBulk(){

        $selection=(array)Yii::$app->request->post('selection'); //typecasting
        $updateIds = $selection;

        $channelIds = VideoItem::find()
            ->select('channel_id')
            ->where(['id' => $selection])
            ->asArray()
            ->all();
        $channelIds = yii\helpers\ArrayHelper::map($channelIds, 'channel_id', 'channel_id');

        if (Yii::$app->request->post('bulk_delete')) {
            $count = VideoItem::updateAll(['actived' => VideoItem::DELETED_STATUS ], [
                'id' => $selection
            ]);
            if ($count) {
                VcsChannelBase::updateVideoNums($channelIds);
                Yii::$app->session->setFlash('info', Yii::t('backend', 'Delete {total} videos successfully!', [
                    'total' => $count
                ]));

                // update cache
                SuperAppApiGw::updateCache('delete', 'video', $updateIds);
            }
        } elseif (Yii::$app->request->post('bulk_active')) {

            // Lay ra cac video du dieu kien
            $videos = VideoItem::find()
                ->alias('v')
                ->joinWith('category ca')
                ->joinWith('channel ch')
                ->where([
                    'v.id' => $selection,
                    'v.actived' => VideoItem::WAIT_APPROVAL_STATUS,
                    'ca.actived' => [1, 2, 3],
                    'ch.actived' => [10, 11, 12],
                ])
                ->all();

            if (count($videos)) {
                $videoIds = [];
                foreach ($videos as $video) {
                    $videoIds[] = $video->id;
                }

                $count = VideoItem::updateAll([
                    'actived' => VideoItem::ACTIVED_STATUS,
                    'publish_time' => date('Y-m-d H:i:s'),
                ], [
                    'id' => $videoIds,
                    'actived' => VideoItem::WAIT_APPROVAL_STATUS,

                ]);

                if ($count) {
                    // count lai video cua channel
                    VcsChannelBase::updateVideoNums($channelIds);
                    
                    Yii::$app->session->setFlash('info', Yii::t('backend', 'Approve {total} videos successfully!', [
                        'total' => $count
                    ]));

                    if ($count < count($selection)) {
                        // lay cac ban ghi vua duoc update
                        $updateIds = VideoItem::find()
                            ->where([
                                'actived' => VideoItem::ACTIVED_STATUS,
                                'id' => $selection,
                            ])
                            ->select(['id'])
                            ->column();
                    }
                    // update cache
                    SuperAppApiGw::updateCache('update', 'video', $updateIds);
                }
            }
        }

        return $this->redirect(['video-item/index']);
    }

    /**
     * Xu ly duyet video
     * @param $id
     * @param $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionApprove($id, $status){

        $model = $this->findModel($id);
        // check status hop le
        if ($model->actived != VideoItem::WAIT_APPROVAL_STATUS || !in_array($status, [10, 11, 12])) {
            Yii::$app->session->setFlash('info', Yii::t('backend', 'Cannot approve this video!'));
            return $this->goBack();
        }
        // check dk channel, category
        /**
         * Chỉ duyệt video có actived=9
        Khi duyệt video check lại trạng thái của cate và channle
        cactetory = 1,2,3
        channel=10,11,12
         */
        $videoCate = $model->category;

        if (!in_array($videoCate->actived, [2, 3])) {
            Yii::$app->session->setFlash('warning', Yii::t('backend', 'Category of video has not approved yet!'));
            return $this->redirect(['index']);
        }

        $videoChannel = $model->channel;
        if (!in_array($videoChannel->actived, [10, 11, 12])) {
            Yii::$app->session->setFlash('warning', Yii::t('backend', 'Channel of video has not approved yet!'));
            return $this->redirect(['index']);
        }


        $model->actived = $status;
        $model->publish_time = date('Y-m-d H:i:s');
        if ($model->first_publish_time == null) {
            $model->first_publish_time = date('Y-m-d H:i:s');
        }
        $model->save(false);

        // Cap nhat video_nums cua channel
        VcsChannelBase::updateVideoNums([$model->channel_id]);

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Approve video successfully!'));

        // update cache
        SuperAppApiGw::updateCache('update', 'video', $model->id);

        return $this->redirect(['index']);
    }
}
