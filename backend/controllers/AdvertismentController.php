<?php

namespace backend\controllers;

use Yii;
use common\components\slim\Slim;
use backend\models\Advertisment;
use backend\models\AdvertismentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * AdvertisementController implements the CRUD actions for Advertisment model.
 *
 * HEELOOOO
 */
class AdvertismentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Advertisment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdvertismentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advertisment model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Advertisment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Advertisment();
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        $imageFieldName = 'image_path';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);

        if ($model->load($form_values) && $model->save()) {
            $images = Slim::getImages($imageFieldName);
            if($images != false && sizeof($images) > 0) {

                $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);

                $model->$imageFieldName = $imagePath;
                $model->save();
            }

            Yii::$app->session->setFlash('success', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', 'Category')]));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Advertisment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        $imageFieldName = 'image_path';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);

        if ($model->load($form_values) && $model->save()) {

            if ($form_values['change_'. $imageFieldName] == '1') {

                $oldImagePath = Yii::getAlias('@backend_web'). DIRECTORY_SEPARATOR. $model->$imageFieldName;
                $images = Slim::getImages($imageFieldName);

                if($images != false && sizeof($images) > 0) {

                    $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);
                    $oldAltPath = $model->$imageFieldName;

                    $model->$imageFieldName = $imagePath;
                    $model->save();

                    if ($oldAltPath != $imagePath) {
                        // Xoa file anh cu
                        if ($model->$imageFieldName && file_exists($oldImagePath))
                            unlink($oldImagePath);
                    }
                } else {

                    $model->$imageFieldName = null;
                    $model->save();
                    // Xoa file anh cu
                    if ($model->$imageFieldName && file_exists($oldImagePath))
                        unlink($oldImagePath);
                }
            }


            Yii::$app->session->setFlash('success', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Advertisment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->delete();

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Advertisment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Advertisment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advertisment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
