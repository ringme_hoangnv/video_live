<?php

namespace backend\controllers;

use backend\models\RepVideo2Search;
use backend\models\RepVideoSummary;
use backend\models\VideoItem;
use Yii;
use backend\models\RepVideo;
use backend\models\RepVideoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * RepVideoController implements the CRUD actions for RepVideo model.
 */
class RepVideoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RepVideo models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (isset($_GET['action']) && $_GET['action'] == 'export') {
            return $this->exportExcel(Yii::$app->request->queryParams);
        }

        $searchModel = new RepVideoSearch();
        $searchModel->report_date = date('d/m/Y', strtotime("-1days")). ' - '. date('d/m/Y');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // Summary
//        $summaryModel = new RepVideoSummary();
//        $summaryModel->report_date = date('d/m/Y', strtotime("-30days")). ' - '. date('d/m/Y');
//
//        $summaryProvider = $summaryModel->search(Yii::$app->request->queryParams);
//        $summaryResults = $summaryProvider->query->asArray()->all();


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'summaryResults' => $summaryResults,
        ]);
    }

    public function actionList()
    {
        $searchModel = new RepVideo2Search();
        $searchModel->report_date = date('d/m/Y', strtotime("-30days")). ' - '. date('d/m/Y');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RepVideo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Finds the RepVideo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RepVideo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RepVideo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function exportExcel($queryParams) {

        if (!strpos($queryParams['report_date'], ' - ')) {
            return $this->redirect(['index']);
        }

        $request_times = \common\helpers\Helpers::splitDate($queryParams['report_date'], 'd/m/Y');

        $result = RepVideo::find()
            ->select('video_id, SUM(total_view) total_view, SUM(total_like) total_like, SUM(total_follow) total_follow, SUM(total_unfollow) total_unfollow, SUM(total_comment) total_comment, SUM(total_like_comment) total_like_comment, sum(total_watching_time) total_watching_time, sum(total_play) total_play, sum(total_watching_time)/sum(total_play) average_watching_time')
            ->andWhere(['BETWEEN', 'report_date', $request_times[0], $request_times[1] ])
            ->andFilterWhere([
                'video_id' => (isset($queryParams['video_id']) && $queryParams['video_id'])? $queryParams['video_id']: null,

            ])
            ->groupBy('video_id')
            ->orderBy('total_watching_time DESC')
            ->asArray()
            ->all()
        ;


        $spreadsheet = new Spreadsheet();
        $activeSheet = $spreadsheet->getActiveSheet();
        $headers = [
            Yii::t('backend', 'Video ID'),
            Yii::t('backend', 'Video'),
            //Yii::t('backend', 'Total watching time'),
            Yii::t('backend', 'Total view'),
            Yii::t('backend', 'Total like'),
//            Yii::t('backend', 'Total follow'),
//            Yii::t('backend', 'Total unfollow'),
            Yii::t('backend', 'Total comment'),
//            Yii::t('backend', 'Total like comment'),
            Yii::t('backend', 'Average watching time'),
        ];

        $activeSheet->mergeCells("B1:G1");
        $activeSheet->setCellValue('B1', Yii::t('backend', 'VIDEO ANALYSIS'));

        $activeSheet->setCellValue('B3', Yii::t('backend', 'Report date'));
        $activeSheet->setCellValue('C3', $queryParams['report_date']);

        $activeSheet->setCellValue('B4', Yii::t('backend', 'Total rows'));
        $activeSheet->setCellValue('C4', "". count($result));

        $activeSheet
            ->fromArray(
                $headers,  // The data to set
                NULL,        // Array values with this value will not be set
                'B6'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
            );
        $activeSheet->getRowDimension('1')->setRowHeight(30);
        $activeSheet->getStyle('B1')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 16
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ]
        ]);

        $activeSheet->getStyle('B6:G6')->applyFromArray([
            'font' => [
                'bold' => true, 'size' => 11
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ]
        ]);

        $r = 7;
        if (count($result)) {
            foreach ($result as $item) {
                $activeSheet->setCellValue('B'. strval($r), $item['video_id']);
                $video = VideoItem::findOne($item['video_id']);
                $activeSheet->setCellValue('C' . strval($r), ($video)? $video->video_title: "");

                $activeSheet->setCellValue('D'. strval($r), $item['total_view']);
                $activeSheet->setCellValue('E'. strval($r), $item['total_like']);
                $activeSheet->setCellValue('F'. strval($r), $item['total_comment']);
                $activeSheet->setCellValue('G'. strval($r), $item['average_watching_time']);
                $r ++;
            }

            $activeSheet->getColumnDimension('B')->setAutoSize(true);
            $activeSheet->getColumnDimension('C')->setAutoSize(true);
            $activeSheet->getColumnDimension('D')->setAutoSize(true);
            $activeSheet->getColumnDimension('E')->setAutoSize(true);
            $activeSheet->getColumnDimension('F')->setAutoSize(true);
            $activeSheet->getColumnDimension('G')->setAutoSize(true);

        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,

                ],
            ],
        ];

        $activeSheet->getStyle('B6:'.
            $activeSheet->getHighestColumn() .
            $activeSheet->getHighestRow()
        )->applyFromArray($styleArray);

        $fileName = 'video-analytic.xlsx';
        $writer = new Xlsx($spreadsheet);
//        header('Content-Type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment;filename="adv-analysis' . '.xlsx"');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        ob_end_clean();
        $writer->save('php://output');
        die;

    }
}
