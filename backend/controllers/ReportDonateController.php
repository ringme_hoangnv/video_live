<?php

namespace backend\controllers;

use app\models\TblReportDay;
use app\models\TblReportDaySearch;
use app\models\TblStarWeekStreamer;
use backend\components\common\MenuHelper;
use backend\models\Menu;
use backend\models\MenuSearch;
use backend\models\VcsChannel;
use common\models\TblLivestream;
use common\models\TblLivestreamSearch;
use common\models\TblTransactionStar;
use common\models\TblTransactionStarSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MenuController implements the CRUD actions for Menu model.
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class ReportDonateController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex(){
//        public function actionIndex($time = null){
//        $searchModelRpDay = new TblReportDaySearch();
//        $searchModelTransaction = new TblTransactionStarSearch();
//        $dataProvider = $searchModelRpDay->search(Yii::$app->request->queryParams,$time);
//        $dataProvider->pagination = ['pageSize' => 8];
//        $dataProviderTransaction = $searchModelTransaction->search(Yii::$app->request->queryParams,null);
//        $dataProviderTransaction->pagination = ['pageSize' => 8];
//        return $this->render('index', [
//            'dataProvider' => $dataProvider,
//            'dataProvider1' => $dataProviderTransaction,
//        ]);
        $searchModel = new TblReportDaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

//    public function actionQueryTime() {
//        if (Yii::$app->request->isAjax) {
//            $data = Yii::$app->request->post();
//            $time = $data['time'];
//            $time = $time[0];
//            $model = $this->findModel($stream_id);
//            $model->status = $status;
//            if($model->save()){
//                return true;
//            }
//            return false;
//
//        }
//    }
    public function actionQueryTime(){
//        public function actionQueryTime($time){
//        $searchModelRpDay = new TblReportDaySearch();
//        $searchModelTransaction = new TblTransactionStarSearch();
//        $dataProvider = $searchModelRpDay->search(Yii::$app->request->queryParams);
//        $dataProvider->pagination = ['pageSize' => 8];
//        $dataProviderTransaction = $searchModelTransaction->search(Yii::$app->request->queryParams);
//        $dataProviderTransaction->pagination = ['pageSize' => 8];
//        return $this->render('index', [
//            'dataProvider' => $dataProvider,
//            'dataProvider1' => $dataProviderTransaction,
//        ]);
        $searchModel = new TblTransactionStarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
}