<?php

namespace backend\controllers;

use common\core\SuperAppApiGw;
use Yii;
use backend\models\VcsChannel;
use backend\models\VcsChannelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;

/**
 * VcsChannelController implements the CRUD actions for VcsChannel model.
 */
class VcsChannelController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VcsChannel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VcsChannelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApproval()
    {
        $searchModel = new VcsChannelSearch();
        $searchModel->actived = VcsChannelSearch::WAIT_APPROVAL_STATUS;
        $searchModel->showStatusFilter = false;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('waitApprove', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionListDeleted()
    {
        $searchModel = new VcsChannelSearch();
        $searchModel->actived = VcsChannelSearch::DELETED_STATUS;
        $searchModel->showStatusFilter = false;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('listDeleted', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VcsChannel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new VcsChannel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VcsChannel();
        $model->actived = 0;
        $model->num_follows = 0;
        $model->num_videos = 0;
        $model->actived = VcsChannel::WAIT_APPROVAL_STATUS;

        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa
        $form_values['channel_avatar'] = Slim::getImagesFromSlimRequest('channel_avatar');
        $form_values['header_banner'] = Slim::getImagesFromSlimRequest('header_banner');

        if ($model->load($form_values) && $model->save()) {

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', 'channel')]));

            $this->processUploadNewImage($model, 'channel_avatar', 'channel_avatar');
            $this->processUploadNewImage($model, 'header_banner', 'header_banner');

            // update cache
            SuperAppApiGw::updateCache('create', 'channel', $model->id);

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing VcsChannel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $form_values['channel_avatar'] = Slim::getImagesFromSlimRequest('channel_avatar');
        $form_values['header_banner'] = Slim::getImagesFromSlimRequest('header_banner');

        if (Yii::$app->request->isPost && $form_values['channel_avatar'])
            $this->processUpdateImage($model, $form_values, 'channel_avatar', 'channel_avatar');

        if (Yii::$app->request->isPost && $form_values['header_banner'])
            $this->processUpdateImage($model, $form_values, 'header_banner', 'header_banner');

        if ($model->load($form_values) && $model->save()) {

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', 'channel')]));

            // update cache
            SuperAppApiGw::updateCache('update', 'channel', $model->id);

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VcsChannel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->actived = VcsChannel::DELETED_STATUS;
        $model->save(false);

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        // update cache
        SuperAppApiGw::updateCache('delete', 'channel', $model->id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the VcsChannel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VcsChannel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VcsChannel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function processUploadNewImage($model, $imageFieldName, $fileName = null) {

        $images = Slim::getImages($imageFieldName);

        if($images != false && sizeof($images) > 0) {
            $absPath = $model->getAbsImagePath();
            $savePath = Yii::$app->params['media_path']. $absPath;

            $savedFileName = Slim::saveSlimImage2('channel', $images[0], $savePath);

            $model->$imageFieldName = $absPath. $savedFileName;
            $model->update(false, [$imageFieldName]);
        }
    }

    public function processUpdateImage($model, $form_values, $imageFieldName, $fileName = null) {
        if ($form_values['change_'. $imageFieldName] == '1') {
            // $fullSavePath =  Yii::$app->params['sticker_folder']. $absSavePath;

            $oldImagePath = ($model->$imageFieldName)?
                Yii::getAlias('@backend_web'). DIRECTORY_SEPARATOR. $model->$imageFieldName:
                null;

            $images = Slim::getImages($imageFieldName);

            if($images != false && sizeof($images) > 0) {

                $absPath = $model->getAbsImagePath();
                $savePath = Yii::$app->params['media_path']. $absPath;
                $savedFileName = Slim::saveSlimImage2('channel', $images[0], $savePath);
                $model->$imageFieldName = $absPath. $savedFileName;
            } else {
                $model->$imageFieldName = null;

                $model->save();
                // Xoa file anh cu
                if ($oldImagePath && file_exists($oldImagePath))
                    unlink($oldImagePath);
            }
        }
    }

    public function actionBulk(){

        $selection=(array)Yii::$app->request->post('selection'); //typecasting
        $updateIds = $selection;

        if (Yii::$app->request->post('bulk_delete')) {
            $count = VcsChannel::updateAll(['actived' => VcsChannel::DELETED_STATUS ], [
                'id' => $selection
            ]);
            if ($count) {
                Yii::$app->session->setFlash('info', Yii::t('backend', 'Delete {total} items successfully!', [
                    'total' => $count
                ]));

                // update cache
                SuperAppApiGw::updateCache('delete', 'channel', $selection);
            }
        } elseif (Yii::$app->request->post('bulk_active')) {

            $count = VcsChannel::updateAll(['actived' => VcsChannel::ACTIVED_STATUS ], [
                'id' => $selection,
                'actived' => VcsChannel::WAIT_APPROVAL_STATUS,
            ]);

            if ($count) {
                Yii::$app->session->setFlash('info', Yii::t('backend', 'Display all: {total} items', [
                    'total' => $count
                ]));

                if ($count < count($selection)) {
                    // lay cac ban ghi vua duoc update
                    $updateIds = VcsChannel::find()
                        ->where([
                            'actived' => VcsChannel::ACTIVED_STATUS,
                            'id' => $selection,
                        ])
                        ->select(['id'])
                        ->column();
                }

                // update cache
                SuperAppApiGw::updateCache('update', 'channel', $updateIds);
            }
        } elseif (Yii::$app->request->post('bulk_active_search')) {

            $count = VcsChannel::updateAll(['actived' => VcsChannel::ACTIVED_SEARCH_STATUS ], [
                'id' => $selection,
                'actived' => VcsChannel::WAIT_APPROVAL_STATUS,
            ]);

            if ($count) {
                Yii::$app->session->setFlash('info', Yii::t('backend', 'Display for search engine: {total} items', [
                    'total' => $count
                ]));

                if ($count < count($selection)) {
                    // lay cac ban ghi vua duoc update
                    $updateIds = VcsChannel::find()
                        ->where([
                            'actived' => VcsChannel::ACTIVED_SEARCH_STATUS,
                            'id' => $selection,
                        ])
                        ->select(['id'])
                        ->column();
                }

                // update cache
                SuperAppApiGw::updateCache('update', 'channel', $updateIds);
            }
        } elseif (Yii::$app->request->post('bulk_hide')) {

            $count = VcsChannel::updateAll(['actived' => VcsChannel::HIDDEN_STATUS ], [
                'id' => $selection
            ]);

            if ($count) {
                Yii::$app->session->setFlash('info', Yii::t('backend', 'Hide {total} items successfully!', [
                    'total' => $count
                ]));

                if ($count < count($selection)) {
                    // lay cac ban ghi vua duoc update
                    $updateIds = VcsChannel::find()
                        ->where([
                            'actived' => VcsChannel::HIDDEN_STATUS,
                            'id' => $selection,
                        ])
                        ->select(['id'])
                        ->column();
                }

                // update cache
                SuperAppApiGw::updateCache('update', 'channel', $updateIds);
            }
        }



        return $this->redirect(['vcs-channel/index']);
    }
}
