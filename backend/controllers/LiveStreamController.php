<?php

namespace backend\controllers;

use  backend\daemons\ChatServer;
use common\components\slim\Slim;
use common\models\HistoryLiveStream;
use common\models\TblChannel;
use PhpOffice\PhpSpreadsheet\Calculation\Exception;
use Yii;
use common\models\TblLivestream;
use common\models\TblLivestreamSearch;
use yii\base\BaseObject;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\httpclient\Client;

/**
 * LiveStreamController implements the CRUD actions for TblLivestream model.
 */
class LiveStreamController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblLivestream models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post("hasEditable")) {
            $editableKey = Yii::$app->request->post('editableKey');
            $model = TblLivestream::findOne($editableKey);
            $posted = current($_POST['TblLivestream']); //gia tri post len
            $name_post_column = $_POST['editableAttribute']; //ten coloumn edit
            $model->$name_post_column = $posted[$name_post_column];
            $model->save(false);
            $output = '';
            $out = Json::encode(['output' => $output, 'message' => '']);

            echo $out;
            return '';
        }
        $searchModel = new TblLivestreamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblLivestream model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view_live_stream', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewDefault()
    {
        $channel = TblChannel::find()->where(['user_id' => Yii::$app->user->id])->one();

        $model = TblLivestream::find()->where([
            "channel_id" => $channel->id,
            "status" => [0, 1, 2, 3, 4], // sua lai status lay la 0
            "time_event_start" => null,
            "time_event_finish" => null
        ])->one();

        if (!$model) {
            $model = $this->actionCreateLiveStreamDefault($channel);
            $model->save(false);
        }
        $model->getErrors();
        return $this->render('view_live_stream', [
            'model' => $model,
            'channel' => $channel,
        ]);
    }

    public function actionCreateLiveStreamDefault($channel)
    {
        $model = new TblLivestream();
        $username = Yii::$app->user->getIdentity("username");
        $datenow = date('d-m-Y h:i:s');
        $streamkey = md5($username . $datenow);
        $model->title = "Live New";
        $model->status = 0;
        $model->link_avatar = '/medias/tbl_livestream/the_first.jpg';
        $model->channel_id = $channel ? $channel->id : Yii::$app->user->id ;
        $model->rtmp_push = 'rtmp://10.20.0.1:1935/live';
        $model->stream_key = $streamkey;
        $streamalias = $model->stream_alias = md5($streamkey);
        $model->profile = 1;
        $model->category = 1;
        $model->hls_play_link = 'http://10.20.0.1:8096/hls/' . $streamalias . '.m3u8';

        return $model;
    }

    // stop livestream tren ngnix
    public function actionStopLivestream()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $stream_id = explode(":", $data['stream_id']);
            $stream_id = $stream_id['0'];
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl('http://10.20.0.1:8091/v1/nginx_control/stop?id=' . $stream_id)
                ->send();
            $response = $response->getContent();
            // lay du lieu de thay doi stream_key
            if ($response == 'success') {
                $model = new TblLivestream();
                $model = $this->actionCreateLiveStreamDefault();
                if ($model->save())
                    return true;
            }
            return false;
        }
    }

    /**
     * Creates a new TblLivestream model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblLivestream();
        $model->channel_id = Yii::$app->user->id;
        $form_values = Yii::$app->request->post();


        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa
        $imageFieldName = 'link_avatar';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);
        if ($model->load(Yii::$app->request->post())) {


            $images = Slim::getImages($imageFieldName); // lay du lieu image

            if ($images != false && sizeof($images) > 0) {
//                $pathServer = 'livestream';
                $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);
                $model->$imageFieldName = $imagePath;
            }

            $username = Yii::$app->user->getIdentity("username");
            $datenow = date('d-m-Y h:i:s');
            $streamkey = md5($username . $datenow);
            $model->enable_time_event = '1';
            $model->stream_key = $streamkey;
            $model->status = 0;
            $streamalias = $model->stream_alias = md5($streamkey);
            $model->rtmp_push = 'rtmp://10.20.0.1/live';
            $model->hls_play_link = 'http://10.20.0.1:8096/hls/' . $streamalias . '.m3u8';
            $model->enable_record;
            $model->save(false);
            Yii::$app->session->setFlash('info', Yii::t('backend', 'Create Live Stream successfully!'));
            return $this->redirect(['create', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionReportComment() {
        if(Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            try {
                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('POST')
                    ->setUrl('http://103.143.206.63:8081/LivestreamAPI/v1/report_comment')
                    ->setData([
                        'blockId' => $data['blockId'],
                        'commentId' => $data['commentId'],
                        'livestreamId' => $data['livestreamId'],
                        'streamerId' => $data['streamerId'],
                        'userId' => $data['userId'],
                    ])
                    ->send();
                $response = $response->getContent();
                Yii::info('[CALL API REPORTCOMMENT ]' . json_encode($response),'app_api');
                return $data['blockId'];
            } catch (\Exception $e) {
                Yii::error('[CALL API REPORTCOMMENT ] error: ' . $e->getMessage(), 'app_api');
                return false;
            }
        }
    }

    public function actionHistoryLivestream()
    {
        if (Yii::$app->request->post("hasEditable")) {
            $editableKey = Yii::$app->request->post('editableKey');
            $model = TblLivestream::findOne($editableKey);
            $posted = current($_POST['TblLivestream']); //gia tri post len
            $name_post_column = $_POST['editableAttribute']; //ten coloumn edit
            $model->$name_post_column = $posted[$name_post_column];
            $model->save(false);
            $this->actionStopLiveStream($model->stream_id);
            $output = '';
            $out = Json::encode(['output' => $output, 'message' => '']);
            echo $out;
            return '';
        }

        $searchModel = new TblLivestreamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('history_livestream', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUpdateLiveDefault($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            $form_values = Yii::$app->request->post();
            // xu ly anh
            $imageFieldName = 'link_avatar';
            $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);
            $images = Slim::getImages($imageFieldName);
            if ($images != false && sizeof($images) > 0) {
                $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);
                $model->$imageFieldName = $imagePath;
            }
            //ket thuc xu ly anh
            $model->title = $form_values['title'];
            $model->enable_record = $form_values['enable_record'] ? 1 : 0;
            $model->description = $form_values['description'];
            $model->category = (int)$form_values['category'];
            $model->profile = (int)$form_values['profile'];
            if ($model->save()) {
                return 1;
            }
            $model->getErrors();
            return 0;

        }

        return $this->render('edit-live-default', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing TblLivestream model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $form_values = Yii::$app->request->post();
        $imageFieldName = 'link_avatar';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);
        if ($model->load(Yii::$app->request->post())) {
            $images = Slim::getImages($imageFieldName); // lay du lieu image
            if ($images != false && sizeof($images) > 0) {
//                $pathServer = 'livestream';
                $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);
                $model->$imageFieldName = $imagePath;
                $model->save();
                $model->getErrors();
            }
            Yii::$app->session->setFlash('info', Yii::t('backend', 'Update Success !!! '));
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TblLivestream model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    // luu status cua livestream
    public function actionChangeStatusLive()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $status = $data['status'];
            $stream_id = $data['stream_id'];
            $model = $this->findModel($stream_id);
            $model->status = $status;
            if ($model->save()) {
                return true;
            }
            return false;

        }
    }

    /**
     * Finds the TblLivestream model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblLivestream the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblLivestream::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
