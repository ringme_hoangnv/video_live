<?php

namespace backend\controllers;

use backend\components\common\MenuHelper;
use backend\models\Menu;
use backend\models\MenuSearch;
use backend\models\TblLiveChannel;
use backend\models\VcsChannel;
use common\models\TblLivestream;
use backend\models\TblReportContentType;
use common\models\TblLivestreamSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MenuController implements the CRUD actions for Menu model.
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class OverviewController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
//        $searchModelLivestream = new TblLivestreamSearch();
//        $dataProviderlivestream = TblLivestream::find()->where(['channel_id'=>211])->one();

        $dataProviderlivestream = TblLivestream::find()->where(['channel_id' => \Yii::$app->getUser()->id])->one();
//        $dataProvider = $searchModelLivestream->search(Yii::$app->request->queryParams);
//        $dataProvider->pagination = ['pageSize' => 1];
        $data_channel = TblLiveChannel::find()->where(['user_id' => 211])->one();
//        $data_violation = \app\models\TblReportContentType::find()->where(['id'=>1])->one();
        $searchModel = new TblLivestreamSearch();
        $dataProvider = $searchModel->searchLastest(Yii::$app->request->queryParams);
        $dataProviderToplivestream = $searchModel->searchTopLivestream(Yii::$app->request->queryParams);

        return $this->render('index', [
            'data_channel' => $data_channel,
            'dataProvider' => $dataProvider,
            'dataProviderlivestream' => $dataProviderlivestream,
            'searchModel' => $searchModel,
            'dataProviderTopLivestream' => $dataProviderToplivestream,
        ]);


    }
}