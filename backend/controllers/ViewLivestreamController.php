<?php

namespace backend\controllers;

use  backend\daemons\ChatServer;
use common\components\slim\Slim;
use common\models\HistoryLiveStream;
use Yii;
use common\models\TblLivestream;
use common\models\TblLivestreamSearch;
use yii\base\BaseObject;
use yii\helpers\Inflector;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\httpclient\Client;


class ViewLivestreamController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if (Yii::$app->request->post("hasEditable")) {
            $editableKey = Yii::$app->request->post('editableKey');
            $model = TblLivestream::findOne($editableKey);
            $posted = current($_POST['TblLivestream']); //gia tri post len
            $name_post_column = $_POST['editableAttribute']; //ten coloumn edit
            $model->$name_post_column = $posted[$name_post_column];
            $model->save(false);
            $this->actionStopLiveStream($model->stream_id);
            $output = '';
            $out = Json::encode(['output' => $output, 'message' => '']);
            echo $out;
            return '';
        }
        $this->actionGetStatusStreamingLive();
        $searchModel = new TblLivestreamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single TblLivestream model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view_live_stream', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionStopLiveStream($stream_id)
    {
        $client = new Client();
        $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://live-api.evgcdn.net/api/v1.0/live-stream/stop-stream/' . $stream_id)
            ->setHeaders(['X-Auth-Key' => '88dde7c9a5e24bbe94991739c3fc86c9'])
            ->send();
    }

}
