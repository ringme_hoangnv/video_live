<?php

namespace backend\controllers;

use backend\models\VideoItem;
use common\core\SuperAppApiGw;
use Yii;
use backend\models\VcsCategory;
use backend\models\VcsCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;

/**
 * VcsCategoryController implements the CRUD actions for VcsCategory model.
 */
class VcsCategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VcsCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VcsCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VcsCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new VcsCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VcsCategory();
        $model->actived = 0;
        $model->display_order = 0;
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa

        $form_values['icon_image'] = Slim::getImagesFromSlimRequest('icon_image');
        $form_values['header_banner'] = Slim::getImagesFromSlimRequest('header_banner');

        if ($model->load($form_values) && $model->save()) {
            $this->processUploadNewImage($model, 'icon_image', 'icon_image');
            $this->processUploadNewImage($model, 'header_banner', 'header_banner');

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', 'Category')]));

            // update cache
            SuperAppApiGw::updateCache('insert', 'cate', $model->id);

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing VcsCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $form_values['icon_image'] = Slim::getImagesFromSlimRequest('icon_image');
        $form_values['header_banner'] = Slim::getImagesFromSlimRequest('header_banner');

        if (Yii::$app->request->isPost && $form_values['icon_image'])
            $this->processUpdateImage($model, $form_values, 'icon_image', 'icon_image');
        if (Yii::$app->request->isPost && $form_values['header_banner'])
            $this->processUpdateImage($model, $form_values, 'header_banner', 'header_banner');


        if ($model->load($form_values) && $model->save()) {

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // update cache
            SuperAppApiGw::updateCache('update', 'cate', $model->id);

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing VcsCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->actived = VcsCategory::DELETED_STATUS;
        $model->save();

        // update cache
        SuperAppApiGw::updateCache('delete', 'cate', $model->id);

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the VcsCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VcsCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VcsCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function processUploadNewImage($model, $imageFieldName, $fileName = null) {

        $images = Slim::getImages($imageFieldName);

        if($images != false && sizeof($images) > 0) {
            $absPath = $model->getAbsImagePath();
            $savePath = Yii::$app->params['media_path']. $absPath;

            $savedFileName = Slim::saveSlimImage2('category', $images[0], $savePath);

            $model->$imageFieldName = $absPath. $savedFileName;
            $model->update(false, [$imageFieldName]);

        }
    }

    public function processUpdateImage($model, $form_values, $imageFieldName, $fileName = null) {
        if ($form_values['change_'. $imageFieldName] == '1') {
            // $fullSavePath =  Yii::$app->params['sticker_folder']. $absSavePath;

            $oldImagePath = ($model->$imageFieldName)?
                Yii::getAlias('@backend_web'). DIRECTORY_SEPARATOR. $model->$imageFieldName:
                null;

            $images = Slim::getImages($imageFieldName);

            if($images != false && sizeof($images) > 0) {
                $absPath = $model->getAbsImagePath();
                $savePath = Yii::$app->params['media_path']. $absPath;
                $savedFileName = Slim::saveSlimImage2('category', $images[0], $savePath);
                $model->$imageFieldName = $absPath. $savedFileName;

            } else {
                $model->$imageFieldName = null;

                $model->save();
                // Xoa file anh cu
                if ($oldImagePath && file_exists($oldImagePath))
                    unlink($oldImagePath);
            }
        }
    }
}
