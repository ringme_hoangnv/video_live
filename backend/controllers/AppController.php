<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;

class AppController extends Controller {

    public function beforeAction($action) {
//        if (Yii::$app->session->has('lang')) {
//            Yii::$app->language = Yii::$app->session->get('lang');
//        } else {
            Yii::$app->language = 'en';
//        }

        return parent::beforeAction($action);

    }

}
