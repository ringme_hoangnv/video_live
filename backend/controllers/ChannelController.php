<?php

namespace backend\controllers;

use common\models\TblChannel;
use common\models\TblChannelSearch;
use common\models\TblLivestream;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChannelController implements the CRUD actions for TblChannel model.
 */
class ChannelController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all TblChannel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblChannelSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblChannel model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblChannel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblChannel();

        if (\Yii::$app->request->isAjax) {
            if ($this->request->isPost) {
                $form_values = \Yii::$app->request->post();
                $model->name = $form_values['name'];
                $model->description = $form_values['description'];
                $model->user_id = \Yii::$app->user->id;
                $model->total_view = 0;
                $model->total_follow = 0;
                $model->total_livestream = 0;
                $model->total_time_live = 0;
                $model->status  = 1;
                if ($model->save()) {
                    $livestream = TblLivestream::find()->where([
                        "channel_id" => \Yii::$app->user->id,
                        "status" => [0, 1, 2, 3, 4], // sua lai status lay la 0
                        "time_event_start" => null,
                        "time_event_finish" => null
                    ])->one();
                    $livestream->channel_id = $model->id;
                    $livestream->save();
                    return 1;
                }
                return 0;
            }
        }
    }

    /**
     * Updates an existing TblChannel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TblChannel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblChannel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return TblChannel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblChannel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
