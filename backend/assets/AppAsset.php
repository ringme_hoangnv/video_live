<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
      'plugins/font-opensans/css/font-opensans.css',
      'plugins/font-awesome/css/font-awesome.min.css',
      'plugins/simple-line-icons/simple-line-icons.min.css',
      'css/components-md.min.css',
      'css/plugins-md.min.css',
      'css/layout.min.css',
      'css/themes/darkblue.min.css',
      'css/custom.css?v1.222222',
      //'css/c3.min.css',
  ];
  public $js = [
      //'plugins/jquery-slimscroll/jquery.slimscroll.min.js',
      'js/metronic/app.min.js',
      'js/metronic/layout.min.js', // có resize màn hình
      'js/metronic/quick-sidebar.min.js',
      'js/admin.js?v1.111',
//      'js/c3js/d3.v3.min.js',
//      'js/c3js/c3.min.js',
  ];
  public $depends = [
      'yii\web\YiiAsset',
      'yii\bootstrap\BootstrapAsset',
  ];

}