<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\RepChannelSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panelx panel-default rep-channel-search">
    <div class="panel-bodyx row">

        <?php  $form = ActiveForm::begin([
            'action' => [(isset($action) && $action)? $action: 'index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-4">
            <?=

            $form->field($model, 'report_date')->widget(\kartik\daterange\DateRangePicker::className(), [
                    'options' => ['class' => 'form-control daterange-field input-sm'],
                    'model' => $model,
                    'attribute' => 'report_date',
                    'convertFormat' => true,
                    'presetDropdown' => true,
                    'readonly' => true,
                    'pluginOptions' => [
                        'opens' => 'left',
                        'alwaysShowCalendars' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd/m/Y',
                        ]
                    ],
                    'pluginEvents' => [
                        'cancel.daterangepicker' => "function(ev, picker) {
                                            $(this).val('');
                                        }",
                        'apply.daterangepicker' => 'function(ev, picker) {
                                            if($(this).val() == "") {
                                                $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator +
                                                picker.endDate.format(picker.locale.format)).trigger("change");
                                            }
                                        }',
                        'show.daterangepicker' => 'function(ev, picker) {
                                            picker.container.find(".ranges").off("mouseenter.daterangepicker", "li");
                                            if($(this).val() == "") {
                                                picker.container.find(".ranges .active").removeClass("active");
                                            }
                                        }',
                    ]
                ]
            )
            ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'channel_id')->textInput()?>
            <?php /* $form->field($model, 'channel_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\VcsChannel::find()->all(), 'id', 'channel_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'Choose channel')
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); */?>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::submitButton('Export', ['class' => 'btn btn-warning', 'name' => 'action', 'value' => 'export', 'data-pjax' => '0']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
