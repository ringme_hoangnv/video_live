<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

\backend\assets\VideoAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel backend\models\VideoItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Video';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/custom-schedule.css');
?>
<style type="text/css">
    #detail-modal .plyr {max-width: 480px;}
</style>
<div class="row video-item-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                                'options' => [
                                    'class' => 'form-control  form-control-sm',
                                ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],
//                            [
//                                'class' => 'yii\grid\CheckboxColumn',
////                                'headerOptions' => ['width' => '5'],
//                            ],

                            'id',

                            'video_title',
                            [
                                'attribute' => 'video_image',
                                'value' => function ($model) {
                                    return $model->getVideoImageUrl();
                                },
                                'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                                'filter' => false,
                                'headerOptions' => ['style' => 'width:120px'],
                            ],

                            [
                                'attribute' => 'actived',
                                'value' => function ($model) {
                                    return $model->getActiveName();
                                },

                            ],

                            [
                                'attribute' => 'is_new',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->is_new == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],
                            [
                                'attribute' => 'is_hot',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->is_hot == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],

                            [
                                'attribute' => 'total_views',
                                'format' => 'integer'
                            ],
                            'resolution',
                            'publish_time',


                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => ['style' => 'width: 110px;', 'class' => 'head-actions'],
                                'contentOptions' => ['class' => 'row-actions'],
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->id], [
                                            'class' => '',
                                            'data-target' => '#detail-modal',
                                            'data-toggle' => "modal"
                                        ]);
                                    },
                                ]
                            ],
                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="detail-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>
<?php
$this->registerJs(<<<JS
    $(document).ready(function() {
        $('#detail-modal').on('shown.bs.modal', function () {

            var videoUrl = $('.vplayer', $(this)).attr('src');

            var video = document.querySelector('.vplayer');

            if (Hls.isSupported()  && videoUrl && (typeof videoUrl != 'undefined')) {
                var hls = new Hls();
                hls.loadSource(videoUrl);
                hls.attachMedia(video);
                hls.on(Hls.Events.MANIFEST_PARSED,function() {
                    //video.play();
                });
            }

            plyr.setup(video);
        });
        $('#detail-modal').on('hidden.bs.modal', function (e) {
            var video = document.querySelector('video');
            if (typeof video != 'undefined')
                video.pause();
        })
    });
JS
);
?>
