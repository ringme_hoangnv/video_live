<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\VideoItem */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="portlet light portlet-fit portlet-form bordered video-item-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'video_title')->textInput(['maxlength' => 500]) ?>
                <?= $form->field($model, 'cate_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\VcsCategory::find()->all(), 'id', 'cate_name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose category'),
                        'disabled' => ($model->belong != 'cms') ? true : false,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
                <?= $form->field($model, 'channel_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\VcsChannel::find()->all(), 'id', 'channel_name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose channel'),
                        'disabled' => ($model->belong != 'cms') ? true : false,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>

                <?=
                $form->field($model, 'publish_time', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>
                <?php
                if (!$model->isNewRecord) {
                    $oldStatus = $model->getOldAttribute('actived');
                    echo $form->field($model, 'actived')->dropDownList(\backend\models\VideoItem::getActiveDefineArr(), [
                        'disabled' => ($model->isNewRecord || $model->actived == \backend\models\VideoItem::WAIT_CONVERT_STATUS || in_array($oldStatus, [2,3])) ? true : false,
                    ]);
                }
                ?>

                <?= $form->field($model, 'video_desc')->textarea(['rows' => 4]) ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'video_media')->fileInput() ?>
                <?php if (!$model->isNewRecord && $model->video_media): ?>
                    <?= Html::a('<span style="font-size: 32px;vertical-align: middle" class="glyphicon glyphicon-play-circle"></span> ' . Yii::t('backend', 'Play video'), '#play', [
                        'class' => '',
                        'data-target' => '#detail-modal',
                        'data-toggle' => "modal"
                    ]);
                    ?>
                <?php endif; ?>

                <br />
                <br />
                <br />
                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'video_image',
                    'itemName' => 'video-image', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Video Image'),
                    'dataMinSize' => '0,0',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '16:9',
                    'model' => $model,
                    'dataWillRemove' => 'imageWillChange',
                    'dataWillSave' => 'imageWillChange',
                ]) ?>

                <br/>
                <?= $form->field($model, 'is_new')->checkbox() ?>
                <?= $form->field($model, 'is_hot')->checkbox() ?>
                <?php // $form->field($model, 'has_live')->checkbox() ?>
            </div>



            <div class="clearfix"></div>
            <?php if (!$model->isNewRecord): ?>
            <div class="col-md-6">
                <?= $form->field($model, 'note')->textarea(['rows' => 4]) ?>
            </div>


            <div class="col-md-6">
                <?= Yii::t('backend', 'Total views') ?>: <?= number_format(intval($model->total_views)) ?>
                <br/>
                <?= Yii::t('backend', 'Total shares') ?>: <?= number_format(intval($model->total_shares)) ?>
                <br/>
                <?= Yii::t('backend', 'Total likes') ?>: <?= number_format(intval($model->total_likes)) ?>
                <br/>
                <?= Yii::t('backend', 'Total comments') ?>: <?= number_format(intval($model->total_comments)) ?>
                <br/>
                <?= Yii::t('backend', 'Adaptive') ?>
                : <?= ($model->is_adaptive == 1) ? Yii::t('backend', 'Yes') : Yii::t('backend', 'No') ?>
                <br/>
                <?= Yii::t('backend', 'Encode type') ?>: <?= number_format(intval($model->getEncodeTypeName())) ?>
            </div>
            <?php endif;?>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= 'Back' ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-transparent green  btn-sm']) ?>

        <?php /*
 <?php if ($model->actived == \backend\models\VideoItem::WAIT_APPROVAL_STATUS):?>
  &nbsp;&nbsp;&nbsp;
        <?= Html::submitInput(Yii::t('backend', 'Approve'), ['class' => 'btn btn-success btn-sm', 'name' => 'approve']) ?>
        <?php endif;?>
 */ ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>

<!-- Modal -->
<div id="detail-modal" class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <?= '<video class="'.(strpos($model->video_media, '.mp4')? '': 'vplayer').'" controls  preload="none" src="'.$model->getVideoMediaUrl().'"  id="vm-'. $model->id. ' >    <source src="'.trim($model->getVideoMediaUrl()).'" type="application/x-mpegURL"></video>' ?>
            </div>

        </div>
    </div>
</div>
