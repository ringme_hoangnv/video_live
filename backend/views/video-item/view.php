<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\VideoItem */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Video Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row video-item-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a('Update', ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'cate_id',
                            'value' => function($model) {
                                return ($model->category)? $model->category->cate_name: null;
                            }
                        ],
                        [
                            'attribute' => 'channel_id',
                            'value' => function($model) {
                                return ($model->channel)? $model->channel->channel_name: null;
                            }
                        ],

                        'video_title',
                        'video_desc:ntext',
                        [
                            'attribute' => 'video_image',
                            'value' => function ($model) {
                                return $model->getVideoImageUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,

                            'headerOptions' => ['style' => 'width:120px'],
                        ],
                        [
                            'attribute' => 'video_media',
                            'value' => function ($model) {
                                return '<video class="'.(strpos($model->video_media, '.mp4')? '': 'vplayer').'" controls  preload="none" src="'.$model->getVideoMediaUrl().'"  id="vm-'. $model->id. ' >    <source src="'.trim($model->getVideoMediaUrl()).'" type="application/x-mpegURL"></video>';
                            },
                            'format' => 'raw',
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],
//                        [
//                            'attribute' => 'image_thumb',
//                            'value' => function ($model) {
//                                return $model->image_thumb;
//                            },
//                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
//                            'filter' => false,
//                            'headerOptions' => ['style' => 'width:120px'],
//                        ],
//                        [
//                            'attribute' => 'image_small',
//                            'value' => function ($model) {
//                                return $model->image_small;
//                            },
//                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
//                            'filter' => false,
//                            'headerOptions' => ['style' => 'width:120px'],
//                        ],


                        'video_time',
                        [
                            'attribute' => 'actived',
                            'value' => function ($model) {
                                return $model->getActiveName();
                            },

                        ],

                        [
                            'attribute' => 'is_new',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_new == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        [
                            'attribute' => 'is_hot',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_hot == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],


                        'publish_time',
                        'first_publish_time',
                        [
                            'attribute' => 'total_views',
                            'format' => 'integer'
                        ],
                        [
                            'attribute' => 'total_likes',
                            'format' => 'integer'
                        ],
                        [
                            'attribute' => 'total_shares',
                            'format' => 'integer'
                        ],
                        [
                            'attribute' => 'total_comments',
                            'format' => 'integer'
                        ],

//                        [
//                            'attribute' => 'has_live',
//                            'format' => 'raw',
//                            'value' => function ($object) {
//                                $class = ($object->has_live == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
//                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
//                            }
//                        ],
                        'resolution',
                        'aspec_ratio',

                        [
                            'attribute' => 'is_adaptive',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_adaptive == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        'adaptive_path',
                        'adaptive_resolution',

                        [
                            'attribute' => 'encode_type',
                            'value' => function ($model) {
                                return $model->getEncodeTypeName();
                            },

                        ],

                        'note:ntext',

                        'slug',
                        'created_at',
                        'updated_at',
                    ],
                ]) ?>

                <?php if ($model->actived == \backend\models\VideoItem::WAIT_APPROVAL_STATUS): ?>
                <div class="text-center ">
                    <div class="btn-group dropup">
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= Yii::t('backend', 'Approve') ?> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="dropdown-item" href="<?= \yii\helpers\Url::to([
                                    'video-item/approve',
                                    'id' => $model->id,
                                    'status' => 10,
                                ]) ?>">
                                    <?= Yii::t('backend', 'Display all') ?>
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="<?= \yii\helpers\Url::to([
                                    'video-item/approve',
                                    'id' => $model->id,
                                    'status' => 10,
                                ]) ?>">
                                    <?= Yii::t('backend', 'Display search') ?>
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="<?= \yii\helpers\Url::to([
                                    'video-item/approve',
                                    'id' => $model->id,
                                    'status' => 10,
                                ]) ?>">
                                    <?= Yii::t('backend', 'Video of channel') ?>
                                </a>
                            </li>
                        </ul>
                        <br />
                        <br />
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
