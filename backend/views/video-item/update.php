<?php

use yii\helpers\Html;
\backend\assets\VideoAsset::register($this);
/* @var $this yii\web\View */
/* @var $model backend\models\VideoItem */

$this->title = 'Update Video: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Video', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update' . ' ' . $model->id;
?>
<div class="row video-item-update">
    <div class="col-md-12">

    <?= $this->render('_form', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>
<?php
$this->registerJs(<<<JS
    $(document).ready(function() {
        $('#detail-modal').on('shown.bs.modal', function () {

            var videoUrl = $('.vplayer', $(this)).attr('src');
            $('.vplayer', $(this)).attr('oldsrc', videoUrl);
            //console.log('old url = ' + videoUrl)
            var video = $('.vplayer', $(this)).get(0);


            if (Hls.isSupported()  && videoUrl && (typeof videoUrl != 'undefined')) {
                var hls = new Hls();
                hls.loadSource(videoUrl);
                hls.attachMedia(video);
                hls.on(Hls.Events.MANIFEST_PARSED,function() {
                    //video.play();
                });
            }

            plyr.setup(video);
        });
        $('#detail-modal').on('hidden.bs.modal', function (e) {
            var video = $('video', $(this)).get(0);
            if (typeof video != 'undefined') {
                var videoUrl = $('.vplayer', $(this)).attr('oldsrc');
                $(video).attr('src', videoUrl)
                video.pause();

            }

        })
    });
JS
);
?>
