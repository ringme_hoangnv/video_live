<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\VideoItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default video-item-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>
        <div class="col-md-3">
            <?= $form->field($model, 'video_title') ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'cate_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\VcsCategory::find()->all(), 'id', 'cate_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'Choose category')
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'channel_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\VcsChannel::find()->all(), 'id', 'channel_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'Choose channel')
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>
        </div>

        <?php if ($model->showStatusFilter): ?>
        <div class="col-md-3">
            <?php
            $statusArr = \backend\models\VideoItem::getActiveDefineArr();
            unset($statusArr[\backend\models\VideoItem::DELETED_STATUS]);
            echo $form->field($model, 'actived')->dropDownList(
                $statusArr,
                ['prompt' => Yii::t('backend', 'All')]
            ) ?>

        </div>
        <?php endif; ?>
        <div class="col-md-3">
            <?php echo
            $form->field($model, 'is_new')->dropDownList(
                [
                    0 => Yii::t('backend', 'Not New') ,
                    1 => Yii::t('backend', 'Is New'),
                ],
                ['prompt' => Yii::t('backend', 'All')]
            );;

            ?>

        </div>
        <div class="col-md-3">
            <?php echo
            $form->field($model, 'is_hot')->dropDownList(
                [
                    0 => Yii::t('backend', 'Not Hot') ,
                    1 => Yii::t('backend', 'Is Hot'),
                ],
                ['prompt' => Yii::t('backend', 'All')]
            );;

            ?>

        </div>

        <div class="col-md-3">
            <?php // echo

            $form->field($model, 'publish_time')->widget(\kartik\daterange\DateRangePicker::className(), [
                    'options' => ['class' => 'form-control daterange-field input-sm'],
                    'model' => $model,
                    'attribute' => 'publish_time',
                    'convertFormat' => true,
                    'presetDropdown' => true,
                    'readonly' => true,
                    'pluginOptions' => [
                        'opens' => 'left',
                        'alwaysShowCalendars' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd/m/Y',
                        ]
                    ],
                    'pluginEvents' => [
                        'cancel.daterangepicker' => "function(ev, picker) {
                                            $(this).val('');
                                        }",
                        'apply.daterangepicker' => 'function(ev, picker) {
                                            if($(this).val() == "") {
                                                $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator +
                                                picker.endDate.format(picker.locale.format)).trigger("change");
                                            }
                                        }',
                        'show.daterangepicker' => 'function(ev, picker) {
                                            picker.container.find(".ranges").off("mouseenter.daterangepicker", "li");
                                            if($(this).val() == "") {
                                                picker.container.find(".ranges .active").removeClass("active");
                                            }
                                        }',
                    ]
                ]
            )
            ?>

        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
