<?php

use awesome\backend\widgets\AwsBaseHtml;
use awesome\backend\form\AwsActiveForm;
use \yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered user-form">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-paper-plane "></i>
                <span class="caption-subject  sbold uppercase">
                <?= $title ?>
                </span>
        </div>

    </div>
    <div class="portlet-body">
        <div class="form-body">
            <div class="col-md-6">
                <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
                <?php
//                $form->field($model, 'status', [
//                    'template' => '{label}{input}{error}{hint}',
//                ])->dropDownList([
//                    1 => 'Hoạt động',
//                    0 => 'Ngừng hoạt động'
//                ])
                ?>
                <?php echo $form->field($model, 'status')->checkbox([
                    'label' => Yii::t('backend', 'Active/Inactive')
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'new_password')->passwordInput() ?>

                <?= $form->field($model, 're_password')->passwordInput() ?>


            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
            <?php


                echo $form->field($model, 'fullname')->textInput(['maxlength' => 255]);

            ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'address')->textInput(['maxlength' => 255]); ?>
            </div>



        </div>
    </div>

    <div class="portlet-title">

        <div class="col-md-12">
            <?= AwsBaseHtml::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-info   btn-sm']) ?>
            <button type="button" name="back" class="btn btn-transparent black  btn-sm"
                    onclick="history.back(-1)">
                <i class="fa fa-angle-left"></i> Back
            </button>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
