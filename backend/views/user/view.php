<?php

use awesome\backend\widgets\AwsBaseHtml;
use backend\models\User;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Admin'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row user-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list "></i>
                    <span class="caption-subject  sbold uppercase">
                        <?= AwsBaseHtml::encode($this->title) ?>
                    </span>
                </div>
                <div class="actions">
                    <?= AwsBaseHtml::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                        ['class' => 'btn btn-info   btn-sm'])
                    ?>
                    <?= AwsBaseHtml::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger red  btn-sm',
                        'data' => [
                            'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'username',
                        'email:email',
                        [
                            'label' => 'Link reset mật khẩu',
                            'format' => 'url',
                            'value' => function($model) {
                                $resetUrl = \yii\helpers\Url::to(['site/reset-password', 'token' => $model->password_reset_token], true);
                                return $resetUrl; //\yii\helpers\Html::a($resetUrl, $resetUrl);
                            }
                        ],
                        [
                            'label' => 'status',
                            'value' => ($model->status == User::STATUS_ACTIVE) ? Yii::t('backend', 'Actived') : Yii::t('backend', 'Inactive'),
                        ],
                        [
                            'label' => 'created_at',
                            'value' => date('H:i:s d/m/Y', $model->created_at),
                        ],
                        [
                            'label' => 'updated_at',
                            'value' => date('H:i:s d/m/Y', $model->updated_at),
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
