<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\VcsChannel */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Channel',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Channels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/custom-schedule.css');
?>
<div class="row vcs-channel-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
