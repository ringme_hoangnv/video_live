<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VcsChannelSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default vcs-channel-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'msisdn') ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'channel_name') ?>

        </div>
        <?php if ($model->showStatusFilter): ?>
        <div class="col-md-3">
            <?php
            $statusArr = \backend\models\VcsChannel::getActiveDefineArr();
            unset($statusArr[\backend\models\VcsChannel::DELETED_STATUS]);
            echo $form->field($model, 'actived')->dropDownList(
                $statusArr,
                ['prompt' => Yii::t('backend', 'All')]
            ) ?>

        </div>
        <?php endif;?>

        <div class="col-md-3">
            <?php  echo
            $form->field($model, 'is_official')->dropDownList(
                [
                    0 => Yii::t('backend', 'Not Official'),
                    1 => Yii::t('backend', 'Official'),
                ],
                ['prompt' => Yii::t('backend', 'All')]
            );

            ?>

        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>

                <?php
                /*<div class="float-right">
                    <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('backend', 'Create {modelClass}', [
                        'modelClass' => Yii::t('backend', 'Channel')
                    ]),
                        ['create'], ['class' => 'btn btn-info btn-sm']) ?>
                </div>
                 * */?>
                <div class="clearfix"></div>
            </div>

        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
