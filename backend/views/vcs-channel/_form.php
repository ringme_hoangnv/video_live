<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model backend\models\VcsChannel */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered vcs-channel-form">

    <div class="portlet-body">
        <div class="form-body row">

            <div class="col-md-6">
                <?= $form->field($model, 'channel_name')->textInput(['maxlength' => 500]) ?>
                <?php if (!$model->isNewRecord):?>
                    <?= $form->field($model, 'msisdn')->textInput(['maxlength' => 30, 'readonly' => true]) ?>
                <?php endif;?>

                <?php
                if (!$model->isNewRecord) {

                    $statusArr = \backend\models\VcsChannel::getActiveDefineArr();
                    unset($statusArr[\backend\models\VcsChannel::DELETED_STATUS]);
                    echo $form->field($model, 'actived')->dropDownList($statusArr, [
                        'disabled' => ($model->isNewRecord)? true: false,
                    ]);
                } ?>

                <br />
                <?= $form->field($model, 'is_official')->checkbox() ?>

                <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
            </div>
            <div class="col-md-6">

                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'channel_avatar',
                    'itemName' => 'channel', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Avatar'),
                    'dataMinSize' => '500,500',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '500:500',
                    'model' => $model,
                    'dataWillRemove' => 'avatarWillChange',
                    'dataWillSave' => 'avatarWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 500 x 500px(.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>

                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'header_banner',
                    'itemName' => 'channel', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Header banner'),
                    'dataMinSize' => '1080,368',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '1080:368',
                    'model' => $model,
                    'dataWillRemove' => 'headerWillChange',
                    'dataWillSave' => 'headerWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 1080 x 368px (.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>
            </div>


            <div class="clearfix"></div>
            <?php /*
            <div class="col-md-6">
                <?= $form->field($model, 'num_follows')->textInput(['readonly' => true]) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'num_videos')->textInput(['readonly' => true]) ?>
            </div>
            */?>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>

    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>

