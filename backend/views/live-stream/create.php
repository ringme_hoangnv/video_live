<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TblLivestream */

$this->title = 'Create Livestream';
$this->params['breadcrumbs'][] = ['label' => 'Livestreams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/custom-schedule.css');
?>
<div class="tbl-livestream-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
