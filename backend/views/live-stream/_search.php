<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TblLivestreamSearch */
/* @var $form yii\widgets\ActiveForm */
$dataCategory = [
    '1' => 'Sport',
    '2' => 'Travel',
    '3' => 'VLOG'
];
?>

<div class="panel panel-default tbl-livestream-search">
    <div class="panel-body row">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'title') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'category')->widget(\kartik\select2\Select2::classname(), [
                'data' => $dataCategory,
                'size' => \kartik\select2\Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'Choose category'),
                    'disabled' =>  false, // vo hieu hoa select neu disabled la true
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a('Reset', ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
