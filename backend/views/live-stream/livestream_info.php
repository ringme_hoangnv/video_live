<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblLivestreamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Livestreams';
$this->params['breadcrumbs'][] = 'Livestreams';
?>
<style type="text/css">
    #detail-modal .plyr {max-width: 480px;}
    .summary {
        display: none;
    }
</style>
<!--<div class="row ">-->
<!--    <div class="col-md-12">-->
<!--            <div class="portlet-body">-->
<!--                <div class="table-container">-->

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'title',
                            'description',
                            'stream_key',

                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->status == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],
                            [
                                'attribute' => 'enable_record',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->enable_record == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],

                            [
                                'attribute' => 'profile',
                                'value' => function($model) {
                                    return $model->getProfile();
                                }
                            ],

                            'rtmp_push',

                        ],
                    ]); ?>

<!--                </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->


