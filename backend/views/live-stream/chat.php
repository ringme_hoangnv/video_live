<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TblLivestream */

?>
<script src="/js/livestreamJs/sockjs.min.js"></script>
<script src="/js/livestreamJs/stomp.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="/js/livestreamJs/socket.js"></script>


<link href="/lib/css/nanoscroller.css" rel="stylesheet">
<link href="/lib/css/emoji.css" rel="stylesheet">

<!-- ** Don't forget to Add jQuery here ** -->
<script src="/lib/js/nanoscroller.min.js"></script>
<script src="/lib/js/tether.min.js"></script>
<script src="/lib/js/config.js"></script>
<script src="/lib/js/util.js"></script>
<script src="/lib/js/jquery.emojiarea.js"></script>
<script src="/lib/js/emoji-picker.js"></script>


<!--<script src="/js/livestreamJs/chat.js"></script>-->
<!--<div class="show-chat">-->
<!--    <button class="button-show">SHOW CHAT</button>-->
<!--</div>-->

<div class="index-chat ">
    <div class="top-chat">
        <h4 style="    padding-top: 17px;
    text-align: center;margin: 0 ;background-color: #282828">Chat</h4>
    </div>
    <div class="tab-comment" id="tab-comment">
        <div id="main-content" class="container">
            <div class="row display-hide">
                <div class="col-md-6">
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="connect">User connection:</label>
                            <button id="connect" class="btn btn-default" type="submit">Connect</button>
                            <button id="subscribe" class="btn btn-default">subscribe</button>
                            <button id="unsubscribe" class="btn btn-default">unsubscribe</button>
                            <button id="disconnect" class="btn btn-default" type="submit" disabled="disabled">Disconnect
                            </button>
                            <button id="test" class="btn btn-default">TEST</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row content-comment">
                <div class="col-md-12 conversation" style="padding: 0">
                    <div id="conversation">
                        <div id="greetings">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="send-msg row">
        <div class="input-comment">
            <div class="avt-user">
                <div class="div-img">
                    <img src="https://icdn.dantri.com.vn/thumb_w/640/2019/11/02/nhung-hot-girl-noi-bat-thang-10-docx-1572697558949.jpeg">
                </div>
            </div>
            <form class="form-inline" style="width: 86%; margin-top: 1%">
                <div class="user-name"> Van Hoang Nguyen</div>
                <div class="form-group input-msg">
                    <input  autocomplete="off" type="text" id="name" class="form-control msg"
                           placeholder="say something..." style="width: 100%; border: none; padding: 0">
                    <hr style="margin: 0">


                </div>
            </form>
        </div>
        <div>
            <button id="send" class="btn btn-default button-send" type="submit"><i class="far fa-paper-plane"></i>
            </button>
            <div class="icon-msg"></div>
        </div>
    </div>
</div>
