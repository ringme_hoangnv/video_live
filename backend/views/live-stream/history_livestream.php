<?php

use yii\helpers\Html;
use kartik\grid\GridView;

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblLivestreamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */
/* @var $model common\models\TblLivestream */

$this->title = 'Create Livestream';
$this->title = 'Livestreams';
$this->params['breadcrumbs'][] = 'Livestreams';
$this->registerCssFile('/css/custom-history.css');
?>
<style type="text/css">
    #detail-modal .plyr {
        max-width: 480px;
    }

    ..bg-green {
        background-color: green;
    }

</style>

<div class="row tbl-livestream-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <h3>History Livestream</h3>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <?= \kartik\grid\GridView::widget(['dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'attribute' => 'link_avatar',
                                'value' => function ($model) {
                                    return $model->getImagePath();
                                },
                                'format' => ['image', ['width' => '100%']],
                                'filter' => false,
                                'options' => ['style' => 'width:20%; height:160px']
//                                'headerOptions' => ['style' => 'width:120px'],
                            ],
                            [
                                'attribute' => 'title',
                                'options' => ['style' => 'width:10%']// For TD
                            ],

                            ['attribute' => 'link_play_record',
                                'options' => ['style' => 'width:5%; height:50px'],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return
                                        "<video id='hls-example' class='video video - js vjs -default-skin' controls muted='muted'>
                    <source type='application / x - mpegURL'
                            src=' <?= $model->link_play_record ?>'>
                    </video>";
                                }
                            ],
                            [
                                'attribute' => 'enable_record',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->enable_record) {
                                        return "<a download href=' <?= $model->link_play_vod ?> '>Dowload</a>";
                                    }
                                }
                            ],
                            [
                                'attribute' => 'total_view',
                                'options' => ['style' => 'width:5%']// For TD
                            ],
                            [
                                'attribute' => 'total_like',
                                'options' => ['style' => 'width:5%']// For TD
                            ],
                            [
                                'attribute' => 'total_comment',
                                'options' => ['style' => 'width:5%']// For TD
                            ],
                            [
                                'attribute' => 'total_share',
                                'options' => ['style' => 'width:5%']// For TD
                            ],
                            [
                                'class' => 'kartik\grid\EditableColumn',
                                'options' => ['style' => 'width:5%; height:160px'],
                                'format' => 'raw',
                                'attribute' => 'status',
                                'editableOptions' => [
                                    'header' => 'status',
                                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                                    'data' => [5 => 'FINISHED',],
                                    'displayValueConfig' => [
                                        '0' => '<span class= "badge badge-secondary">OFFLIVE</span>',
                                        '1' => '<span class= "badge badge-danger">LIVE</span>',
                                        '2' => '<span class= "badge badge-success">PUBLIC</span>',
                                        '3' => '<span class= "badge badge-info">PROTECTED</span>',
                                        '4' => '<span class= "badge badge-warning">PRIVATE</span>',
                                        '5' => '<span class="badge badge-default">FINISHED</span>',

                                    ]
                                ],
                            ],
                            [
                                'attribute' => 'time_start',
                                'options' => ['style' => 'width:15%']// For TD
                            ],
//                            [
//                                'attribute' => 'time_event_finish',
//                                'options' => ['style' => 'width:15%']// For TD
//                            ],


                            ['class' => 'yii\grid\ActionColumn', 'options' => ['style' => 'width:5%']],

                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</div>



