<?php

use yii\helpers\Html;

use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblLivestreamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\TblLivestream */

$this->title = 'Create Livestream';
$this->title = 'Livestreams';
$this->params['breadcrumbs'][] = 'Livestreams';
$this->registerCssFile('/css/custom-schedule.css');
?>
<style type="text/css">
    #detail-modal .plyr {
        max-width: 480px;
    }

    ..bg-green {
        background-color: green;
    }

</style>

<div class="row tbl-livestream-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                <div>

                    <?= Html::a(Yii::t('backend', 'Create live stream ', [

                    ]),
                        ['create'], ['class' => 'btn btn-info   btn-sm']) ?>

                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'attribute' => 'link_avatar',
                                'value' => function ($model) {
                                    return $model->getImagePath();
                                },
                                'format' => ['image', ['width' => '100%']],
                                'filter' => false,
                                'options' => ['style' => 'width:15%; height:160px']
//                                'headerOptions' => ['style' => 'width:120px'],
                            ],

                            [
                                'attribute' => 'title',
                                'options' => ['style' => 'width:20%']// For TD
                            ],

                            [
                                'attribute' => 'time_event_start',
                                'options' => ['style' => 'width:15%']// For TD
                            ],
                            [
                                'attribute' => 'time_event_finish',
                                'options' => ['style' => 'width:15%']// For TD
                            ],


//                            [
//                                'class' => 'kartik\grid\EditableColumn',
//                                'format' => 'raw',
//                                'attribute' => 'status',
//                                'options' => ['style' => 'width:11%'],
//                                'editableOptions' => [
//                                    'header' => 'status',
//                                    'inputType' => kartik\editable\Editable::INPUT_DROPDOWN_LIST,
//                                    'data' => [ 1 => 'LIVE', 2 => 'PUBLIC', 3 => 'PROTECTED', 4 => 'PRIVATE'],
//                                    'displayValueConfig' => [
//                                        '0' => '<span class= "badge badge-secondary">OFFLIVE</span>',
//                                        '1' => '<span class= "badge badge-danger">LIVE</span>',
//                                        '2' => '<span class= "badge badge-success">PUBLIC</span>',
//                                        '3' => '<span class= "badge badge-info">PROTECTED</span>',
//                                        '4' => '<span class= "badge badge-warning">PRIVATE</span>',
//                                        '5' => '<span class= "badge badge-default">FINISHED</span>',
//                                    ]
//                                ],
//                            ],


//                            [
//                                'attribute' => 'stream_key',
//                                'options' => ['style' => 'width:10%']
//                            ],
//
//                            [
//                                'attribute' => 'rtmp_push',
//                                'options' => ['style' => 'width:13%']
//                            ],

                            ['class' => 'yii\grid\ActionColumn', 'options' => ['style' => 'width:7%']],

                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
</div>



