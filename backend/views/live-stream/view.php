<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TblLivestream */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Livestreams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tbl-livestream-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($object) {
                    $class = ($object->status == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                }
            ],
            'stream_key',
            'hls_play_link',
            'time_event_start',
            'time_event_finish',
            'vod_play_back',
            'created_at',
            'updated_at',
            'stream_id',
            'user_id',
            [
                'attribute' => 'profile',
                'value' => function($model) {
                    return $model->getProfile();
                }
            ],
            'total_view',
            'total_comment',
            'total_share',
//            'enable_time_event:boolean',
            [
                'attribute' => 'enable_record',
                'format' => 'raw',
                'value' => function ($object) {
                    $class = ($object->enable_record == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                }
            ],
            'time_start',
            'time_finish',
             [
                'attribute' => 'category',
                'value' => function($model) {
                    return $model->getCategory();
                }
            ],
            'total_like',
        ],
    ]) ?>

</div>
