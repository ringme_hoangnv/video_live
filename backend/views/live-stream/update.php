<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TblLivestream */

$this->title = 'Update  ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Livestreams', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];`
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-livestream-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
