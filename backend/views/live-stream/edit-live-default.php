<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TblLivestream */
/* @var $form yii\widgets\ActiveForm */
$dataCategory = [
    '1' => 'Sport',
    '2' => 'Travel',
    '3' => 'VLOG'
];
$dataProfile = [
    '1' => "1080p, 720p, 480p, 360p",
    '2' => "720p, 480p, 360p",
    '3' => "480p, 360p",
    '4' => "live",
];
?>
<?php $form = ActiveForm::begin(); ?>

<div class="col-md-6 edit-left">

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->widget(\kartik\select2\Select2::classname(), [
        'data' => $dataCategory,
        'size' => \kartik\select2\Select2::MEDIUM,
        'options' => [
            'placeholder' => Yii::t('backend', 'Choose category'),
            'disabled' => false, // vo hieu hoa select neu disabled la true
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'addon' => [
            'prepend' => [
                'content' => '<i class="glyphicon glyphicon-search"></i>'
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'profile')->widget(\kartik\select2\Select2::classname(), [
        'data' => $dataProfile,
        'size' => \kartik\select2\Select2::MEDIUM,
        'options' => [
            'placeholder' => Yii::t('backend', 'Choose profile'),
            'disabled' => false, // vo hieu hoa select neu disabled la true
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
        'addon' => [
            'prepend' => [
                'content' => '<i class="glyphicon glyphicon-search"></i>'
            ]
        ],
    ]); ?>


    <div class="record_live_stream" style="margin-top: 27px;">
        <div class="input-group-text">
            <input type="checkbox" aria-label="Checkbox for following text input"
                   name=TblLivestream[enable_record]>
            <label class="form-check-label" for="flexCheckDefault">
                Record Live Streams as MP4
            </label>
        </div>
    </div>


</div>

<div class="col-md-6 edit-right">
    <?= $form->field($model, 'description')->textarea(['rows' => 3,]) ?>
    <?= $this->render('/common/_slim_image_field', [
        'fieldName' => 'link_avatar',
        'itemName' => \yii\helpers\Inflector::camel2id(\yii\helpers\StringHelper::basename($model::className())), // Vi du: adv, post, post-category de luu rieng tung folder
        'fieldLabel' => Yii::t('backend', 'Image'),
        'dataMinSize' => '0,0',
        'dataSize' => '',
        'dataForceSize' => '',
        'dataRatio' => '',
        'model' => $model,
    ]) ?>

</div>

<div class="portlet-title">

    <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm btn-back">
        <i class="fa fa-angle-left"></i> <?= 'Back' ?>
    </a>

    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-transparent green btn-sm']) ?>

</div>

<?php
\common\components\slim\SlimAsset::register($this);
?>

<?php ActiveForm::end(); ?>

