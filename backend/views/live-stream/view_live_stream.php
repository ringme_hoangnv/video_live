<?php

use backend\assets\LivestreamAsset;
use common\models\TblLivestream;
use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\components\toast\AwsAlertToast;

/* @var $this yii\web\View */
/* @var $model common\models\TblLivestream */
/* @var $channel common\models\TblChannel */

//\yii\web\YiiAsset::register($this);
$userName = Yii::$app->user->identity->username;
$userID = Yii::$app->user->id;
$roomId = $model->id;
?>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
    .video-js {
        width: 100%;
        height: 600px;
    }

</style>


<!--<div class="div-image-live">-->
<!--    <img class="image-live"-->
<!--         src="https://mk0salkeizk12or7kyfk.kinstacdn.com/wp-content/uploads/sites/6/2020/04/live-stream-scaled.jpg">-->
<!--    <div class='run-text' style="width: 95.8%;">-->
<!--        <marquee direction="right">-->
<!--            Wish you happy livestream !!!-->
<!--        </marquee>-->
<!--    </div>-->
<!--</div>-->
<div class="alert alert-success" id="success-alert">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <strong>Notice! </strong> you dont have channel!!.
</div>
<div class="main">

    <div class="live-stream">
        <div class="live-stream-screen">
            <div class="screen1">
                <video id='hls-example' class="video video-js vjs-default-skin" controls muted="muted">
                    <source type="application/x-mpegURL" src=" <?= $model->hls_play_link ?>">
                </video>
            </div>
            <div class="livestream-discription">
                <div class="livestream-title" style="margin-bottom: 1%">
                    <div style="opacity: 0.7;">Title</div>
                    <div style="font-size: 113%;"><?= $model->title ?></div>
                </div>
                <div class="livestream-category" style="margin-bottom: 1%">
                    <div style="opacity: 0.7;">Category</div>
                    <div style="font-size: 113%;">
                        <?php if ($model->category == 1): ?>
                            <div> Sport</div>
                        <?php elseif ($model->category == 2): ?>
                            <div> Travel</div>
                        <?php else: ?>
                            <div> Vlog</div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="livestream-profile" style="margin-bottom: 1%">
                    <div style="opacity: 0.7;"> Profile</div>
                    <div style="font-size: 113%;">
                        <?php if ($model->profile == 1): ?>
                            <div> 1080p, 720p, 480p, 360p</div>
                        <?php elseif ($model->profile == 2): ?>
                            <div> 720p, 480p, 360p</div>
                        <?php elseif ($model->profile == 3): ?>
                            <div> 480p, 360p</div>
                        <?php else: ?>
                            <div> Live</div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="view-like">
                    <div class="view-like-1">
                        <div style="opacity: 0.7;width: auto;">Total view</div>
                        <div style="font-size: 113%;" class="stream-view"></div>
                    </div>

                    <div class="view-like-2">
                        <div style="opacity: 0.7;width: auto;">Total like</div>
                        <div style="font-size: 113%;" class="stream-like"></div>
                    </div>
                </div>


            </div>

            <div class="div-button">
                <!-- Button to Open the Modal -->
                <button type="button" class="btn btn-primary btn-edit-livestream" data-toggle="modal"
                        data-target="#exampleModal-2">
                    Edit
                </button>


            </div>

        </div>

        <div class="notify-live">
            <!--           <div id = 'online'><i class="glyphicon glyphicon-remove-sign" style="margin-right: 1%;"></i> Để phát trực tiếp, hãy gửi video của bạn bằng phần mềm phát trực tiếp</div>-->
            <div class="notify-live-title" id='offline'><i class="glyphicon glyphicon-ok-sign"
                                                           style="margin-right: 1%;"></i> Livestream đã được
                kết nối
            </div>
            <div class="notify-live-btn">

                <div class="div-stop"><a> <?= 'STOP' ?></a></div>
                <div class="div-public">
                    <select name="cars" id="cars" class="selectStatus">
                        <option value="">Select...</option>
                        <option value="2">Public</option>
                        <option value="3">Only follower</option>
                        <option value="4">Private</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="setting-info-livestream">
            <div class="livestream-setting">
                <div class="live-stream-button">

                    <div class="btn-setting" id="btn-setting-father" style="background-color: white">
                        <button id="btn-setting" type="button" onclick="openCity(event,'setting')">
                            Livestream setting
                        </button>
                    </div>
                    <div class="btn-analysis" id="btn-analysis-father">
                        <button type="button" id="btn-analysis" onclick="openCity(event,'analysis')">
                            Analysis livestream
                        </button>
                    </div>
                    <div class="btn-status" id="btn-status-father">
                        <button type="button" id="btn-status" onclick="openCity(event,'status')">
                            Livestream status
                        </button>
                    </div>

                </div>


            </div>

            <div id="live-stream-setting" style="display: block">
                <div class="live-stream-manager">
                    <div class="config-stream">
                        <div class="stream-key">
                            <div style="opacity: 0.7; font-size: 108%;">Stream key 1</div>
                            <div style="font-size: 120%; display: none">
                                <div id='stream-key-1'
                                     style="display: inline-block;width: 209px;word-break: break-word"> <?= $model->stream_key ?> </div>
                                <button onclick="copyValueStreamKey()" style="border: 1px solid;margin-left: 5%;">Copy
                                </button>
                            </div>
                        </div>
                        <div class="stream-url">
                            <div style="opacity: 0.7; font-size: 108%;"> Stream url</div>
                            <div style="font-size: 120%; display: none ">
                                <div id='stream-url-1' style="display: inline-block;width: 209px;word-break: break-word"
                                "> <?= $model->rtmp_push ?> </div>
                            <button onclick="copyValueStreamUrl()" style="border: 1px solid;margin-left: 5%;">
                                Copy
                            </button>
                        </div>
                    </div>
                </div>


                <div class="additional">
                    additional 1
                </div>
            </div>

        </div>

        <div id="live-stream-analysis" style="display: none">
            <div class="live-stream-manager">
                <div class="config-stream">
                    <div class="stream-key">
                        <div style="opacity: 0.7; font-size: 108%;">Stream key 2</div>
                        <div style="font-size: 120%; display: none"><?= $model->stream_key ?></div>
                    </div>
                    <div class="stream-url">
                        <div style="opacity: 0.7; font-size: 108%;"> Stream url</div>
                        <div style="font-size: 120%; display: none"> <?= $model->rtmp_push ?> </div>
                    </div>
                    <div class="stream-delay">
                        <div style="opacity: 0.7; font-size: 108%;">Total view</div>
                        <div style="font-size: 120%;"><?= $model->total_view ?></div>
                    </div>
                </div>

                <div class="additional">
                    additional 2
                </div>
            </div>
        </div>
        <div id="live-stream-status" style="display: none">
            <div class="live-stream-manager">
                <div class="status-title">
                    <div class="status-title-1">Trang thai livestream</div>
                    <div class="status-title-2">Trang thai 1</div>
                </div>

                <div class="additional">
                    <div> Tin nhan</div>
                    <div class="livestream-status"></div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="chat">
    <?= $this->render('chat', ['model' => $model]) ?>
</div>
<!-- Modal edit livestream -->
<div class="modal-livestream">
    <!-- Modal -->
    <div class="modal fade" id="exampleModal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Live Stream</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php
                    echo $this->render('edit-live-default', [
                        'model' => $model,
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!--        Modal create channel-->
<div class="modal-channel" >


    <!-- Modal -->
    <div class="modal modal-child" id="exampleModal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" style="display: none">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Channel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php
                    if (!$channel) {
                        $channel = new \common\models\TblChannel();
                    }
                    echo $this->render('/channel/create', [
                        'model' => $channel,
                    ]);
                    ?>
                </div>

                <!--                       <div class="modal-footer">-->
                <!--                           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                <!--                           <button type="button" class="btn btn-primary">Save</button>-->
                <!--                       </div>-->
            </div>
        </div>
    </div>
</div>


</div>


<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
<script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        $("#success-alert").hide();


    });
    function stopLiveStream(stream_id) {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . '/live-stream/stop-livestream' ?>',
            type: 'post',
            data: {
                stream_id: stream_id,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                if (data) {
                    alert("Stop live stream success !!!")
                } else {
                    alert("Stop live stream fail !!!")
                }
            }
        });
    }

    function createChannel(channel_name, channel_des) {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . '/channel/create' ?>',
            type: 'post',
            data: {
                name: channel_name,
                description: channel_des,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                if (data==1) {
                    // window.setTimeout('alert("Create channel success !!!");window.close();', 5000);
                    alert("Create channel success !!!")
                    location.reload();
                } else {
                    document.getElementById('exampleModal-1').style.display = 'block'

                }
            }
        });
    }

    function editLiveDefault() {
        $.ajax({
            // var formData = new FormData($('.my-slim').val());
            url: '<?php echo Yii::$app->request->baseUrl . '/live-stream/update-live-default?id=' . $model->id ?>',
            type: 'post',
            data: {
                link_avatar: $('[name="link_avatar[]"]').val(),
                enable_record: $('[name="TblLivestream[enable_record]"]').val(),
                title: $('#tbllivestream-title').val(),
                description: $('#tbllivestream-description').val(),
                category: $('#tbllivestream-category').val(),
                profile: $('#tbllivestream-profile').val(),
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                if (data) {
                    // alert("Edit live stream succes !!!");
                    location.reload();
                    // $( ".livestream-title div:last-child" ).text($('#tbllivestream-title').val());
                    // $( ".livestream-category div:last-child" ).text($('#tbllivestream-category').val() == 1  ? 'Sport' : ('#tbllivestream-category').val() == 2 ? 'Travel' : 'VLOG' );
                    // $( ".livestream-profile div:last-child" ).text($('#tbllivestream-profile').val() ==1 ? '1080p, 720p, 480p, 360p' : $('#tbllivestream-profile').val() ==2 ? '720p, 480p, 360p' : $('#tbllivestream-profile').val() ==3 ? '480p, 360p' : 'live' );
                } else {
                    alert("Edit live stream fail !!!")
                }
            }
        });
    }

    function checkHaveChannel(channel_id) {
        if (!channel_id) {
            // alert('You have not channel !!!');

            // $('.modal-child').style.display = 'block';
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                $("#success-alert").slideUp(500);
            });
            setTimeout(function () {
                document.getElementById('exampleModal-1').style.display = 'block'
            }, 3000);


        } else {
            $('.stream-key :last-child').show();
            $('.stream-url :last-child').show();
        }
    }

    function changeStatusLive(status, stream_id) {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . '/live-stream/change-status-live' ?>',
            type: 'post',
            data: {
                status: status,
                stream_id: stream_id,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                if (data) {
                    $(".status-livestream").text(status);
                }
            }
        });
    }

    function reportComment(blockId, commentId, livestreamId, streamerId, userId) {
        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . '/live-stream/report-comment' ?>',
            type: 'post',
            data: {
                blockId: blockId,
                commentId: commentId,
                livestreamId: livestreamId,
                streamerId: streamerId,
                userId: userId,
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
            },
            success: function (data) {
                if (data == 13) {
                    $('.' + commentId).hide();
                }
            }
        });
    }

    function copyValueStreamKey() {
        navigator.clipboard.writeText($('#stream-key-1').text());
        alert("Copied the text");
    }

    function copyValueStreamUrl() {
        navigator.clipboard.writeText($('#stream-url-1').text());
        alert("Copied the text");
    }

    $(document).ready(function () {
        $(".menu-toggler").click();

        $(".div-stop").click(function () {
            stopLiveStream(<?= $model->id ?>);
        });
        $(".selectStatus").change(function () {
            changeStatusLive($(".selectStatus").val(), <?= $model->id ?>);
        });
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })
        $(".green").click(function () {
            editLiveDefault();
            $('.close').click();
        })
        checkHaveChannel(<?= $channel->id ?>)

        $('.btn-save-channel').click(function () {
            createChannel($('#tblchannel-name').val(), $('#tblchannel-description').val());
            $('.close').click();
        })

    });
    var player = videojs('hls-example');
    player.play();
    var streammer_ID = <?php echo $userID?>;
    var roomId = <?php echo $roomId ?>;
    var userName = '<?php echo $userName?>';
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "-"
        + (currentdate.getMonth() + 1) + "-"
        + currentdate.getDate() + " "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();
</script>


<?php LivestreamAsset::register($this); ?>





