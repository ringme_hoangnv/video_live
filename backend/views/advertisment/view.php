<?php

use awesome\backend\widgets\AwsBaseHtml;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Advertisment */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertisments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row advertisment-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'name',
                        'position',
                        [
                            'attribute' => 'link',
                            'value' => function ($model) {
                                return $model->link;
                            },
                            'format' => ['url'],
                        ],
                        [
                            'attribute' => 'image_path',
                            'value' => function ($model) {
                                return $model->image_path;
                            },
                            'format' => ['image', ['width' => '100%']],
                        ],
                        'start_time',
                        'end_time',
                        'created_at',
                        'created_by',
                        'updated_at',
                        'updated_by',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
