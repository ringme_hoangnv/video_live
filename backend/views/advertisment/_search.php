<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\AdvertismentSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default advertisment-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-6">
            <?= $form->field($model, 'name') ?>

        </div>
        <div class="col-md-6">

            <?php /*  $form->field($model, 'position')->widget(Select2::classname(), [
                'data' => Yii::$app->params['adv_position'],
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => 'Tất cả vị trí'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); */ ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
