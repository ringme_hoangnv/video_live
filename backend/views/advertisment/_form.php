<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use common\helpers\Helpers;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model backend\models\Advertisment */
/* @var $title string */
/* @var $form AwsActiveForm */
?>

<?php  $form = ActiveForm::begin(); ?>

    <div class="portlet light portlet-fit portlet-form bordered advertisment-form">

        <div class="portlet-body">
            <div class="form-body row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

                    <?= $form->field($model, 'position')->dropDownList(
                        Yii::$app->params['adv_position']
                        //['prompt'=> 'Chọn vị trí']
                    ) ?>
                    <?= $form->field($model, 'priority')->textInput(['maxlength' => 20]) ?>
                    <?= $form->field($model, 'is_active')->checkBox() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'link')->textInput(['maxlength' => 1000]) ?>


                    <?=$this->render('/common/_slim_image_field', [
                        'fieldName' => 'image_path',
                        'itemName' => Inflector::camel2id(StringHelper::basename($model::className())), // Vi du: adv, post, post-category de luu rieng tung folder
                        'fieldLabel' => Yii::t('backend', 'Ảnh đại diện (kích thước: 1920x800 px)'),
                        'model' => $model,
                        'dataMinSize' => '0,0',
                        'dataSize' => '1920,800',
                        'dataForceSize' => '',
                        'dataRatio' => '1920:800',
                    ])?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <?php
                    echo $form->field($model, 'start_time', [
                        'template' => '{label}{input}{error}{hint}',
                    ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd hh:ii:ss'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $form->field($model, 'end_time', [
                        'template' => '{label}{input}{error}{hint}',
                    ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd hh:ii:ss'
                        ]
                    ]);
                    ?>
                </div>




            </div>
        </div>
        <div class="portlet-title">
            
            <div class="">
                <?=  Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-info']) ?>
                &nbsp;&nbsp;
                <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default">
                    <i class="fa fa-angle-left"></i> Quay lại
                </a>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>