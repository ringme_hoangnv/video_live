<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Advertisment */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Advertisment',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Advertisments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row advertisment-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
