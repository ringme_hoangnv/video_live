<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblReportDay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-report-day-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'streamer_id')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount_star')->textInput() ?>

    <?= $form->field($model, 'amount_money')->textInput() ?>

    <?= $form->field($model, 'percent_extract')->textInput() ?>

    <?= $form->field($model, 'money_extract')->textInput() ?>

    <?= $form->field($model, 'amount_transaction')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'report_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
