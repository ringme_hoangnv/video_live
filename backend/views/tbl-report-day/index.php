<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblReportDaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Report Days';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-report-day-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tbl Report Day', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'streamer_id',
            'status',
            'description',
            //'amount_star',
            //'amount_money',
            //'percent_extract',
            //'money_extract',
            //'amount_transaction',
            //'created_at',
            //'report_date',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, TblReportDay $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
