<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblReportDay */

$this->title = 'Update Tbl Report Day: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Report Days', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-report-day-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
