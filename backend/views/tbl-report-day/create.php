<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblReportDay */

$this->title = 'Create Tbl Report Day';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Report Days', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-report-day-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
