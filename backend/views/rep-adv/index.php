<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RepAdvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Rep Advs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row rep-adv-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>


            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    //Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>
                    <?php /* <div class="summary-info" >

                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Watching time');?>:
                            </label> <?= number_format($summaryResults[0]['total_watching_time'])?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Play');?>:
                            </label> <?= number_format($summaryResults[0]['total_play'])?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Click');?>:
                            </label> <?= number_format($summaryResults[0]['total_click'])?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    */ ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                                'options' => [
                                    'class' => 'form-control  form-control-sm',
                                ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],
                            'adv_id',
                            [
                                'label' => Yii::t('backend', 'Adv'),
                                'format' => 'raw',
                                'attribute' => 'adv_id',
                                'value' => function ($model) {
                                    if ($model->adv_id) {
                                        $adv = \backend\models\VcsAdvertisement::findOne($model->adv_id);
                                        if ($adv) {
                                            return $adv->title;
                                        }
                                    }
                                }
                            ],
//                            'video_id',
//                            [
//                                'label' => Yii::t('backend', 'Video Title'),
//                                'format' => 'raw',
//                                'attribute' => 'video_id',
//                                'value' => function ($model) {
//                                    if ($model->video_id) {
//                                        $video = \backend\models\VideoItem::findOne($model->video_id);
//                                        if ($video) {
//                                            return $video->video_title;
//                                            //return '<a href="' . \yii\helpers\Url::to(['video-item/update', 'id' => $model->video_id]) . '">' . $video->video_title . '</a>';
//                                        }
//                                    }
//                                }
//                            ],
                            //'total_watching_time:integer',
                            'total_view:integer',
                            'total_click:integer',


                        ],
                    ]); ?>

                    <?php
                    //Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="detail-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>