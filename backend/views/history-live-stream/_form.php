<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HistoryLiveStream */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="history-live-stream-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'stream_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hls_play_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'time_event_start')->textInput() ?>

    <?= $form->field($model, 'time_event_finish')->textInput() ?>

    <?= $form->field($model, 'vod_play_back')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'stream_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'profile')->textInput() ?>

    <?= $form->field($model, 'total_comment')->textInput() ?>

    <?= $form->field($model, 'total_like')->textInput() ?>

    <?= $form->field($model, 'total_share')->textInput() ?>

    <?= $form->field($model, 'total_view')->textInput() ?>

    <?= $form->field($model, 'enable_record')->checkbox() ?>

    <?= $form->field($model, 'time_finish')->textInput() ?>

    <?= $form->field($model, 'time_start')->textInput() ?>

    <?= $form->field($model, 'category')->textInput() ?>

    <?= $form->field($model, 'rtmp_push')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stream_alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_avatar')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
