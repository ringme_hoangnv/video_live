<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HistoryLiveStream */

$this->title = 'Create History Live Stream';
$this->params['breadcrumbs'][] = ['label' => 'History Live Streams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="history-live-stream-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
