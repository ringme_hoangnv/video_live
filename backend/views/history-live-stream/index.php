<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HistoryLiveStreamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'History Live Streams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="history-live-stream-index row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'title',
                            'description:ntext',
                            'stream_key',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
