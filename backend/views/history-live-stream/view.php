<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HistoryLiveStream */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'History Live Streams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="history-live-stream-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:ntext',
            'status',
            'stream_key',
            'hls_play_link',
            'time_event_start',
            'time_event_finish',
            'vod_play_back',
            'created_at',
            'updated_at',
            'stream_id',
            'user_id',
            'profile',
            'total_comment',
            'total_like',
            'total_share',
            'total_view',
            'enable_record:boolean',
            'time_finish',
            'time_start',
            'category',
            'rtmp_push',
            'stream_alias',
            'link_avatar',
        ],
    ]) ?>

</div>
