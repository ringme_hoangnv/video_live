<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RepVideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Videos Report');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row rep-video-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>

            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    //Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>
                    <?php /*
                    <div class="summary-info" >

                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'View');?>:
                            </label> <?= number_format($summaryResults[0]['total_view'])?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Like');?>:
                            </label> <?= number_format($summaryResults[0]['total_like'])?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Follow');?>:
                            </label> <?= number_format($summaryResults[0]['total_follow'])?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Unfollow');?>:
                            </label> <?= $summaryResults[0]['total_unfollow'];?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Comment');?>:
                            </label> <?= $summaryResults[0]['total_comment'];?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Like comment');?>:
                            </label> <?= $summaryResults[0]['total_like_comment'];?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Watching time');?>:
                            </label> <?= $summaryResults[0]['total_watching_time'];?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Play');?>:
                            </label> <?= $summaryResults[0]['total_play'];?>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <label>
                                <?= Yii::t('backend', 'Average watching time');?>:
                            </label> <?= $summaryResults[0]['total_play'];?>
                            <?php $avg = (intval($summaryResults[0]['total_play']) > 0)? number_format(intval($summaryResults[0]['total_watching_time']) / intval($summaryResults[0]['total_play'])): 0 ?>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                    */ ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                                'options' => [
                                    'class' => 'form-control  form-control-sm',
                                ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],

                            //'report_date',
                            'video_id',
                            [
                                'label' => Yii::t('backend', 'Video Title'),
                                'format' => 'raw',
                                'attribute' => 'video_id',
                                'value' => function ($model) {
                                    if ($model->video_id) {
                                        $video = \backend\models\VideoItem::findOne($model->video_id);
                                        if ($video) {
                                            return $video->video_title;
                                            //return '<a href="' . \yii\helpers\Url::to(['video-item/update', 'id' => $model->video_id]) . '">' . $video->video_title . '</a>';
                                        }
                                    }
                                }
                            ],
                            'total_view:integer',
                            'total_like:integer',
//                            'total_follow:integer',
//                            'total_unfollow:integer',
                            'total_comment:integer',
                            ///'total_like_comment:integer',
//                            'total_watching_time',
//                            'total_play',
                            [
                                'attribute' => 'average_watching_time',
                                'value' => function($model) {
                                    return (intval($model->total_play) > 0)? number_format(intval($model->total_watching_time) / intval($model->total_play)): 0;
                                },

                            ],
                        ],
                    ]); ?>

                    <?php
                    //Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
