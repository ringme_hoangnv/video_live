<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model backend\models\StaticPage */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php  $form = \kartik\form\ActiveForm::begin(); ?>

    <div class="portlet light portlet-fit portlet-form bordered static-page-form">
        <div class="form-actions">
            <?=  Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-info']) ?>
            &nbsp;
            <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default">
                <i class="fa fa-angle-left"></i> <?=  Yii::t('backend', 'Back') ?>                </a>

            <?= $form->field($model, 'lang', [
                'options' => ['class' => 'form-group lang-field']
            ])->dropDownList(Yii::$app->params['content_languages'], [
                'class' => 'form-control lang-input ',
                'id' => 'lang-input',
                'href' => \yii\helpers\Url::to(['static-page/update', 'id' => $model->id])
            ])->label(false) ?>
        </div>
        <div class="portlet-body">
            <div class="form-body row">
                    <div class="col-md-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
        <?=
        $form->field($model, 'published_at', [
            'template' => '{label}{input}{error}{hint}',
        ])->widget(\kartik\widgets\DateTimePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd hh:ii:ss'
            ]
        ]);
        ?>

    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'short_desc')->textarea(['rows' => 4]) ?>
        <?= $form->field($model, 'is_active')->checkbox() ?>
    </div>


                <?php if (!$model->isNewRecord): ?>
    <div class="col-md-6">


            <?= $form->field($model, 'slug')->textInput(['maxlength' => 200])->hint('Dùng để hiển thị trên URL: ví dụ: https://vietace.vn/'.$model->lang.'/p/'. '<b>'. $model->slug. '/'. $model->id.'</b>')->label(Yii::t('backend', 'Slug')) ?>




    </div>
                <?php endif; ?>

                <div class="col-md-12">
                    <?=
                    \navatech\roxymce\widgets\RoxyMceWidget::widget([
                        'model'     => $model,
                        'attribute' => 'body',
                        'clientOptions' => [
                            'title' => Yii::t('backend', 'File management'),
                            'relative_urls' => false,
                            'remove_script_host' => true,
                            'document_base_url' =>  "/",
                            'convert_urls' => true,
                        ]
                    ]);
                    ?>

                </div>

                <br />
                <br />
                <div class="col-md-6">
                    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => 255]) ?>
                    <?= $form->field($model, 'seo_title')->textInput(['maxlength' => 200]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'seo_description')->textarea(['maxlength' => 255, 'rows' => 3]) ?>

                </div>
            </div>
        </div>
        <div class="portlet-title row">
            
            <div class="form-actions">
                <?=  Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-info']) ?>
                &nbsp;
                <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default">
                    <i class="fa fa-angle-left"></i> <?=  Yii::t('backend', 'Back') ?>                </a>
            </div>
        </div>
    </div>

<?php \kartik\form\ActiveForm::end(); ?>
