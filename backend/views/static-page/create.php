<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\StaticPage */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Static Page',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Static Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row static-page-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
