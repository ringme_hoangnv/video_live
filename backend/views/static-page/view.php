<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\StaticPage */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Static Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row static-page-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!Yii::$app->request->isAjax): ?>            <div class="portlet-title">

                <div class="">
                    <?=                     Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                        ['class' => 'btn btn-info  btn-sm'])
                    ?>
                    <?=                     Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-transparent red  btn-sm',
                        'data' => [
                            'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
            <?php endif; ?>            <div class="portlet-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                            'id',
                            
            'title',
            'short_desc:ntext',
            
            [
                'attribute' => 'body', 
                'format' => 'raw',
            ],
            'published_at',
            'is_active',
            'created_at',
            'updated_at',
            'slug',
            'seo_title',
            'seo_description',
            'seo_keywords',
                ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
