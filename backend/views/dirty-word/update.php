<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DirtyWord */

$this->title = 'Update Dirty Word: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dirty Words', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update' . ' ' . $model->id;
?>
<div class="row dirty-word-update">
    <div class="col-md-12">

    <?= $this->render('_form', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>
