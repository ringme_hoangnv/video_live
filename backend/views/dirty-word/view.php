<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\DirtyWord */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Dirty Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row dirty-word-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>            <div class="portlet-title">

                <div class="">
                    <?=                     Html::a('Update', ['update', 'id' => $model->id],
                        ['class' => 'btn btn-info  btn-sm'])
                    ?>
                    <?=                     Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-transparent red  btn-sm',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </div>
            </div>
            <?php endif; ?>            <div class="portlet-body">
                <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                            'id',
            'word',
            'updated_time',
                ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
