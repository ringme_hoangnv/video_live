<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DirtyWord */

$this->title = 'Create Dirty Word';
$this->params['breadcrumbs'][] = ['label' => 'Dirty Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row dirty-word-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
