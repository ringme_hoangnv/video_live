<?php

use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;
use backend\assets\LivestreamAsset;
use yii\widgets\DetailView;
use practically\chartjs\Chart;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\TblLivestream */

//$this->title = 'Overview livestream';
//$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/css/custom-overview.css');

?>
<div class="channel-analytics-data">
    <div class="title-analytic-data">
        <h2>Channel analytics data:</h2>
    </div>
    <div class="analytic">
        <div class="channel panel panel-body ">
            <h3>Channel</h3>
            <p>Channel name: <?php echo $data_channel->name ?> </p>
            <p>Total follow: <?php echo $data_channel->total_follow ?> </p>
            <p>Total time live: <?php echo $data_channel->total_time_live ?></p>
            <p>Total video: <?php echo $data_channel->total_view ?> </p>
            <p>Total livestream: <?php echo $data_channel->total_livestream ?> </p>
            <!--            <p>aaaaaaaaaaa</p>-->

        </div>
        <div class="livestream panel panel-body ">
            <h3>Livestream</h3>
            <!--            <p>total view: --><?php //echo $data_livestream->total_view?><!-- </p>-->
            <!--            <p>total like: --><?php //echo $data_livestream->total_like?><!-- </p>-->
            <!--            <p>total comment: --><?php //echo $data_livestream->total_comment?><!-- </p>-->
            <p>total view: <?php echo $dataProviderlivestream->total_view ?> </p><br>
            <p>total like: <?php echo $dataProviderlivestream->total_like ?> </p><br>
            <p>total comment: <?php echo $dataProviderlivestream->total_comment ?> </p><br>

        </div>
        <!--        <div class="violation">-->
        <!--            <h3>Violations of the channel</h3>-->
        <!--            <p>Violation: --><?php //echo $data_violation->title?><!--</p>-->
        <!--        </div>-->
    </div>
</div>

<div class="trend-info">
    <div class="lastest-livestream panel panel-body">
        <h3>Lastest live</h3>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'title',
                    'options' => ['style' => 'width:15%']// For TD
                ],
                [
                    'attribute' => 'link_avatar',
                    'value' => function ($model) {
                        return $model->getImagePath();
                    },
                    'format' => ['image', ['width' => '100%']],
                    'filter' => false,
                    'options' => ['style' => 'width:13%; height:160px']
                ],
//
//                ['class' => 'yii\grid\ActionColumn', 'options' => ['style' => 'width:5%']],

            ],
        ]); ?>

    </div>
    <div class="top-livestream panel panel-body">
        <h3>Top livestream</h3>
        <?= GridView::widget([
            'dataProvider' => $dataProviderTopLivestream,
            'columns' => [
                [
                    'attribute' => 'title',
                    'options' => ['style' => 'width:15%']// For TD
                ],
                [
                    'attribute' => 'total_view',
                    'options' => ['style' => 'width:15%']// For TD
                ],
                [
                    'attribute' => 'total_like',
                    'options' => ['style' => 'width:15%']// For TD
                ],
                [
                    'attribute' => 'total_comment',
                    'options' => ['style' => 'width:15%']// For TD
                ],

//
//                ['class' => 'yii\grid\ActionColumn', 'options' => ['style' => 'width:5%']],

            ],
        ]); ?>
    </div>
</div>



