<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model backend\models\VcsCategory */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php  $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="portlet light portlet-fit portlet-form bordered vcs-category-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'cate_name')->textInput(['maxlength' => 500]) ?>
                <?php
                if (!$model->isNewRecord) {
                    $statusArr = \backend\models\VcsCategory::getActiveDefineArr();
                    unset($statusArr[\backend\models\VcsCategory::DELETED_STATUS]);
                    echo $form->field($model, 'actived')->dropDownList($statusArr, [
                        'disabled' => ($model->isNewRecord) ? true : false,
                    ]);
                }
                ?>

                <?= $form->field($model, 'display_order')->textInput() ?>
                <?= $form->field($model, 'for_user')->checkbox() ?>
                <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
            </div>

            <div class="col-md-6">

                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'icon_image',
                    'itemName' => 'category', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Icon'),
                    'dataMinSize' => '200,200',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '200:200',
                    'model' => $model,
                    'dataWillRemove' => 'iconWillChange',
                    'dataWillSave' => 'iconWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 200 x 200px(.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>

                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'header_banner',
                    'itemName' => 'category', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Header banner'),
                    'dataMinSize' => '1080,368',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '1080:368',
                    'model' => $model,
                    'dataWillRemove' => 'headerWillChange',
                    'dataWillSave' => 'headerWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 1080 x 368px (.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>
            </div>


            <div class="clearfix"></div>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>

