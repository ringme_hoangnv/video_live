<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\VcsCategory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Vcs Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row vcs-category-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'cate_name',
                        [
                            'attribute' => 'icon_image',
                            'value' => function ($model) {
                                return $model->getIconImageUrl();
                            },
                            'format' => ['image', ['height' => '80', 'width' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:100px'],
                        ],
                        [
                            'attribute' => 'header_banner',
                            'value' => function ($model) {
                                return $model->getHeaderBannerUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],
                        [
                            'attribute' => 'actived',
                            'value' => function ($model) {
                                return $model->getActiveName();
                            },

                        ],
                        'display_order',
                        'slug',
                        'description:ntext',
                        'updated_at',
                        'created_at',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
