<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TblChannel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-channel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['maxlength' => true,'rows' => 8]) ?>
    <div class="form-group">
       <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-save-channel']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
