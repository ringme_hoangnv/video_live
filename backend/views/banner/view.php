<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Banner */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row banner-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a('Update', ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'bn_name',

                        'bn_position',
                        'bn_action',
                        [
                            'attribute' => 'bn_image_url',
                            'value' => function ($model) {
                                return $model->getBnImageUrlUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],

                        [
                            'attribute' => 'actived',
                            'value' => function ($model) {
                                return $model->getActiveName();
                            },
                            'label' => Yii::t('backend', 'Status')
                        ],
                        [
                            'attribute' => 'position',
                            'value' => function ($model) {
                                return $model->getPositionName();
                            },
                            'label' => Yii::t('backend', 'Position')
                        ],
                        'content_id',
                        'bn_media_url:url',
                        'bn_link',
                        'display_order',
                        'publish_time',
                        'created_at',
                        'updated_at',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
