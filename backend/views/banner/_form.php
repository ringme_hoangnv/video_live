<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model backend\models\Banner */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered banner-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'bn_name')->textInput(['maxlength' => 255]) ?>
                <?php
                echo $form->field($model, 'publish_time', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-6">

                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'bn_image_url',
                    'itemName' => 'banner', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Banner image'),
                    'dataMinSize' => '0,0',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '',
                    'model' => $model,
                    'dataWillRemove' => 'bannerWillChange',
                    'dataWillSave' => 'bannerWillChange',
                ]) ?>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'display_order')->textInput() ?>

            </div>


            <div class="col-md-6">

                <?php
                $statusArr = \backend\models\Banner::getActiveDefineArr();
                unset($statusArr[\backend\models\Banner::DELETED_STATUS]);
                echo $form->field($model, 'actived')->dropDownList($statusArr) ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'bn_position')->dropDownList(\backend\models\Banner::getBannerPositionArr()) ?>

            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'bn_action')->dropDownList(\backend\models\Banner::getBannerActionArr()) ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">

                <?= $form->field($model, 'bn_link')->textInput(['maxlength' => 1000]) ?>
            </div>





            <div class="col-md-6">
                <?= $form->field($model, 'content_id')->textInput(['maxlength' => 100]) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'bn_media_url')->textInput(['maxlength' => 1000]) ?>

            </div>








        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= 'Back' ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>