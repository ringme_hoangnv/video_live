<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AppUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Customer info');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row app-user-index">
    <div class="col-md-12">

        <?php echo $this->render('_search', ['model' => $searchModel]); ?>


        <div class="portlet-body">
            <div class="table-container">
                <?php
                Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterSelector' => 'select[name="per-page"]',
                    'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                            'options' => [
                                'class' => 'form-control  form-control-sm',
                            ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        //['class' => 'yii\grid\SerialColumn'],
                        'username',
                        [
                            'attribute' => 'active',
                            'value' => function($object) {
                                return $object->getActiveName();
                            }
                        ],
                        'name',
                        'client_type',
                        'revision',
                        'created_at',

                        // 'last_online',
                        // 'last_offline',
                        // 'last_avatar',
                        // 'status:ntext',
                        // 'name',
                        // 'client_type',
                        // 'num_unreaded_msg',
                        // 'gender',
                        // 'notification_view',
                        // 'newuser_notification',
                        // 'birthday',
                        // 'revision',
                        // 'app_version',
                        // 'os_version',
                        // 'domain',
                        // 'last_seen',
                        // 'num_msg_sended',
                        // 'send_msg_date',
                        // 'device_name',
                        // 'smsfeed_time',
                        // 'birthday_processed_time',
                        // 'password_expired',
                        // 'provision_profile',
                        // 'active_count',
                        // 'country_code',
                        // 'device_id',
                        // 'language_code',
                        // 'facebook_id',
                        // 'permission',
                        // 'uuid',
                        // 'last_contact',
                        // 'desk_revision',
                        // 'desk_version',
                        // 'desk_os',
                        // 'desk_device_id',
                        // 'desktop',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['style' => 'width: 110px;', 'class' => 'head-actions'],
                            'contentOptions' => ['class' => 'row-actions'],
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->username], [
                                        'class' => '',
                                        'data-target' => '#detail-modal',
                                        'data-toggle' => "modal"
                                    ]);
                                },
                            ]
                        ],
                    ],
                ]); ?>

                <?php
                Pjax::end();
                ?>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="detail-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>