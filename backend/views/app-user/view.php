<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AppUser */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'App Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row app-user-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [

                        'username',
                        'created_at',
                        [
                            'attribute' => 'active',
                            'value' => function($object) {
                                return $object->getActiveName();
                            }
                        ],
                        'name',
                        'gender',
                        'birthday',
                        'client_type',
                        'app_version',
                        'revision',
                        'os_version',
                        'device_id',
                        'device_name',
                        'country_code',
                        'language_code',
//
//
//                        'username',
//                        'created_at',
//
//
//                        'token:ntext',
//
//                        'token_expired',
//                        'last_online',
//                        'last_offline',
//                        'last_avatar',
//                        'status:ntext',
//                        'name',
//                        'client_type',
//                        'num_unreaded_msg',
//                        'gender',
//                        'notification_view',
//                        'newuser_notification',
//                        'birthday',
//                        'revision',
//                        'app_version',
//                        'os_version',
//                        'domain',
//                        'last_seen',
//                        'num_msg_sended',
//                        'send_msg_date',
//                        'device_name',
//                        'smsfeed_time',
//                        'birthday_processed_time',
//                        'password_expired',
//                        'provision_profile',
//                        'active_count',
//                        'country_code',
//                        'device_id',
//                        'language_code',
//                        'facebook_id',
//                        'permission',
//                        'uuid',
//                        'last_contact',
//                        'desk_revision',
//                        'desk_version',
//                        'desk_os',
//                        'desk_device_id',
//                        'desktop',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
