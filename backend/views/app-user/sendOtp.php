<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Post */

$this->title = Yii::t('backend', 'Send OTP', [
]);

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row send-otp">
    <div class="col-md-12">

        <form method="post" action="<?= \yii\helpers\Url::to(['app-user/send-otp']) ?>" style="padding: 15px 20px;">
            <?php
            $msg = Yii::$app->session->getFlash('success', $message);
            if ($msg) :
            ?>
                <div class="alert alert-warning" role="alert">
                    <?= $msg; ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif;?>
            <div class="form-group">
                <label for="exampleInputEmail1"><?= Yii::t('backend', 'Phone number') ?></label>
                <input type="text" maxlength="15" class="form-control" id="username" name="username" placeholder="<?= Yii::t('backend', 'Enter phone number') ?>">
                <small id="usernameHelp" class="form-text text-muted"></small>
            </div>
            <input type="hidden" value="<?= Yii::$app->request->csrfToken ?>" name="<?= Yii::$app->request->csrfParam ?>" />
            <button type="submit" class="btn btn-primary"><?= Yii::t('backend', 'Send OTP') ?></button>
        </form>
    </div>
</div>
