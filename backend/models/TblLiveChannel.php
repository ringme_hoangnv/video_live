<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tbl_live_channel".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $username
 * @property int|null $user_id
 * @property int|null $total_follow
 * @property int|null $total_time_live
 * @property int|null $total_livestream
 * @property int|null $total_view
 * @property int|null $status
 */
class TblLiveChannel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_live_channel';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db1');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'total_follow', 'total_time_live', 'total_livestream', 'total_view', 'status'], 'integer'],
            [['name', 'username'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'username' => 'Username',
            'user_id' => 'User ID',
            'total_follow' => 'Total Follow',
            'total_time_live' => 'Total Time Live',
            'total_livestream' => 'Total Livestream',
            'total_view' => 'Total View',
            'status' => 'Status',
        ];
    }
}
