<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RepCate;

/**
 * RepCateSearch represents the model behind the search form about `backend\models\RepCate`.
 */
class RepCateSearch extends RepCate
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cate_id', 'total_view', 'total_like', 'total_comment', 'total_like_comment'], 'integer'],
            [['report_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepCate::find()
            ->select('cate_id, SUM(total_view) as total_view, SUM(total_like) total_like, SUM(total_comment) total_comment, SUM(total_like_comment) total_like_comment')
            ->groupBy('cate_id')
            ;

        if (!Yii::$app->request->get('sort')) {
            $query->orderBy('total_view asc');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['total_view' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->report_date != Yii::t('backend', 'All') && strpos($this->report_date, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->report_date, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'report_date', $request_times[0], $request_times[1]]);
        }

        $query->andFilterWhere([
            'cate_id' => $this->cate_id,
        ]);

        return $dataProvider;
    }
}
