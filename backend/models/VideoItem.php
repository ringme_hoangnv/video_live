<?php

namespace backend\models;

use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

class VideoItem extends \common\models\VideoItemBase {


    public function rules()
    {
        $rules = [
            [['video_title', 'cate_id', 'channel_id', 'video_desc',], 'required'],
            [['cate_id', 'channel_id', 'video_time', 'actived', 'is_new', 'is_hot', 'total_views', 'total_likes', 'total_shares', 'total_comments', 'has_live', 'resolution', 'is_adaptive', 'encode_type'], 'integer'],
            [['video_desc', 'note', 'belong'], 'string'],
            [['publish_time', 'first_publish_time', 'created_at', 'updated_at'], 'safe'],
            [['video_title', 'image_thumb', 'image_small', 'slug', 'adaptive_path', 'adaptive_resolution'], 'string', 'max' => 500],
            [['actived'], 'validateStatus'],

        ];

        if ($this->isNewRecord) {
            $rules = array_merge($rules, [
                [['video_media'], 'file', 'skipOnEmpty' => false, 'extensions' => 'mp4', 'maxSize' => 512000000, 'tooBig' => Yii::t('backend', 'File is too big, maximun is 512Mb') ],
                ['video_image', 'required', 'message' => Yii::t('backend', 'Please upload video image')]
            ]);
        } else {
            $rules = array_merge($rules, [
                [['video_media'], 'file', 'skipOnEmpty' => true, 'extensions' => 'mp4',],
            ]);
        }
        return $rules;
    }

    /**
     * updload file video
     * @return bool
     */
    public function uploadVideoMedia()
    {

        if ($this->validate() && $this->video_media) {

            $absSavePath = $this->getAbsVideoMediaPath();

            $fileName = md5($this->id.'_'. time(). Inflector::camel2id($this->video_media->name)). '.'. $this->video_media->extension;
//            $filePathInfo = pathinfo($fileName);
//            if (empty($filePathInfo['filename'])) {
//                $fileName = $this->id.'_'. time(). '.'. $this->video_media->extension;
//            }

            $fullFilePath = Yii::$app->params['media_path'] . $absSavePath;

            if (!file_exists($fullFilePath)) {
                FileHelper::createDirectory($fullFilePath);
            }

            $this->video_media->saveAs($fullFilePath . DIRECTORY_SEPARATOR . $fileName);
            $this->video_media = $absSavePath . $fileName;
            $this->actived = self::WAIT_CONVERT_STATUS;

            $this->update(false, ['video_media', 'actived']);

            return true;
        } else {

            return false;
        }
    }

    public function getAbsVideoMediaPath() {
        return '/cms_upload/video/'. date('Y/m/d/');
    }
    public function getAbsVideoImagePath() {
        return '/cms_upload/img/'. date('Y/m/d/');
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cate_id' => 'Cate',
            'channel_id' => 'Channel',
            'video_title' => 'Video Title',
            'video_desc' => 'Video Desc',
            'video_media' => 'Video Media',
            'video_image' => 'Video Image',
            'image_thumb' => 'Image Thumb',
            'image_small' => 'Image Small',
            'video_time' => 'Video Time',
            'actived' => 'Status',
            'is_new' => 'Is New',
            'is_hot' => 'Is Hot',
            'publish_time' => 'Publish Time',
            'first_publish_time' => 'First Publish Time',
            'total_views' => 'Total Views',
            'total_likes' => 'Total Likes',
            'total_shares' => 'Total Shares',
            'total_comments' => 'Total Comments',
            'slug' => 'Slug',
            'has_live' => 'Has Live',
            'resolution' => 'Resolution',
            //'aspec_ratio' => 'Aspec Ratio',
            'is_adaptive' => 'Is Adaptive',
            'adaptive_path' => 'Adaptive Path',
            'adaptive_resolution' => 'Adaptive Resolution',
            'encode_type' => 'Encode Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'note' => 'Note',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->first_publish_time == null && $this->actived > 10) {
            $this->first_publish_time = date('Y-m-d H:i:s');
            $this->publish_time = date('Y-m-d H:i:s');
        } elseif ($this->actived > 10) {
            $this->publish_time = date('Y-m-d H:i:s');
        }

        if (!$this->isNewRecord) {
            $noChangeArr = [
                'total_views',
                'total_likes',
                'total_shares',
                'total_comments',
                'resolution',
                //'aspec_ratio',
                'is_adaptive',
                'adaptive_path',
                'adaptive_resolution',
                'belong',
                'encode_type',
            ];

            foreach ($noChangeArr as $field) {
                $this->$field = $this->getOldAttribute($field);
            }
        } else {
            // Tao moi
            $this->belong = 'cms';
            $this->total_likes = 0;
            $this->total_views = 0;
            $this->total_comments = 0;
            $this->total_shares = 0;
            $this->is_adaptive = null;
            $this->encode_type = null;
            $this->actived = self::WAIT_CONVERT_STATUS;
        }

        // neu video o trang thai cho convert --> ko cho chuyen trang thai tru trang thai XOA
        $oldActived = $this->getOldAttribute('actived');
        if (($oldActived == self::WAIT_CONVERT_STATUS || $oldActived == 2 || $oldActived == 3) && $this->actived != self::DELETED_STATUS) {
            $this->actived = $oldActived;
        }

        // Video cua user, ko sua channel_id
        if ($this->belong != 'cms') {
            $this->channel_id = $this->getOldAttribute('channel_id');
        }

        return parent::beforeSave($insert);
    }

    public function validateStatus($attribute, $params) {

        $oldStatus = $this->getOldAttribute('actived');
        // Ko cho chuyen ve trang thai 2, 3
        if (!in_array($oldStatus, [2,3]) && in_array($this->actived, [2,3])) {
            $this->addError($attribute, Yii::t('backend', 'Cannot update the status, please choose other status'));
            return false;
        }

        if (!$this->isNewRecord && !in_array($oldStatus, [10, 11, 12]) && in_array($this->actived, [10, 11, 12])) {

            // Truong hop duyet --> check validate
            // check category & channel co dang active ko?
            $cate = $this->category;
            $channel = $this->channel;

            if (!$cate || !in_array($cate->actived, [2,3])) {
                $this->addError($attribute, Yii::t('backend', 'Category "{name}" is not activated', ['name' => $cate->cate_name]));
                return false;
            }
            if (!$channel || !in_array($channel->actived, [10,11,12])) {
                $this->addError($attribute, Yii::t('backend', 'Channel "{name}" is not activated', ['name' => $channel->channel_name]));
                return false;
            }
        }

        return true;
    }

}