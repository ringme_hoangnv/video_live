<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RepActivate;

/**
 * RepActivateSearch represents the model behind the search form about `backend\models\RepActivate`.
 */
class RepActivateSearch extends RepActivate
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_active', 'channel_active'], 'integer'],
            [['report_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepActivate::find()
            ->select('sum(user_active) user_active, sum(channel_active) channel_active')
            ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['user_active' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([

            'user_active' => $this->user_active,
            'channel_active' => $this->channel_active,
        ]);
        
        if($this->report_date != Yii::t('backend', 'All') && strpos($this->report_date, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->report_date, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'report_date', $request_times[0], $request_times[1]]);
        }


        return $dataProvider;
    }
}
