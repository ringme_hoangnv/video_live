<?php

namespace backend\models;

use common\models\db\RepChannelDB;
use Yii;

class RepChannel extends RepChannelDB {

    public $channel_name;

    public function getChannel()
    {
        return $this->hasOne(VcsChannel::className(), ['id' => 'channel_id']);
    }

}