<?php

namespace backend\models;

use Yii;

class Banner extends \common\models\BannerBase {

    public function rules()
    {
        return [
            [['bn_name', 'bn_position', 'bn_action'], 'required'],
            [['bn_link', ], 'url'],
            [['actived', 'display_order'], 'integer'],
            [['publish_time', 'created_at', 'updated_at'], 'safe'],
            [['bn_name'], 'string', 'max' => 255],
            [['bn_image_url', 'bn_media_url', 'bn_link'], 'string', 'max' => 1000],
            [['bn_position', 'bn_action'], 'string', 'max' => 20],
            [['content_id'], 'string', 'max' => 100]
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'bn_name' => Yii::t('backend', 'Name'),
            'bn_image_url' => Yii::t('backend', 'Image Url'),
            'bn_position' => Yii::t('backend', 'Position'),
            'bn_action' => Yii::t('backend', 'Action'),
            'content_id' => Yii::t('backend', 'Content ID'),
            'bn_media_url' => Yii::t('backend', 'Media Url'),
            'bn_link' => Yii::t('backend', 'Banner Link'),
            'actived' => Yii::t('backend', 'Status'),
            'display_order' => Yii::t('backend', 'Display Order'),
            'publish_time' => Yii::t('backend', 'Publish Time'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
}