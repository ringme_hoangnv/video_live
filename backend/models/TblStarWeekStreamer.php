<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_star_week_streamer".
 *
 * @property int $id
 * @property int|null $amount_star
 * @property string|null $created_at
 * @property int|null $user_id
 * @property int|null $position
 * @property string|null $finished_at
 */
class TblStarWeekStreamer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_star_week_streamer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amount_star', 'user_id', 'position'], 'integer'],
            [['created_at', 'finished_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount_star' => 'Amount Star',
            'created_at' => 'Created At',
            'user_id' => 'User ID',
            'position' => 'Position',
            'finished_at' => 'Finished At',
        ];
    }
}
