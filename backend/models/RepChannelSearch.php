<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RepChannel;

/**
 * RepChannelSearch represents the model behind the search form about `backend\models\RepChannel`.
 */
class RepChannelSearch extends RepChannel
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id','channel_name', 'channel_id', 'total_view', 'total_like', 'total_follow', 'total_unfollow', 'total_comment', 'total_like_comment'], 'integer'],
            [['report_date', 'channel_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepChannel::find()
            ->alias('rc')
            ->select('rc.channel_id, c.channel_name channel_name, SUM(rc.total_view) total_view, SUM(rc.total_like) total_like, SUM(total_follow) total_follow, SUM(total_unfollow) total_unfollow, SUM(total_comment) total_comment, SUM(total_like_comment) total_like_comment')
            ->joinWith('channel c')
            ->groupBy('rc.channel_id')

            ;
        if (!Yii::$app->request->get('sort')) {
            $query->orderBy('channel_name asc');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->report_date != Yii::t('backend', 'All') && strpos($this->report_date, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->report_date, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'report_date', $request_times[0], $request_times[1]]);
        }
        $query->andFilterWhere(['channel_id' => $this->channel_id]);

        return $dataProvider;
    }
}
