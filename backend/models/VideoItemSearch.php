<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VideoItem;

/**
 * VideoItemSearch represents the model behind the search form about `backend\models\VideoItem`.
 */
class VideoItemSearch extends VideoItem
{
    public $showStatusFilter = true;

    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cate_id', 'channel_id', 'video_time', 'actived', 'is_new', 'is_hot', 'total_views', 'total_likes', 'total_shares', 'total_comments', 'has_live', 'resolution', 'is_adaptive', 'encode_type'], 'integer'],
            [['video_title', 'video_desc', 'video_media', 'video_image', 'image_thumb', 'image_small', 'publish_time', 'first_publish_time', 'slug', 'adaptive_path', 'adaptive_resolution', 'created_at', 'updated_at', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VideoItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cate_id' => $this->cate_id,
            'channel_id' => $this->channel_id,
            'video_time' => $this->video_time,
            'is_new' => $this->is_new,
            'is_hot' => $this->is_hot,
            'publish_time' => $this->publish_time,
            'first_publish_time' => $this->first_publish_time,
            'total_views' => $this->total_views,
            'total_likes' => $this->total_likes,
            'total_shares' => $this->total_shares,
            'total_comments' => $this->total_comments,
            'has_live' => $this->has_live,
            'resolution' => $this->resolution,
            'is_adaptive' => $this->is_adaptive,
            'encode_type' => $this->encode_type,
        ]);

        $query->andFilterWhere(['like', 'video_title', $this->video_title])
            ->andFilterWhere(['like', 'video_desc', $this->video_desc])
            //->andFilterWhere(['like', 'aspec_ratio', $this->aspec_ratio])
            ->andFilterWhere(['like', 'adaptive_path', $this->adaptive_path])
            ->andFilterWhere(['like', 'adaptive_resolution', $this->adaptive_resolution])
            ;

        if ($this->actived != '-1') {
            $query->andFilterWhere(['!=', 'actived' , -1]);
        }
        $query->andFilterWhere([
            'actived' => $this->actived,
        ]);

        return $dataProvider;
    }
}
