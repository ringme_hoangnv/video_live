<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_report_content_type".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $type
 */
class TblReportContentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_report_content_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'type' => 'Type',
        ];
    }
}
