<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VcsCategory;

/**
 * VcsCategorySearch represents the model behind the search form about `backend\models\VcsCategory`.
 */
class VcsCategorySearch extends VcsCategory
{
    public function formName()
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'actived', 'display_order'], 'integer'],
            [['cate_name', 'icon_image', 'header_banner', 'slug', 'description', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VcsCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['display_order' => SORT_DESC, 'cate_name' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        if ($this->actived != '-1') {

            $query->andFilterWhere(['!=', 'actived' , -1]);

        }
        $query->andFilterWhere([
            'actived' => $this->actived,
        ]);

        $query->andFilterWhere(['like', 'cate_name', $this->cate_name]);

        return $dataProvider;
    }
}
