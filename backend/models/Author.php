<?php

namespace backend\models;

use Yii;

class Author extends \common\models\AuthorBase {


    /**
     * Tra ve mang dang [id => name]
     * Dung trong select box
     * @return array
     */
    public static function getAuthorArray() {
        $itemArr = self::find()
            ->select('id, name')
            ->asArray()
            ->orderBy('name asc')
            ->all();

        if (!empty($itemArr)) {
            return  \yii\helpers\ArrayHelper::map($itemArr, 'id', 'name');

        } else {
            return array();
        }
    }
}