<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_report_day".
 *
 * @property int $id
 * @property string|null $user_id
 * @property int|null $streamer_id
 * @property int|null $status
 * @property string|null $description
 * @property int|null $amount_star
 * @property float|null $amount_money
 * @property float|null $percent_extract
 * @property float|null $money_extract
 * @property int|null $amount_transaction
 * @property string|null $created_at
 * @property string|null $report_date
 */
class TblReportDay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_report_day';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamer_id', 'status', 'amount_star', 'amount_transaction'], 'integer'],
            [['amount_money', 'percent_extract', 'money_extract'], 'number'],
            [['created_at', 'report_date'], 'safe'],
            [['user_id', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'streamer_id' => 'Streamer ID',
            'status' => 'Status',
            'description' => 'Description',
            'amount_star' => 'Amount Star',
            'amount_money' => 'Amount Money',
            'percent_extract' => 'Percent Extract',
            'money_extract' => 'Money Extract',
            'amount_transaction' => 'Amount Transaction',
            'created_at' => 'Created At',
            'report_date' => 'Report Date',
        ];
    }
}
