<?php

namespace app\models;

use app\models\TblReportDay;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TblReportDaySearch represents the model behind the search form of `app\models\TblReportDay`.
 */
class TblReportDaySearch extends TblReportDay
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'streamer_id', 'status', 'amount_star', 'amount_transaction'], 'integer'],
            [['user_id', 'description', 'created_at', 'report_date'], 'safe'],
            [['amount_money', 'percent_extract', 'money_extract'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $time)
    {
        $query = TblReportDay::find();

        if ($time == 'week') {
            $datenow = date("Y-m-d H:i:s");
            $datenow7 = strtotime("-7 day", strtotime($datenow));
            $datenow7 = date("Y-m-d H:i:s", $datenow7);
            $query = \common\models\TblReportDay::find()->where(['description' => 'DONATE'])->andWhere(['>', 'created_at', $datenow7]);
        } elseif ($time == 'month') {
            $datenow = date("Y-m-d H:i:s");
            $datenowmonth = strtotime("-30 day", strtotime($datenow));
            $datenowmonth = date("Y-m-d H:i:s", $datenowmonth);
            $query = \common\models\TblReportDay::find()->where(['description' => 'DONATE'])->andWhere(['>', 'created_at', $datenowmonth]);
        }
        elseif ($time == 'year'){
            $query = TblReportDay::find();
        }


//         add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'streamer_id' => $this->streamer_id,
            'status' => $this->status,
            'amount_star' => $this->amount_star,
            'amount_money' => $this->amount_money,
            'percent_extract' => $this->percent_extract,
            'money_extract' => $this->money_extract,
            'amount_transaction' => $this->amount_transaction,
            'created_at' => $this->created_at,
            'report_date' => $this->report_date,
        ]);

        $query->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
