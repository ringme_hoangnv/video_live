<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Banner;

/**
 * BannerSearch represents the model behind the search form about `backend\models\Banner`.
 */
class BannerSearch extends Banner
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'actived', 'display_order'], 'integer'],
            [['bn_name', 'bn_image_url', 'bn_position', 'bn_action', 'content_id', 'bn_media_url', 'bn_link', 'publish_time', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Banner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'actived' => $this->actived,
            'display_order' => $this->display_order,
        ]);

        $query->andFilterWhere(['like', 'bn_name', $this->bn_name])
            ->andFilterWhere(['like', 'bn_position', $this->bn_position])
            ->andFilterWhere(['like', 'bn_action', $this->bn_action])
            ->andFilterWhere(['like', 'content_id', $this->content_id])
            ;

        if($this->publish_time && strpos($this->publish_time, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->publish_time, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'publish_time', $request_times[0], $request_times[1]]);
        }

        return $dataProvider;
    }
}
