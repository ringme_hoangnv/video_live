<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VcsChannel;

/**
 * VcsChannelSearch represents the model behind the search form about `backend\models\VcsChannel`.
 */
class VcsChannelSearch extends VcsChannel
{
    public $showStatusFilter = true;
    
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'actived', 'num_follows', 'num_videos', 'is_official'], 'integer'],
            [['msisdn', 'channel_name', 'channel_avatar', 'header_banner', 'description', 'slug', 'created_from', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VcsChannel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_official' => $this->is_official,
        ]);

        if ($this->actived != '-1') {
            $query->andFilterWhere(['!=', 'actived' , -1]);
        }
        $query->andFilterWhere([
            'actived' => $this->actived,
        ]);

        $query->andFilterWhere(['like', 'msisdn', $this->msisdn])
            ->andFilterWhere(['like', 'channel_name', $this->channel_name])
            ;

        return $dataProvider;
    }
}
