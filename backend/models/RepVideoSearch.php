<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RepVideo;

/**
 * RepVideoSearch represents the model behind the search form about `backend\models\RepVideo`.
 */
class RepVideoSearch extends RepVideo
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['video_id', 'total_view', 'total_like', 'total_follow', 'total_unfollow', 'total_comment', 'total_like_comment', 'total_watching_time', 'total_play'], 'integer'],
            [['report_date', 'video_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepVideo::find()
            ->select('video_id, SUM(total_view) total_view, SUM(total_like) total_like, SUM(total_follow) total_follow, SUM(total_unfollow) total_unfollow, SUM(total_comment) total_comment, SUM(total_like_comment) total_like_comment, sum(total_watching_time) total_watching_time, sum(total_play) total_play, sum(total_watching_time)/sum(total_play) average_watching_time')
            ->groupBy('video_id')
            ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['total_watching_time' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere(['video_id' => $this->video_id]);

        if($this->report_date != Yii::t('backend', 'All') && strpos($this->report_date, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->report_date, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'report_date', $request_times[0], $request_times[1]]);
        }

        return $dataProvider;
    }
}
