<?php

namespace backend\models;

use Yii;

class DirtyWord extends \common\models\DirtyWordBase {

    public function rules()
    {
        return [
            [['word'], 'required'],
            [['word'], 'trim'],
            [['word'], 'unique'],
            [['updated_time'], 'safe'],
            [['word'], 'string', 'max' => 100]
        ];
    }
}