<?php

return [
    'meta_content_filter' => [
        'P' => 'Được phổ biến rộng rãi',
        'C13' => 'Dành cho khán giả trên 13 tuổi',
        'C16' => 'Dành cho khán giả trên 16 tuổi',
        'C18' => 'Dành cho khán giả trên 18 tuổi',
    ]
];