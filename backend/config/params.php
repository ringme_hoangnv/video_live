<?php

return [
    'login_background_img' => [
        #'/img/bg2.jpg',
        #'/img/bg3.jpg',
        #'/img/bg4.jpg',
    ],
    'page_sizes' => [
        10 => 10,
        20 => 20,
        50 => 50,
#        100 => 100,
    ],
    'upload_folder' => 'uploads',

    'is_active' => [
        '0' => 'Chưa kích hoạt',
        '1' => 'Kích hoạt',
    ],
    'official_account_id_pattern' => '/^[A-Za-z0-9_]+$/',
    'phonenum_list_pattern' => '/^\+670([0-9]{8,10})(\,\+670([0-9]{8,10}))*$/',

    'viettel_phone_expression' => '/^(84)?(((86|96|97|98|16[0-9])\d|868)\d{6})$/',

    'report_max_interval' => 60,

    'adminEmail' => 'admin@example.com',
];
