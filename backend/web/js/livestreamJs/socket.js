var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    } else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('https://livechat.ringme.vn:8088/live/event');
    stompClient = Stomp.over(socket);
    stompClient.connect({"user": roomId}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        subscribe();
        subcribeNotify()
    });
}

var subscription_id = null;


function subscribe() {
    console.log(subscription_id);
    subscription_id = stompClient.subscribe('/watching/' + roomId, function (greeting) {
        // console.log("MESSAGE_BODY: " + greeting.body);
        var msg = JSON.parse(JSON.parse(greeting.body));
        if (msg.type == 'seen_number') {
            $('.stream-view').text(msg.number)
        } else if (msg.type == 'chat') {
            showGreeting(msg.avatar, msg.userName, msg.msgBody,msg.smsgId,msg.userId);
            scrollToBottom();
        } else if (msg.type = "gift") {
            showGreeting(msg.giftImg, msg.userName, "Vừa ném quà vào mặt bạn !!!");
        } else if (msg.type == 'like') {
            $('.stream-like').text(msg.totalLike);
        }
    });
}

function unsubscribe() {
    if (subscription_id) {
        stompClient.unsubscribe(subscription_id);
    }
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendMessage() {
    stompClient.send("/chat/message/" + roomId, {},
        JSON.stringify({
            'msgBody': $(".msg").val(),
            'type': "chat",
            'userId': streammer_ID,
            'userName': userName,
            'cIdMessage': 'abac121',
            'sIdMessage': Date.now(),
            'roomID': roomId,
            'avatar': 'https://www.dallalii.com/img/admin/logo.png',
            'createdAt': datetime,
        }));
}

function subcribeNotify() {
    stompClient.subscribe('/user/watching', function (frame) {
        var msg = JSON.parse(JSON.parse(frame.body));
        if (msg.contentType == "START") {
            setTimeout(function () {
                window.location.reload(1);
            }, 15000);
            // $('.div-stop').show();
            // $('.div-public').show();
        } else if (msg.contentType == "STOP") {
            $(".status-livestream").text("5")
        } else if (msg.type == 'HEALTH_CHECK') {
            $('.livestream-status').append('<div> "+ msg.descStatusLive +"</div>')
        }

    });
}


function showGreeting(avatar, userName, message,sidMsg,user_comment_id) {
    $("#greetings").append("<div class='content-father "+ sidMsg +"'>"+
        "<div>\n" +
        "        <img style='width: 25px' src=" + avatar + ">\n" +
        "    </div>\n" +
        "    <div class='content-index'><p style='display: inline; opacity: 0.6'>" + userName + "</p> : " + message + "</div>\n" +
        "    <div class=\"icon-dots\"> " +
        "<div class=\"dropleft\">\n" +
        "<i class=\"fas fa-ellipsis-v dropdown-toggle icon-dots-2\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"></i> " +
        "  <ul class=\"dropdown-menu ul-left\" aria-labelledby=\"dropdownMenuButton1\" >\n" +
        "    <li  class='delComment' onclick='reportComment(13, "+ sidMsg +" , "+ roomId +" , "+ streammer_ID +" ,"+ user_comment_id +")'><a class=\"dropdown-item\" >Delete comment</a></li>\n" +
        "    <li  class='blockChat' onclick='reportComment(14,"+ sidMsg +","+ roomId +","+ streammer_ID +","+ user_comment_id +")'><a  class=\"dropdown-item\">Block chat</a></li>\n" +
        "    <li  class='blockUser' onclick='reportComment(15,"+ sidMsg +","+ roomId +","+ streammer_ID +","+ user_comment_id +")'><a  class=\"dropdown-item\">Block user</a></li>\n" +
        "    <li  class='blockAll' onclick='reportComment(16,"+ sidMsg +","+ roomId +","+ streammer_ID +","+ user_comment_id +")'><a  class=\"dropdown-item\">Block all</a></li>\n" +
        "  </ul>\n" +
        "</div></div>\n" +
        "    </div>");

}

function showBtnStopPublic() {
    if ($(".status-livestream").text() != '0') {
        $('.div-stop').show();
        $('.div-public').show();
    }
}

function openCity(evt, btn) {
    document.getElementById("live-stream-analysis").style.display = "none";
    document.getElementById("live-stream-setting").style.display = "none";
    document.getElementById("live-stream-status").style.display = "none";
    document.getElementById("btn-setting-father").style.backgroundColor = "#212121";
    document.getElementById("btn-analysis-father").style.backgroundColor = "#212121";
    document.getElementById("btn-status-father").style.backgroundColor = "#212121";
    if (btn == "setting") {
        document.getElementById("btn-setting-father").style.backgroundColor = 'white';
        document.getElementById("live-stream-setting").style.display = "block";
    }
    if (btn == "analysis") {
        document.getElementById("btn-analysis-father").style.backgroundColor = 'white';

        document.getElementById("live-stream-analysis").style.display = "block";
    }
    if (btn == "status") {
        document.getElementById("btn-status-father").style.backgroundColor = 'white';

        document.getElementById("live-stream-status").style.display = "block";
    }
}

function scrollToBottom() {
    document.getElementById('tab-comment').scrollTop = document.getElementById('tab-comment').scrollHeight + 10;
}


$(function () {


    $("form").on('submit', function (e) {
        e.preventDefault();
        scrollToBottom();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#subscribe").click(function () {
        subscribe();
    });
    $("#unsubscribe").click(function () {
        unsubscribe();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    scrollToBottom();

    $("#send").click(function () {
        if ($(".msg").val() != "") {
            sendMessage();
            $('.msg').val("");

        }
    });
    $(".msg").on('keypress', function (e) {
        if (e.which == 13) {
            if ($(".msg").val() != "") {
                sendMessage();
                $('.msg').val("");
            }
        }
    });
    connect();

    showBtnStopPublic();

});
