<?php

return [
    'components' => [
//        'db' => [
//            'class' => 'yii\db\Connection',
//
//            'dsn' => 'mysql:host=103.143.206.63:3306;dbname=dbvideo_tm',
//            'username' => 'ukakoak',
//            'password' => 'Kakoak@123',
////            'dsn' => 'mysql:host=10.226.40.158:3306;dbname=video',
////            'username' => 'telemor_master',
////            'password' => 'Telemor@#$2020',
//            'charset' => 'utf8',
//            'enableSchemaCache' => false,
//            // Duration of schema cache.
//            'schemaCacheDuration' => 3600,
//            // Name of the cache component used to store schema information
//            'schemaCache' => 'cache',
//        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
//            'class' => 'yii\redis\Cache',
//            'redis' => [
//                'hostname' => 'localhost',
//                'port' => 8002,
//                'database' => 1,
//            ]
        ],
//        'session' => [
//            'class' => 'yii\redis\Session',
//            'redis' => [
//                'hostname' => 'localhost',
//                'port' => 8002,
//				'password'=>'123456a@',
//                'database' => 0,
//            ],
//            'cookieParams' => [
//                //    'path' => '/',
//                //     'domain' => "localhost:9501",
//                'expire' => 0
//            ],
//        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
        ],
    ],
];
