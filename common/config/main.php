<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',

            #'dsn' => 'mysql:host=10.226.40.158:3306;dbname=video',
            #'username' => 'telemor_master',
            #'password' => 'Telemor@#$2020',
//            'dsn' => 'mysql:host=10.20.0.88:3306;dbname=dbvideo_tm',
            'dsn' => 'mysql:host=192.168.1.88:3306;dbname=dbvideo_tm',
            'username' => 'ukakoak',
            'password' => 'Kakoak@123',
            'charset' => 'utf8',
            'enableSchemaCache' => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache' => 'cache',
        ],
        'db1' => [
            'class' => 'yii\db\Connection',

//            'dsn' => 'mysql:host=10.20.0.88:3306;dbname=livestream',
            'dsn' => 'mysql:host=192.168.1.88:3306;dbname=livestream',
            'username' => 'ukakoak',
            'password' => 'Kakoak@123',
//            'dsn' => 'mysql:host=10.226.40.158:3306;dbname=video',
//            'username' => 'telemor_master',
//            'password' => 'Telemor@#$2020',
            'charset' => 'utf8',
            'enableSchemaCache' => false,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache' => 'cache',
        ],
//        'db' => [
//            'class' => 'yii\db\Connection',
//
//            #'dsn' => 'mysql:host=10.226.40.158:3306;dbname=video',
//            #'username' => 'telemor_master',
//            #'password' => 'Telemor@#$2020',
//            'dsn' => 'mysql:host=10.20.0.88:3306;dbname=dbvideo_tm',
//            'username' => 'ukakoak',
//            'password' => 'Kakoak@123',
//            'charset' => 'utf8',
//            'enableSchemaCache' => false,
//            // Duration of schema cache.
//            'schemaCacheDuration' => 3600,
//            // Name of the cache component used to store schema information
//            'schemaCache' => 'cache',
//        ],
//        'db1' => [
//            'class' => 'yii\db\Connection',
//
//            'dsn' => 'mysql:host=10.20.0.88:3306;dbname=livestream',
//            'username' => 'ukakoak',
//            'password' => 'Kakoak@123',
//            'charset' => 'utf8',
//            'enableSchemaCache' => false,
//            // Duration of schema cache.
//            'schemaCacheDuration' => 3600,
//            // Name of the cache component used to store schema information
//            'schemaCache' => 'cache',
//        ],
        'i18n' => [
            'translations' => [
                'api*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@api/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'frontend' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
//            'class' => 'yii\redis\Cache',
//            'redis' => [
//                'hostname' => 'localhost',
//                'port' => 8002,
//                'database' => 1,
//            ]
        ],
//        'session' => [
//            'class' => 'yii\redis\Session',
//            'redis' => [
//                'hostname' => 'localhost',
//                'port' => 8002,
//				'password'=>'123456a@',
//                'database' => 0,
//            ],
//            'cookieParams' => [
//                //    'path' => '/',
//                //     'domain' => "localhost:9501",
//                'expire' => 0
//            ],
//        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
        ],
//        'urlManagerFrontend' => [
//            'class' => 'yii\web\urlManager',
//            'baseUrl' => 'http://qltb.tbproduct.com/',
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
//            'rules' => require(__DIR__ . '/../../frontend/config/routes.php'),
//
//        ],
    ],
];
