<?php

namespace common\models;

use frontend\models\Product;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class ProductCategoryBase extends \common\models\db\ProductCategoryDB {

    public function getWebDetailUrl() {

        return Url::to([
            'product/list-by-cate',
            'cate_slug' => $this->slug,
            'cate_id' => $this->id
        ]);


    }

    public function getHotProductList($limit = 12) {
        return $this->getActiveProductQuery()
            ->limit($limit)
            ->all();
    }

    public static function getActiveCategoriesByParent($parentId = null) {

        $cache = Yii::$app->cache;
        $key = 'cate_by_parent_'. $parentId;
        $data = $cache->get($key);

        if (!$data) {
            $query = self::getActiveCategoryQuery();

            if (intval($parentId) > 0) {
                $query->andWhere(['parent_id' => $parentId]);
            } else {
                $query->andWhere('parent_id is NULL');
            }
            $data = $query->all();

            $cache->set($key, $data, CACHE_TIMEOUT);

        }

        return $data;
    }

    public static function getAllActiveCategory() {
        return self::getActiveCategoryQuery()
            ->all();

    }

    public static function getActiveCategoryQuery() {
        return self::find()
            ->where('is_active = 1')
            ->orderBy('priority ASC, name ASC')
            ;

    }

    public function getCreatorUserName() {
        $user = $this->getCreator();
        return ($user)? $user->username: '-';
    }

    public function getUpdaterUserName() {
        $user = $this->getUpdater();
        return ($user)? $user->username: '-';
    }

    public function getCreator() {
        $user = null;

        if ($this->created_by) {
            $user = User::find()
                ->where('id = :userId', [
                    ':userId' => $this->created_by
                ])
                ->one();
        }
        return $user;
    }

    public function getUpdater() {
        $user = null;

        if ($this->updated_by) {
            $user = User::find()
                ->where('id = :userId', [
                    ':userId' => $this->updated_by
                ])
                ->one();
        }
        return $user;
    }

    public function getActiveProductQuery()
    {
        return Product::getActivatedProductQuery()
            ->andWhere([
                'category_id' => $this->id,
            ])
            ->orderBy('priority DESC, is_hot DESC, id DESC')
            ;

    }

    public function getChildIds() {
        $childIds = self::find()
            ->select('id')
            ->where([
                'parent_id' => $this->id,
            ])
            ->all();
        return ArrayHelper::map($childIds, 'id', 'id');

    }
}