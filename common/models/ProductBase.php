<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

class ProductBase extends \common\models\db\ProductDB {

    public function getNewActiveProducts($limit = null) {
        $products = self::find()
            ->where(['status' => 1])
            ->orderBy('created_at desc')
            ;
        if ($limit > 0)  {
            $products->limit($limit);
        }
        return $products->all();
    }
    public function getWebDetailUrl() {

        return Yii::$app->urlManagerFrontend->createUrl(['product/detail', 'pd_id' => $this->id, 'pd_slug'=> $this->slug]);
//        $cate = ProductCategoryBase::findOne(['id' => $this->category_id]);
//
//        if (!$cate)
//            return '#';
//        else
//            return Url::to([
//                'product/detail',
//                'cate_slug' => $cate->slug,
//                'pd_slug' => $this->slug,
//                'pd_id' => $this->id
//            ]);


    }

    public function getCategory() {
        return $this->hasOne(ProductCategoryBase::className(), ['id' => 'category_id']);
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'pd_code' => Yii::t('backend', 'Code'),
            'description' => Yii::t('backend', 'Description'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'amount' => Yii::t('backend', 'Amount'),
            'booking_amount' => Yii::t('backend', 'Booking Amount'),
            'published_at' => Yii::t('backend', 'Published At'),
            'status' => Yii::t('backend', 'Status'),
            'priority' => Yii::t('backend', 'Priority'),
            'is_display_price' => Yii::t('backend', 'Is Display Price'),
            'is_hot' => Yii::t('backend', 'Is Hot'),
            'price' => Yii::t('backend', 'Price'),
            'slug' => Yii::t('backend', 'Slug'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
            'qr_code_image' => Yii::t('backend', 'Qr Code Image'),
        ];
    }

    public function getBorrowedList() {
        $key = 'pd_borrowed_'.$this->id;
        $data = Yii::$app->cache->get($key);

        if (!$data) {
            $data = \common\models\BookingHistoryBase::find()
                ->where([
                    'product_id' => $this->id,
                    'status' => 1,
                ])
                ->all();

            Yii::$app->cache->set($key, $data, CACHE_TIMEOUT);
        }
        return $data;
    }
}