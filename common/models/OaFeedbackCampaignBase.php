<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;

class OaFeedbackCampaignBase extends \common\models\db\OaFeedbackCampaignDB
{

    public static function getAllInputTypeArr() {

        return [
            'text' => Yii::t('backend', 'Enter phonenumber'),
            'file' => Yii::t('backend', 'Using File Upload'),
            'active_users' => Yii::t('backend', 'All active users'),
        ];
    }
    public function getInputTypeName() {
        $inputTypeArr = self::getAllInputTypeArr();
        return $inputTypeArr[$this->input_type];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function getFilePathUrl() {
        return Yii::$app->params['media_url']. $this->file_path;
    }

    public function getStatusName() {
        switch ($this->status) {
            case 0:
                return Yii::t('backend', 'Draft');
                break;
            case 1:
                return Yii::t('backend', 'Approved');
                break;
        }
    }

    public function getProcessStatusName() {
        switch ($this->process_status) {
            case 0:
                return Yii::t('backend', 'Not yet process');
                break;
            case 1:
                return Yii::t('backend', 'Processing');
                break;
            case 2:
                return Yii::t('backend', 'Done');
                break;
            case -1:
                return Yii::t('backend', 'Error');
                break;
        }
    }

    public function getCreatedBy() {
        return $this->hasOne(UserBase::className(), ['id' => 'created_by']);
    }
    public function getUpdatedBy() {
        return $this->hasOne(UserBase::className(), ['id' => 'updated_by']);
    }
    public function getApprovedBy() {
        return $this->hasOne(UserBase::className(), ['id' => 'approved_by']);
    }
}