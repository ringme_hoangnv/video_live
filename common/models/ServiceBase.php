<?php

namespace common\models;

use frontend\models\Post;
use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Url;

class ServiceBase extends \common\models\db\ServiceDB
{


    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'name',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function getWebDetailUrl()
    {

        return Url::to([
            'service/detail',
            'service_slug' => $this->slug,
            'service_id' => $this->id,
            'lang' => Yii::$app->language,
        ]);
    }

    public function getImageUrl($absolute = false)
    {
        return $this->image_path;
    }

    public function getActivatedPostQuery()
    {
        return PostBase::getActivatedPostQuery()
            ->andWhere([
                'a.service_id' => $this->id,
            ])
            ;
    }

    public static function getActivatedQuery() {
        return self::find()
            ->alias('a')
            ->joinWith('translation t')
            ->select('t.id as id, t.name as name, t.short_desc short_desc, t.content as content, t.slug as slug, t.seo_title as seo_title, t.seo_description as seo_description, t.seo_keywords')
            ->where([
                'a.status' => 1,
            ])
            ->orderBy('a.priority DESC, a.id DESC')
            ;
    }

    public static function getAllServices() {
        return self::find()
            ->alias('a')
            ->joinWith('translation t')
            ->select('t.id as id, t.name as name, t.short_desc short_desc, t.content as content, t.slug as slug, t.seo_title as seo_title, t.seo_description as seo_description, t.seo_keywords')
            ->orderBy('a.priority DESC, a.id DESC')
            ->all()
            ;
    }

    public function getTranslation() {

        return $this->hasOne(ServiceTranslationBase::className(), ['id' => 'id'])
            ->andWhere(['lang' => Yii::$app->session->get('language', Yii::$app->params['default_content_lang'])]);
    }
}