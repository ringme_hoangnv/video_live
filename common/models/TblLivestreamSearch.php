<?php

namespace common\models;

use backend\models\AuthAssignment;
use common\models\db\AuthAssignmentDB;
use common\models\TblLivestream;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * TblLivestreamSearch represents the model behind the search form of `common\models\TblLivestream`.
 */
class TblLivestreamSearch extends TblLivestream
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'channel_id', 'total_view', 'total_comment', 'total_share', 'category', 'total_like', 'profile','total_star','age_limit','average_watch_time','enable_chat','chat_setting'], 'integer'],
            [['title', 'description', 'stream_key', 'hls_play_link', 'time_event_start', 'time_event_finish', 'created_at', 'updated_at', 'profile', 'time_start', 'time_finish'], 'safe'],
            [['enable_time_event', 'enable_record'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $authen_assign = AuthAssignmentDB::find()->where(['user_id' =>\Yii::$app->getUser()->id])->one();

        if($authen_assign->item_name=='admin'||$authen_assign->item_name=='super-admin'){
             $query = TblLivestream::find()->where(['status' => 0])->andWhere(['not', ['time_event_start' => null]]);
        }
        else{
            $query = TblLivestream::find()->where(['channel_id' => \Yii::$app->getUser()->id,'status'=>5]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'time_event_start' => $this->time_event_start,
            'time_event_finish' => $this->time_event_finish,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'channel_id' => $this->channel_id,
            'total_view' => $this->total_view,
            'total_comment' => $this->total_comment,
            'total_share' => $this->total_share,
            'enable_time_event' => $this->enable_time_event,
            'enable_record' => $this->enable_record,
            'time_start' => $this->time_start,
            'time_finish' => $this->time_finish,
            'category' => $this->category,
            'total_like' => $this->total_like,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'stream_key', $this->stream_key])
            ->andFilterWhere(['like', 'hls_play_link', $this->hls_play_link])
            ->andFilterWhere(['like', 'profile', $this->profile]);

        return $dataProvider;
    }

    public function searchLastest($params)
    {
        $query = TblLivestream::find()
            ->where(['channel_id' => \Yii::$app->getUser()->id])
            ->andWhere(['<=', 'time_finish', date("Y-m-d H:i:s")]);
//            ->orderBy(['time_finish'=>'DES'])->limit(0);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['time_finish' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);
        return $dataProvider;
    }

    public function searchTopLivestream($params)
    {
        $query = TblLivestream::find()
            ->where(['channel_id' => \Yii::$app->getUser()->id])
            ->andWhere(['>=', 'total_view', 0]);
//            ->orderBy(['time_finish'=>'DES'])->limit(0);
        $dataProviderTopLivestream = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['total_view' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        return $dataProviderTopLivestream;
    }

    public function searchHistorylivestream($query)
    {
        $authen_assign = AuthAssignmentDB::find()->where(['user_id' => \Yii::$app->getUser()->id])->one();

        if ($authen_assign->item_name == 'admin' || $authen_assign->item_name == 'super-admin') {

            $query = TblLivestream::find()->where(['status' => 5]);
        } else {
            $query = TblLivestream::find()->where(['channel_id' => \Yii::$app->getUser()->id, 'status' => 5]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'time_event_start' => $this->time_event_start,
            'time_event_finish' => $this->time_event_finish,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'channel_id' => $this->channel_id,
            'total_view' => $this->total_view,
            'total_comment' => $this->total_comment,
            'total_share' => $this->total_share,
            'enable_time_event' => $this->enable_time_event,
            'enable_record' => $this->enable_record,
            'time_start' => $this->time_start,
            'time_finish' => $this->time_finish,
            'category' => $this->category,
            'total_like' => $this->total_like,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'stream_key', $this->stream_key])
            ->andFilterWhere(['like', 'hls_play_link', $this->hls_play_link])
            ->andFilterWhere(['like', 'profile', $this->profile]);

        return $dataProvider;
    }

}
