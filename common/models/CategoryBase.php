<?php

namespace common\models;

use backend\models\User;
use Yii;
use yii\helpers\Url;

class CategoryBase extends \common\models\db\CategoryDB {

    public function getWebDetailUrl() {

        return Url::to([
            'category/detail',
            'cate_slug' => $this->slug,
            'cate_id' => $this->id
        ]);


    }

    public static function getActiveCategoriesByParent($parentId = null) {
        return self::find()
            ->where('status = 1')
            ->andWhere('parent_id = ?', $parentId)
            ->orderBy('priority DESC, name ASC')
            ->all();
    }

    public static function getAllActiveCategory() {
        return self::find()
            ->where('status = 1')
            ->orderBy('priority DESC, name ASC')
            ->all();

    }

    public function getCreatorUserName() {
        $user = $this->getCreator();
        return ($user)? $user->username: '-';
    }

    public function getUpdaterUserName() {
        $user = $this->getUpdater();
        return ($user)? $user->username: '-';
    }

    public function getCreator() {
        $user = null;

        if ($this->created_by) {
            $user = User::find()
                ->where('id = :userId', [
                    ':userId' => $this->created_by
                ])
                ->one();
        }
        return $user;
    }

    public function getUpdater() {
        $user = null;

        if ($this->updated_by) {
            $user = User::find()
                ->where('id = :userId', [
                    ':userId' => $this->updated_by
                ])
                ->one();
        }
        return $user;
    }

    public function getStories()
    {
        return $this->hasMany(StoryBase::className(), ['id' => 'story_id'])
            ->viaTable('story_category', ['story_id' => 'id']);

    //        return $this->hasMany(StoryBase::className(), ['story_id' => 'id']);
    }
}