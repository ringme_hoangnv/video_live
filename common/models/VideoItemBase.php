<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class VideoItemBase extends \common\models\db\VideoItemDB
{
    const DELETED_STATUS = -1;
    const WAIT_CONVERT_STATUS = 1;
    const WAIT_APPROVAL_STATUS = 9;


    public function getVideoMediaUrl() {
        if (strpos($this->video_media, 'http') === 0) {
            return $this->video_media;
        } elseif (strpos($this->video_media, '/cms_upload/') === 0) {
            return Yii::$app->params['media_url_cms']. $this->video_media;
        } elseif (strpos($this->video_media, '/usr_upload/') === 0) {
            return Yii::$app->params['media_url_user']. $this->video_media;
        } elseif (strpos($this->video_media, '/vcs_medias/') === 0) {
            return Yii::$app->params['media_url_vcs']. $this->video_media;
        } else {
            return Yii::$app->params['media_url']. $this->video_media;
        }
    }
    public function getVideoImageUrl() {
        return (strpos($this->video_image, 'http') !== 0)? Yii::$app->params['media_url']. $this->video_image: $this->video_image;
    }

    /**
     * -1: xóa  (Ko hiển thị ở đâu  Còn record DB);
        1: Video chờ convert (Convert adaptive mp4);
        8: video bị gỡ;
        9: Trạng thái chờ duyệt;
        10: Hiển thị toàn bộ;
        11: Hiển thị khi tìm kiếm;
        12: Hiển thị listVideo của kênh;
     * @return array
     */
    public static function getActiveDefineArr() {
        return [
            -1 => Yii::t('backend', 'Deleted'),
            1 => Yii::t('backend', 'Convert waiting'),
            2 => Yii::t('backend', 'Start Convert'),
            3 => Yii::t('backend', 'Convert ERROR'),
            8 => Yii::t('backend', 'Disapproved'),
            9 => Yii::t('backend', 'Approval waiting'),
            10 => Yii::t('backend', 'Display all'),
            11 => Yii::t('backend', 'Display search'),
            12 => Yii::t('backend', 'Video of channel'),
        ];
    }
    public function getActiveName() {
        $activeArr = self::getActiveDefineArr();
        return (isset($activeArr[$this->actived]))? $activeArr[$this->actived]: $this->actived;
    }

    /**
     *  0: convert to mp4;
        1: adaptive;
        2: khong convert;
     * @return int
     */
    public static function getEncodeTypeArr() {
        return [
            0 => Yii::t('backend', 'Convert to mp4'),
            1 => Yii::t('backend', 'Adaptive'),
            2 => Yii::t('backend', 'Not convert'),
        ];
    }
    public function getEncodeTypeName() {
        $arr = self::getEncodeTypeArr();
        return (isset($arr[$this->encode_type]))? $arr[$this->encode_type]: $this->encode_type;
    }


    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'video_title',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getCategory() {

        return $this->hasOne(VcsCategoryBase::className(), ['id' => 'cate_id']);
    }
    public function getChannel() {

        return $this->hasOne(VcsChannelBase::className(), ['id' => 'channel_id']);
    }
}