<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment_live_stream".
 *
 * @property string|null $comment
 * @property int $id_live_stream
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $id_user
 * @property string|null $user_name
 */
class CommentLiveStream extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment_live_stream';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment'], 'string'],
            [['id_live_stream'], 'required'],
            [['id_live_stream', 'id_user'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comment' => 'Comment',
            'id_live_stream' => 'Id Live Stream',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'id_user' => 'Id User',
            'user_name' => 'User Name',
        ];
    }
}
