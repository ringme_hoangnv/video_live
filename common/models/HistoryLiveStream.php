<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "History_LiveStream".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property int $status
 * @property string|null $stream_key
 * @property string|null $hls_play_link
 * @property string|null $time_event_start
 * @property string|null $time_event_finish
 * @property string|null $vod_play_back
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $stream_id
 * @property int|null $user_id
 * @property int|null $profile
 * @property int|null $total_comment
 * @property int|null $total_like
 * @property int|null $total_share
 * @property int|null $total_view
 * @property bool|null $enable_record
 * @property string $time_finish
 * @property string|null $time_start
 * @property int|null $category
 * @property string|null $rtmp_push
 * @property string|null $stream_alias
 * @property string|null $link_avatar
 */
class HistoryLiveStream extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'History_LiveStream';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'title', 'status', 'time_finish'], 'required'],
            [['id', 'status', 'stream_id', 'user_id', 'profile', 'total_comment', 'total_like', 'total_share', 'total_view', 'category'], 'integer'],
            [['description'], 'string'],
            [['time_event_start', 'time_event_finish', 'created_at', 'updated_at', 'time_finish', 'time_start'], 'safe'],
            [['enable_record'], 'boolean'],
            [['title', 'hls_play_link', 'vod_play_back', 'rtmp_push', 'stream_alias', 'link_avatar'], 'string', 'max' => 255],
            [['stream_key'], 'string', 'max' => 20],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id Livestream',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'stream_key' => 'Stream Key',
            'hls_play_link' => 'Hls Play Link',
            'time_event_start' => 'Time Event Start',
            'time_event_finish' => 'Time Event Finish',
            'vod_play_back' => 'Vod Play Back',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'stream_id' => 'Stream ID',
            'user_id' => 'User ID',
            'profile' => 'Profile',
            'total_comment' => 'Total Comment',
            'total_like' => 'Total Like',
            'total_share' => 'Total Share',
            'total_view' => 'Total View',
            'enable_record' => 'Enable Record',
            'time_finish' => 'Time Finish',
            'time_start' => 'Time Start',
            'category' => 'Category',
            'rtmp_push' => 'Rtmp Push',
            'stream_alias' => 'Stream Alias',
            'link_avatar' => 'Link Avatar',
        ];
    }
}
