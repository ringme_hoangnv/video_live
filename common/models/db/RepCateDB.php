<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "rep_cate".
 *
 * @property integer $id
 * @property string $report_date
 * @property integer $cate_id
 * @property integer $total_view
 * @property integer $total_like
 * @property integer $total_comment
 * @property integer $total_like_comment
 */
class RepCateDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rep_cate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date'], 'safe'],
            [['cate_id', 'total_view', 'total_like', 'total_comment', 'total_like_comment'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'Report Date',
            'cate_id' => 'Cate ID',
            'total_view' => 'Total View',
            'total_like' => 'Total Like',
            'total_comment' => 'Total Comment',
            'total_like_comment' => 'Total Like Comment',
        ];
    }
}
