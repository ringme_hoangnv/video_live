<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "user_oa_feedback".
 *
 * @property integer $id
 * @property string $official_account_id
 * @property string $username
 * @property string $message
 * @property string $sub_type
 * @property integer $is_receive
 * @property string $time_receive
 * @property integer $is_read
 */
class UserOaFeedbackDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_oa_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['is_receive'], 'integer'],
            [['time_receive'], 'safe'],
            [['official_account_id', 'username'], 'string', 'max' => 50],
            [['sub_type'], 'string', 'max' => 30],
            [['is_read'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'official_account_id' => 'Official Account ID',
            'username' => 'Username',
            'message' => 'Message',
            'sub_type' => 'Sub Type',
            'is_receive' => 'Is Receive',
            'time_receive' => 'Time Receive',
            'is_read' => 'Is Read',
        ];
    }
}
