<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "campaign".
 *
 * @property string $id
 * @property string $name
 * @property string $partner_id
 * @property string $description
 * @property integer $status
 * @property integer $campaign_type
 * @property string $max_data
 * @property string $max_number_access
 * @property string $warning_data
 * @property string $warning_number_access
 * @property string $current_data
 * @property string $current_number_access
 * @property string $begin_time
 * @property string $end_time
 * @property string $last_active_time
 * @property string $last_deactive_time
 * @property string $last_warning_time
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class CampaignDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'partner_id'], 'required'],
            [['partner_id', 'status', 'campaign_type', 'max_data', 'max_number_access', 'warning_data', 'warning_number_access', 'current_data', 'current_number_access', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['begin_time', 'end_time', 'last_active_time', 'last_deactive_time', 'last_warning_time', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'partner_id' => Yii::t('backend', 'Partner ID'),
            'description' => Yii::t('backend', 'Description'),
            'status' => Yii::t('backend', 'Status'),
            'campaign_type' => Yii::t('backend', 'Campaign Type'),
            'max_data' => Yii::t('backend', 'Max Data'),
            'max_number_access' => Yii::t('backend', 'Max Number Access'),
            'warning_data' => Yii::t('backend', 'Warning Data'),
            'warning_number_access' => Yii::t('backend', 'Warning Number Access'),
            'current_data' => Yii::t('backend', 'Current Data'),
            'current_number_access' => Yii::t('backend', 'Current Number Access'),
            'begin_time' => Yii::t('backend', 'Begin Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'last_active_time' => Yii::t('backend', 'Last Active Time'),
            'last_deactive_time' => Yii::t('backend', 'Last Deactive Time'),
            'last_warning_time' => Yii::t('backend', 'Last Warning Time'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }
}
