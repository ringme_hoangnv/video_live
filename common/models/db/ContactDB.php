<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property string $id
 * @property string $fullname
 * @property string $phonenumber
 * @property string $email
 * @property string $body
 * @property string $referal_url
 * @property string $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class ContactDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'phonenumber', 'body'], 'required'],
            [['body'], 'string'],
            [['product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['fullname', 'email', 'referal_url'], 'string', 'max' => 255],
            [['phonenumber'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'fullname' => Yii::t('backend', 'Fullname'),
            'phonenumber' => Yii::t('backend', 'Phonenumber'),
            'email' => Yii::t('backend', 'Email'),
            'body' => Yii::t('backend', 'Body'),
            'referal_url' => Yii::t('backend', 'Referal Url'),
            'product_id' => Yii::t('backend', 'Product ID'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
}
