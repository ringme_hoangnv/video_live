<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "job".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $category_id
 * @property string $image_path
 * @property string $work_location
 * @property integer $amount
 * @property string $level
 * @property double $salary
 * @property string $start_time
 * @property string $end_time
 * @property string $slug
 * @property string $content
 * @property string $created_at
 * @property string $company_name
 * @property integer $company_id
 */
class JobDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'content'], 'required'],
            [['status', 'category_id', 'amount', 'company_id'], 'integer'],
            [['salary'], 'number'],
            [['start_time', 'end_time', 'created_at'], 'safe'],
            [['content'], 'string'],
            [['name', 'image_path', 'slug', 'company_name'], 'string', 'max' => 255],
            [['work_location', 'level'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'status' => Yii::t('backend', 'Status'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'work_location' => Yii::t('backend', 'Work Location'),
            'amount' => Yii::t('backend', 'Amount'),
            'level' => Yii::t('backend', 'Level'),
            'salary' => Yii::t('backend', 'Salary'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'slug' => Yii::t('backend', 'Slug'),
            'content' => Yii::t('backend', 'Content'),
            'created_at' => Yii::t('backend', 'Created At'),
            'company_name' => Yii::t('backend', 'Company Name'),
            'company_id' => Yii::t('backend', 'Company ID'),
            'lang' =>  Yii::t('backend', 'Language'),
        ];
    }
}
