<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "report_buy_package".
 *
 * @property integer $id
 * @property string $report_date
 * @property string $package_name
 * @property string $package_type
 * @property integer $total_register
 * @property integer $total_user_register
 * @property double $total_money
 * @property string $created_at
 */
class ReportBuyPackageDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_buy_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date', 'created_at'], 'safe'],
            [['total_register', 'total_user_register'], 'integer'],
            [['total_money'], 'string'],
            [['package_name', 'package_type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'Report Date',
            'package_name' => 'Package Name',
            'package_type' => 'Package Type',
            'total_register' => 'Total Register',
            'total_user_register' => 'Total User Register',
            'total_money' => 'Total Money',
            'created_at' => 'Created At',
        ];
    }
}
