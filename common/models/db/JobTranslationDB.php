<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "job_translation".
 *
 * @property integer $id
 * @property string $name
 * @property string $work_location
 * @property string $level
 * @property double $salary
 * @property string $slug
 * @property string $content
 * @property string $company_name
 * @property string $lang
 */
class JobTranslationDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'job_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'content', 'lang'], 'required'],
            [['content'], 'string'],
            [['name', 'slug', 'company_name'], 'string', 'max' => 255],
            [['work_location', 'level'], 'string', 'max' => 200],
            [['lang'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'work_location' => Yii::t('backend', 'Work Location'),
            'level' => Yii::t('backend', 'Level'),
            'slug' => Yii::t('backend', 'Slug'),
            'content' => Yii::t('backend', 'Content'),
            'company_name' => Yii::t('backend', 'Company Name'),
            'lang' => Yii::t('backend', 'Lang'),
        ];
    }
}
