<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $common_name
 * @property string $formal_name
 * @property string $country_type
 * @property string $country_sub_type
 * @property string $sovereignty
 * @property string $capital
 * @property string $currency_code
 * @property string $currency_name
 * @property string $telephone_code
 * @property string $country_code3
 * @property string $country_number
 * @property string $internet_country_code
 * @property integer $sort_order
 * @property integer $is_published
 * @property string $flags
 * @property integer $is_deleted
 */
class CountryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_code'], 'required'],
            [['sort_order', 'is_published', 'is_deleted'], 'integer'],
            [['country_code', 'common_name', 'formal_name', 'country_type', 'country_sub_type', 'sovereignty', 'capital', 'currency_code', 'currency_name', 'telephone_code', 'country_code3', 'country_number', 'internet_country_code'], 'string', 'max' => 100],
            [['flags'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country Code'),
            'common_name' => Yii::t('backend', 'Common Name'),
            'formal_name' => Yii::t('backend', 'Formal Name'),
            'country_type' => Yii::t('backend', 'Country Type'),
            'country_sub_type' => Yii::t('backend', 'Country Sub Type'),
            'sovereignty' => Yii::t('backend', 'Sovereignty'),
            'capital' => Yii::t('backend', 'Capital'),
            'currency_code' => Yii::t('backend', 'Currency Code'),
            'currency_name' => Yii::t('backend', 'Currency Name'),
            'telephone_code' => Yii::t('backend', 'Telephone Code'),
            'country_code3' => Yii::t('backend', 'Country Code3'),
            'country_number' => Yii::t('backend', 'Country Number'),
            'internet_country_code' => Yii::t('backend', 'Internet Country Code'),
            'sort_order' => Yii::t('backend', 'Sort Order'),
            'is_published' => Yii::t('backend', 'Is Published'),
            'flags' => Yii::t('backend', 'Flags'),
            'is_deleted' => Yii::t('backend', 'Is Deleted'),
        ];
    }
}
