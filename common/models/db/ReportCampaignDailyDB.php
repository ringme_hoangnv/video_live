<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "report_campaign_daily".
 *
 * @property string $id
 * @property string $report_date
 * @property string $partner_id
 * @property string $campaign_id
 * @property string $address
 * @property string $port
 * @property string $data
 * @property string $number_access
 */
class ReportCampaignDailyDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_campaign_daily';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date', 'partner_id', 'campaign_id', 'address', 'port'], 'required'],
            [['report_date'], 'safe'],
            [['partner_id', 'campaign_id', 'data', 'number_access'], 'integer'],
            [['address'], 'string', 'max' => 200],
            [['port'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'report_date' => Yii::t('backend', 'Report Date'),
            'partner_id' => Yii::t('backend', 'Partner ID'),
            'campaign_id' => Yii::t('backend', 'Campaign ID'),
            'address' => Yii::t('backend', 'Address'),
            'port' => Yii::t('backend', 'Port'),
            'data' => Yii::t('backend', 'Data'),
            'number_access' => Yii::t('backend', 'Number Access'),
        ];
    }
}
