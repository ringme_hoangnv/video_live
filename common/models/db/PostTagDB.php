<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "post_tag".
 *
 * @property integer $post_id
 * @property integer $tag_id
 * @property integer $total
 *
 * @property PostDB $post
 * @property TagDB $tag
 */
class PostTagDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'tag_id'], 'required'],
            [['post_id', 'tag_id', 'total'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_id' => Yii::t('backend', 'Post ID'),
            'tag_id' => Yii::t('backend', 'Tag ID'),
            'total' => Yii::t('backend', 'Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(PostDB::className(), ['id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(TagDB::className(), ['id' => 'tag_id']);
    }
}
