<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $pd_code
 * @property string $description
 * @property integer $category_id
 * @property string $image_path
 * @property integer $amount
 * @property integer $booking_amount
 * @property string $published_at
 * @property integer $status
 * @property integer $priority
 * @property integer $is_display_price
 * @property integer $is_hot
 * @property integer $price
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $qr_code_image
 */
class ProductDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'pd_code', 'slug'], 'required'],
            [['description'], 'string'],
            [['category_id', 'amount', 'booking_amount', 'status', 'priority', 'is_display_price', 'is_hot', 'price'], 'integer'],
            [['published_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'image_path', 'slug', 'seo_description', 'seo_keywords', 'qr_code_image'], 'string', 'max' => 255],
            [['pd_code'], 'string', 'max' => 15],
            [['seo_title'], 'string', 'max' => 200],
            [['pd_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'pd_code' => Yii::t('backend', 'Pd Code'),
            'description' => Yii::t('backend', 'Description'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'amount' => Yii::t('backend', 'Amount'),
            'booking_amount' => Yii::t('backend', 'Booking Amount'),
            'published_at' => Yii::t('backend', 'Published At'),
            'status' => Yii::t('backend', 'Status'),
            'priority' => Yii::t('backend', 'Priority'),
            'is_display_price' => Yii::t('backend', 'Is Display Price'),
            'is_hot' => Yii::t('backend', 'Is Hot'),
            'price' => Yii::t('backend', 'Price'),
            'slug' => Yii::t('backend', 'Slug'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
            'qr_code_image' => Yii::t('backend', 'Qr Code Image'),
        ];
    }
}
