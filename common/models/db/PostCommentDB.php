<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "post_comment".
 *
 * @property string $id
 * @property string $post_id
 * @property string $content
 * @property integer $is_active
 * @property string $fullname
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */
class PostCommentDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'post_id', 'created_at', 'updated_at'], 'required'],
            [['id', 'post_id', 'is_active'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['fullname', 'email'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'post_id' => Yii::t('backend', 'Post ID'),
            'content' => Yii::t('backend', 'Content'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'fullname' => Yii::t('backend', 'Fullname'),
            'email' => Yii::t('backend', 'Email'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
}
