<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "oa_feedback_campaign".
 *
 * @property integer $id
 * @property string $title
 * @property string $official_account_id
 * @property string $start_at
 * @property string $end_at
 * @property string $cron_params
 * @property string $cron_expression
 * @property string $input_type
 * @property string $phone_no_list
 * @property string $file_path
 * @property string $msg_type
 * @property string $image_path
 * @property string $image_post_path
 * @property string $image_link
 * @property integer $deeplink_configure_id
 * @property string $deeplink_params
 * @property string $msg_content
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property integer $process_status
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $process_time
 * @property integer $process_id
 * @property string $approved_at
 * @property integer $approved_by
 * @property integer $total
 */
class OaFeedbackCampaignDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oa_feedback_campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['start_at', 'end_at', 'created_at', 'updated_at', 'process_time', 'approved_at'], 'safe'],
            [['phone_no_list', 'deeplink_params', 'msg_content'], 'string'],
            [['deeplink_configure_id', 'created_by', 'updated_by', 'process_id', 'approved_by', 'total'], 'integer'],
            [['title', 'cron_params', 'file_path', 'image_path', 'image_post_path', 'image_link'], 'string', 'max' => 255],
            [['official_account_id'], 'string', 'max' => 50],
            [['cron_expression'], 'string', 'max' => 100],
            [['input_type', 'msg_type'], 'string', 'max' => 30],
            [['status', 'process_status'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title' => Yii::t('backend', 'Title'),
            'official_account_id' => Yii::t('backend', 'Official Account ID'),
            'start_at' => Yii::t('backend', 'Start At'),
            'end_at' => Yii::t('backend', 'End At'),
            'cron_params' => Yii::t('backend', 'Cron Params'),
            'cron_expression' => Yii::t('backend', 'Cron Expression'),
            'input_type' => Yii::t('backend', 'Input Type'),
            'phone_no_list' => Yii::t('backend', 'Phone No List'),
            'file_path' => Yii::t('backend', 'File Path'),
            'msg_type' => Yii::t('backend', 'Msg Type'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'image_post_path' => Yii::t('backend', 'Image Post Path'),
            'image_link' => Yii::t('backend', 'Image Link'),
            'deeplink_configure_id' => Yii::t('backend', 'Deeplink Configure ID'),
            'deeplink_params' => Yii::t('backend', 'Deeplink Params'),
            'msg_content' => Yii::t('backend', 'Msg Content'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'status' => Yii::t('backend', 'Status'),
            'process_status' => Yii::t('backend', 'Process Status'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'process_time' => Yii::t('backend', 'Process Time'),
            'process_id' => Yii::t('backend', 'Process ID'),
            'approved_at' => Yii::t('backend', 'Approved At'),
            'approved_by' => Yii::t('backend', 'Approved By'),
            'total' => Yii::t('backend', 'Total'),
        ];
    }
}
