<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property integer $is_active
 * @property integer $is_hot
 * @property string $priority
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 */
class ProductCategoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['is_active', 'is_hot', 'priority', 'created_by', 'updated_by'], 'integer'],
            [['slug'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'image', 'slug', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['seo_title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
            'image' => Yii::t('backend', 'Image'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'is_hot' => Yii::t('backend', 'Is Hot'),
            'priority' => Yii::t('backend', 'Priority'),
            'slug' => Yii::t('backend', 'Slug'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
        ];
    }
}
