<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "campaign_website".
 *
 * @property string $partner_id
 * @property string $campaign_id
 * @property string $address
 * @property integer $port
 * @property string $start_time
 * @property string $end_time
 */
class CampaignWebsiteDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign_website';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'campaign_id', 'address'], 'required'],
            [['partner_id', 'campaign_id', 'port'], 'integer'],
            [['start_time', 'end_time'], 'safe'],
            [['address'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'partner_id' => Yii::t('backend', 'Partner ID'),
            'campaign_id' => Yii::t('backend', 'Campaign ID'),
            'address' => Yii::t('backend', 'Address'),
            'port' => Yii::t('backend', 'Port'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
        ];
    }
}
