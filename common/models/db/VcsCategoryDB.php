<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vcs_category".
 *
 * @property integer $id
 * @property string $cate_name
 * @property string $icon_image
 * @property string $header_banner
 * @property integer $actived
 * @property integer $display_order
 * @property string $slug
 * @property string $description
 * @property string $updated_at
 * @property string $created_at
 */
class VcsCategoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vcs_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cate_name'], 'required'],
            [['actived', 'display_order'], 'integer'],
            [['description'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['cate_name', 'icon_image', 'header_banner', 'slug'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cate_name' => Yii::t('backend', 'Cate Name'),
            'icon_image' => Yii::t('backend', 'Icon Image'),
            'header_banner' => Yii::t('backend', 'Header Banner'),
            'actived' => Yii::t('backend', 'Actived'),
            'display_order' => Yii::t('backend', 'Display Order'),
            'slug' => Yii::t('backend', 'Slug'),
            'description' => Yii::t('backend', 'Description'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
