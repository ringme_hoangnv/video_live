<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "rep_video".
 *
 * @property integer $id
 * @property string $report_date
 * @property integer $video_id
 * @property integer $total_view
 * @property integer $total_like
 * @property integer $total_follow
 * @property integer $total_unfollow
 * @property integer $total_comment
 * @property integer $total_like_comment
 * @property integer $total_watching_time
 * @property integer $total_play
 */
class RepVideoDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rep_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date'], 'safe'],
            [['video_id', 'total_view', 'total_like', 'total_follow', 'total_unfollow', 'total_comment', 'total_like_comment', 'total_watching_time', 'total_play'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'Report Date',
            'video_id' => 'Video ID',
            'total_view' => 'Total View',
            'total_like' => 'Total Like',
            'total_follow' => 'Total Follow',
            'total_unfollow' => 'Total Unfollow',
            'total_comment' => 'Total Comment',
            'total_like_comment' => 'Total Like Comment',
            'total_watching_time' => 'Total Watching Time',
            'total_play' => 'Total Play',
        ];
    }
}
