<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vcs_dirty_word".
 *
 * @property integer $id
 * @property string $word
 * @property string $updated_time
 */
class DirtyWordDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vcs_dirty_word';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['word'], 'required'],
            [['updated_time'], 'safe'],
            [['word'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'word' => 'Word',
            'updated_time' => 'Updated Time',
        ];
    }
}
