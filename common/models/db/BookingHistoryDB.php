<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "booking_history".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $booking_date
 * @property string $return_date
 * @property integer $cus_id
 * @property string $cus_name
 * @property string $cus_phone
 * @property string $cus_address
 * @property integer $status
 * @property integer $updated_by
 * @property string $created_at
 */
class BookingHistoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'cus_name', 'cus_address'], 'required'],
            [['product_id', 'cus_id', 'status', 'updated_by'], 'integer'],
            [['booking_date', 'return_date', 'created_at'], 'safe'],
            [['cus_name'], 'string', 'max' => 200],
            [['cus_phone'], 'string', 'max' => 15],
            [['cus_address'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'product_id' => Yii::t('backend', 'Product ID'),
            'booking_date' => Yii::t('backend', 'Booking Date'),
            'return_date' => Yii::t('backend', 'Return Date'),
            'cus_id' => Yii::t('backend', 'Cus ID'),
            'cus_name' => Yii::t('backend', 'Cus Name'),
            'cus_phone' => Yii::t('backend', 'Cus Phone'),
            'cus_address' => Yii::t('backend', 'Cus Address'),
            'status' => Yii::t('backend', 'Status'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
