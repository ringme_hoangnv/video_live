<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vcs_banner".
 *
 * @property integer $id
 * @property string $bn_name
 * @property string $bn_image_url
 * @property string $bn_position
 * @property string $bn_action
 * @property string $content_id
 * @property string $bn_media_url
 * @property string $bn_link
 * @property integer $actived
 * @property integer $display_order
 * @property string $publish_time
 * @property string $created_at
 * @property string $updated_at
 */
class BannerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vcs_banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['actived', 'display_order'], 'integer'],
            [['publish_time', 'created_at', 'updated_at'], 'safe'],
            [['bn_name'], 'string', 'max' => 255],
            [['bn_image_url', 'bn_media_url', 'bn_link'], 'string', 'max' => 1000],
            [['bn_position', 'bn_action'], 'string', 'max' => 20],
            [['content_id'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bn_name' => 'Bn Name',
            'bn_image_url' => 'Bn Image Url',
            'bn_position' => 'Bn Position',
            'bn_action' => 'Bn Action',
            'content_id' => 'Content ID',
            'bn_media_url' => 'Bn Media Url',
            'bn_link' => 'Bn Link',
            'actived' => 'Actived',
            'display_order' => 'Display Order',
            'publish_time' => 'Publish Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
