<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $name
 * @property string $short_desc
 * @property string $image_path
 * @property string $content
 * @property integer $is_hot
 * @property integer $status
 * @property string $link
 * @property string $slug
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $priority
 */
class ServiceDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['content'], 'string'],
            [['is_hot', 'status', 'created_by', 'updated_by', 'priority'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'short_desc', 'image_path', 'link', 'slug', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['seo_title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'short_desc' => Yii::t('backend', 'Short Desc'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'content' => Yii::t('backend', 'Content'),
            'is_hot' => Yii::t('backend', 'Is Hot'),
            'status' => Yii::t('backend', 'Status'),
            'link' => Yii::t('backend', 'Link'),
            'slug' => Yii::t('backend', 'Slug'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'priority' => Yii::t('backend', 'Priority'),
            'service_id' => Yii::t('backend', 'Service'),
        ];
    }
}
