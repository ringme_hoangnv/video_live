<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "advertisment".
 *
 * @property string $id
 * @property string $name
 * @property string $channel
 * @property string $position
 * @property integer $show_times
 * @property string $link
 * @property integer $target_type
 * @property string $content
 * @property string $image_path
 * @property string $start_time
 * @property string $end_time
 * @property string $item_type
 * @property string $item_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class AdvertismentDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'advertisment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position'], 'required'],
            [['show_times', 'target_type', 'created_by', 'updated_by', 'is_active','priority' ], 'integer'],
            [['start_time', 'end_time', 'created_at', 'updated_at'], 'safe'],
            [['name', 'position', 'item_type'], 'string', 'max' => 255],
            [['channel'], 'string', 'max' => 15],
            [['link', 'content', 'image_path', 'item_id'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'channel' => Yii::t('backend', 'Channel'),
            'position' => Yii::t('backend', 'Position'),
            'show_times' => Yii::t('backend', 'Show Times'),
            'link' => Yii::t('backend', 'Link'),
            'target_type' => Yii::t('backend', 'Target Type'),
            'content' => Yii::t('backend', 'Content'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'item_type' => Yii::t('backend', 'Item Type'),
            'item_id' => Yii::t('backend', 'Item ID'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'priority' => Yii::t('backend', 'Priority'),
            'is_active' => Yii::t('backend', 'Is Active'),
        ];
    }
}
