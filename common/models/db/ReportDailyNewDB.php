<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "telemor_report_daily_new".
 *
 * @property integer $id
 * @property string $date_report
 * @property integer $new_user
 * @property integer $active_user
 * @property integer $callout_user
 * @property integer $callout_minute
 * @property integer $smsout_user
 * @property integer $number_smsout
 * @property integer $calldata_user
 * @property integer $calldata_minute
 * @property integer $new_user_in_month
 * @property integer $active_user_in_month
 * @property integer $callout_user_in_month
 * @property integer $callout_minute_in_month
 * @property integer $sms_out_user_in_month
 * @property integer $number_smsout_in_month
 * @property integer $calldata_user_in_month
 * @property integer $calldata_minute_in_month
 */
class ReportDailyNewDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telemor_report_daily_new';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_report'], 'safe'],
            [['new_user', 'active_user', 'callout_user', 'callout_minute', 'smsout_user', 'number_smsout', 'calldata_user', 'calldata_minute', 'new_user_in_month', 'active_user_in_month', 'callout_user_in_month', 'callout_minute_in_month', 'sms_out_user_in_month', 'number_smsout_in_month', 'calldata_user_in_month', 'calldata_minute_in_month'], 'integer'],
            [['date_report'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_report' => 'Date Report',
            'new_user' => 'New User',
            'active_user' => 'Active User',
            'callout_user' => 'Callout User',
            'callout_minute' => 'Callout Minute',
            'smsout_user' => 'Smsout User',
            'number_smsout' => 'Number Smsout',
            'calldata_user' => 'Calldata User',
            'calldata_minute' => 'Calldata Minute',
            'new_user_in_month' => 'New User In Month',
            'active_user_in_month' => 'Active User In Month',
            'callout_user_in_month' => 'Callout User In Month',
            'callout_minute_in_month' => 'Callout Minute In Month',
            'sms_out_user_in_month' => 'Sms Out User In Month',
            'number_smsout_in_month' => 'Number Smsout In Month',
            'calldata_user_in_month' => 'Calldata User In Month',
            'calldata_minute_in_month' => 'Calldata Minute In Month',
        ];
    }
}
