<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "selfcare_banner".
 *
 * @property integer $id
 * @property string $name
 * @property string $image_url
 * @property string $deeplink
 * @property string $start_date
 * @property string $end_date
 * @property integer $is_active
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $updated_at
 */
class SelfcareBannerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'selfcare_banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date', 'created_at', 'updated_at', 'id'], 'safe'],
            [['is_active'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['image_url'], 'string', 'max' => 500],
            [['deeplink'], 'string', 'max' => 1024],
            [['created_by', 'updated_by'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image_url' => 'Image Url',
            'deeplink' => 'Deeplink',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
