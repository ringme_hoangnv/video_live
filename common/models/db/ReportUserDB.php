<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "telemor_report_user".
 *
 * @property integer $id
 * @property string $date_report
 * @property string $username
 * @property integer $callout_time
 * @property integer $sms_number
 * @property string $created_at
 */
class ReportUserDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telemor_report_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_report', 'created_at', 'active_day'], 'safe'],
            [['callout_time', 'sms_number'], 'integer'],
            [['username'], 'string', 'max' => 30],
            [['date_report', 'username'], 'unique', 'targetAttribute' => ['date_report', 'username'], 'message' => 'The combination of Date Report and Username has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_report' => 'Date Report',
            'username' => 'Username',
            'callout_time' => 'Callout Time',
            'sms_number' => 'Sms Number',
            'created_at' => 'Created At',
        ];
    }
}
