<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "post_translation".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_desc
 * @property string $content
 * @property string $slug
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 */
class PostTranslationDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['slug'], 'required'],
            [['title', 'short_desc', 'slug', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['seo_title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title' => Yii::t('backend', 'Title'),
            'short_desc' => Yii::t('backend', 'Short Desc'),
            'content' => Yii::t('backend', 'Content'),
            'slug' => Yii::t('backend', 'Slug'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
        ];
    }
}
