<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "telemor_report_daily".
 *
 * @property integer $id
 * @property string $date_report
 * @property integer $total_user
 * @property integer $callout_user
 * @property integer $callout_minute
 * @property integer $smsout_user
 * @property integer $number_smsout
 * @property integer $calldata_user
 * @property integer $calldata_minute
 * @property string $created_at
 * @property integer $delta_total_user
 * @property integer $delta_callout_user
 * @property integer $delta_callout_minute
 * @property integer $delta_smsout_user
 * @property integer $delta_number_smsout
 * @property integer $delta_calldata_user
 * @property integer $delta_calldata_minute
 */
class ReportDailyDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telemor_report_daily';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_report', 'created_at'], 'safe'],
            [['total_user', 'callout_user', 'callout_minute', 'smsout_user', 'number_smsout', 'calldata_user', 'calldata_minute', 'delta_total_user', 'delta_callout_user', 'delta_callout_minute', 'delta_smsout_user', 'delta_number_smsout', 'delta_calldata_user', 'delta_calldata_minute'], 'integer'],
            [['date_report'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_report' => 'Date Report',
            'total_user' => 'Total User',
            'callout_user' => 'Callout User',
            'callout_minute' => 'Callout Minute',
            'smsout_user' => 'Smsout User',
            'number_smsout' => 'Number Smsout',
            'calldata_user' => 'Calldata User',
            'calldata_minute' => 'Calldata Minute',
            'created_at' => 'Created At',
            'delta_total_user' => 'Delta Total User',
            'delta_callout_user' => 'Delta Callout User',
            'delta_callout_minute' => 'Delta Callout Minute',
            'delta_smsout_user' => 'Delta Smsout User',
            'delta_number_smsout' => 'Delta Number Smsout',
            'delta_calldata_user' => 'Delta Calldata User',
            'delta_calldata_minute' => 'Delta Calldata Minute',
        ];
    }
}
