<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "m_attr_tmp".
 *
 * @property string $id
 * @property string $media_id
 * @property string $attribute_id
 * @property integer $count
 */
class MAttrTmpDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_attr_tmp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'media_id', 'attribute_id', 'count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'media_id' => Yii::t('backend', 'Media ID'),
            'attribute_id' => Yii::t('backend', 'Attribute ID'),
            'count' => Yii::t('backend', 'Count'),
        ];
    }
}
