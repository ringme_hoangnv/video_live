<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "telesale_call_log".
 *
 * @property string $id
 * @property string $request_id
 * @property string $sub_id
 * @property string $account_id
 * @property string $callid
 * @property string $caller_phonenumber
 * @property string $calling
 * @property integer $return_code
 * @property string $description
 * @property string $created_at
 * @property string $created_at_int
 * @property string $file_path
 * @property string $hobby
 */
class ReportCallLogDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telesale_call_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_id', 'account_id', 'created_at'], 'required'],
            [['account_id', 'callid', 'return_code', 'created_at_int'], 'integer'],
            [['created_at'], 'safe'],
            [['request_id', 'sub_id', 'description', 'file_path', 'hobby'], 'string', 'max' => 255],
            [['caller_phonenumber'], 'string', 'max' => 45],
            [['calling'], 'string', 'max' => 15],
            [['request_id', 'created_at'], 'unique', 'targetAttribute' => ['request_id', 'created_at'], 'message' => 'The combination of Request ID and Created At has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'request_id' => Yii::t('backend', 'Request ID'),
            'sub_id' => Yii::t('backend', 'Sub ID'),
            'account_id' => Yii::t('backend', 'Account ID'),
            'callid' => Yii::t('backend', 'Callid'),
            'caller_phonenumber' => Yii::t('backend', 'Caller Phonenumber'),
            'calling' => Yii::t('backend', 'Calling'),
            'return_code' => Yii::t('backend', 'Return Code'),
            'description' => Yii::t('backend', 'Description'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_at_int' => Yii::t('backend', 'Created At Int'),
            'file_path' => Yii::t('backend', 'File Path'),
            'hobby' => Yii::t('backend', 'Hobby'),
        ];
    }
}
