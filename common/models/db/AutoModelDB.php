<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "automodel".
 *
 * @property integer $id
 * @property string $name
 * @property string $intro
 * @property integer $automaker_id
 * @property integer $item_type
 * @property string $production_date
 * @property integer $style_id
 * @property integer $gear_box
 * @property integer $fuel_type
 * @property string $exterior_color
 * @property string $interior_color
 * @property integer $num_of_seat
 * @property integer $num_of_door
 * @property integer $price
 * @property string $slug
 * @property string $created_at
 * @property string $updated_at
 * @property string $image_paths
 * @property string $videos
 * @property string $map_id
 * @property string $features
 */
class AutoModelDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'automodel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'automaker_id', 'gear_box'], 'required'],
            [['intro', 'image_paths', 'videos', 'features'], 'string'],
            [['automaker_id', 'item_type', 'style_id', 'gear_box', 'fuel_type', 'num_of_seat', 'num_of_door', 'price'], 'integer'],
            [['production_date', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['exterior_color', 'interior_color'], 'string', 'max' => 50],
            [['slug', 'map_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'intro' => Yii::t('backend', 'Intro'),
            'automaker_id' => Yii::t('backend', 'Automaker ID'),
            'item_type' => Yii::t('backend', 'Item Type'),
            'production_date' => Yii::t('backend', 'Production Date'),
            'style_id' => Yii::t('backend', 'Style ID'),
            'gear_box' => Yii::t('backend', 'Gear Box'),
            'fuel_type' => Yii::t('backend', 'Fuel Type'),
            'exterior_color' => Yii::t('backend', 'Exterior Color'),
            'interior_color' => Yii::t('backend', 'Interior Color'),
            'num_of_seat' => Yii::t('backend', 'Num Of Seat'),
            'num_of_door' => Yii::t('backend', 'Num Of Door'),
            'price' => Yii::t('backend', 'Price'),
            'slug' => Yii::t('backend', 'Slug'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'image_paths' => Yii::t('backend', 'Image Paths'),
            'videos' => Yii::t('backend', 'Videos'),
            'map_id' => Yii::t('backend', 'Map ID'),
            'features' => Yii::t('backend', 'Features'),
        ];
    }
}
