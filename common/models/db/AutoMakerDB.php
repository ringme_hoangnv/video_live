<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "automaker".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $logo
 * @property string $intro
 */
class AutoMakerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'automaker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['intro'], 'string'],
            [['name'], 'string', 'max' => 200],
            [['slug', 'logo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'slug' => Yii::t('backend', 'Slug'),
            'logo' => Yii::t('backend', 'Logo'),
            'intro' => Yii::t('backend', 'Intro'),
        ];
    }
}
