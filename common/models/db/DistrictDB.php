<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "district".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $lati_long_tude
 * @property integer $province_id
 * @property integer $sort_order
 * @property integer $is_published
 * @property integer $is_deleted
 */
class DistrictDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'district';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['province_id'], 'required'],
            [['province_id', 'sort_order', 'is_published', 'is_deleted'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['type', 'lati_long_tude'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'type' => Yii::t('backend', 'Type'),
            'lati_long_tude' => Yii::t('backend', 'Lati Long Tude'),
            'province_id' => Yii::t('backend', 'Province ID'),
            'sort_order' => Yii::t('backend', 'Sort Order'),
            'is_published' => Yii::t('backend', 'Is Published'),
            'is_deleted' => Yii::t('backend', 'Is Deleted'),
        ];
    }
}
