<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "static_page".
 *
 * @property string $id
 * @property string $title
 * @property string $short_desc
 * @property resource $body
 * @property string $published_at
 * @property integer $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $slug
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 */
class StaticPageDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'slug'], 'required'],
            [['short_desc', 'body'], 'string'],
            [['published_at', 'created_at', 'updated_at'], 'safe'],
            [['is_active'], 'integer'],
            [['title', 'slug', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['seo_title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title' => Yii::t('backend', 'Title'),
            'short_desc' => Yii::t('backend', 'Short Desc'),
            'body' => Yii::t('backend', 'Body'),
            'published_at' => Yii::t('backend', 'Published At'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'slug' => Yii::t('backend', 'Slug'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
        ];
    }
}
