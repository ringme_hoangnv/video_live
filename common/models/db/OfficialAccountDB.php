<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "official_account".
 *
 * @property integer $id
 * @property string $s
 * @property string $name
 * @property string $description
 * @property string $image_path
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class OfficialAccountDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'official_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['official_account_id', 'name'], 'required'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['official_account_id'], 'string', 'max' => 200],
            [['name', 'image_path'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'official_account_id' => Yii::t('backend', 'Official Account ID'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
            'image_path' => Yii::t('backend', 'Image Path'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
}
