<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "sticker_item".
 *
 * @property integer $id
 * @property integer $sticker_id
 * @property integer $iorder
 * @property string $image_url
 * @property string $voice_url
 * @property integer $active
 * @property string $created_date
 * @property string $modified_date
 */
class StickerItemDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sticker_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sticker_id'], 'required'],
            [['sticker_id', 'iorder', 'active'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['image_url', 'voice_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'sticker_id' => Yii::t('backend', 'Sticker ID'),
            'iorder' => Yii::t('backend', 'Iorder'),
            'image_url' => Yii::t('backend', 'Image Url'),
            'voice_url' => Yii::t('backend', 'Voice Url'),
            'active' => Yii::t('backend', 'Active'),
            'created_date' => Yii::t('backend', 'Created Date'),
            'modified_date' => Yii::t('backend', 'Modified Date'),
        ];
    }
}
