<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "campaign_history".
 *
 * @property string $id
 * @property string $partner_id
 * @property string $campaign_id
 * @property string $channel
 * @property string $action
 * @property string $reason
 * @property string $created_by
 * @property string $created_at
 */
class CampaignHistoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'campaign_id', 'channel', 'action', 'reason', 'created_at'], 'required'],
            [['partner_id', 'campaign_id', 'created_by'], 'integer'],
            [['reason'], 'string'],
            [['created_at'], 'safe'],
            [['channel'], 'string', 'max' => 100],
            [['action'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'partner_id' => Yii::t('backend', 'Partner ID'),
            'campaign_id' => Yii::t('backend', 'Campaign ID'),
            'channel' => Yii::t('backend', 'Channel'),
            'action' => Yii::t('backend', 'Action'),
            'reason' => Yii::t('backend', 'Reason'),
            'created_by' => Yii::t('backend', 'Created By'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
