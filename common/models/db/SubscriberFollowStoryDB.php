<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "subscriber_follow_story".
 *
 * @property integer $id
 * @property integer $story_id
 * @property integer $subscriber_id
 * @property string $created_at
 */
class SubscriberFollowStoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriber_follow_story';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['story_id', 'subscriber_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'story_id' => Yii::t('backend', 'Story ID'),
            'subscriber_id' => Yii::t('backend', 'Subscriber ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
