<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "subscriber".
 *
 * @property integer $id
 * @property string $oauth_provider
 * @property string $oauth_id
 * @property string $username
 * @property string $email
 * @property string $fullname
 * @property string $avatar
 * @property string $phone_number
 * @property string $reg_time
 * @property string $last_login_time
 * @property integer $status
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 */
class SubscriberDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriber';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['reg_time', 'last_login_time'], 'safe'],
            [['status'], 'integer'],
            [['oauth_provider'], 'string', 'max' => 20],
            [['oauth_id', 'email', 'avatar', 'password_hash', 'password_reset_token', 'auth_key'], 'string', 'max' => 255],
            [['username', 'fullname'], 'string', 'max' => 200],
            [['phone_number'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'oauth_provider' => Yii::t('backend', 'Oauth Provider'),
            'oauth_id' => Yii::t('backend', 'Oauth ID'),
            'username' => Yii::t('backend', 'Username'),
            'email' => Yii::t('backend', 'Email'),
            'fullname' => Yii::t('backend', 'Fullname'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'phone_number' => Yii::t('backend', 'Phone Number'),
            'reg_time' => Yii::t('backend', 'Reg Time'),
            'last_login_time' => Yii::t('backend', 'Last Login Time'),
            'status' => Yii::t('backend', 'Status'),
            'password_hash' => Yii::t('backend', 'Password Hash'),
            'password_reset_token' => Yii::t('backend', 'Password Reset Token'),
            'auth_key' => Yii::t('backend', 'Auth Key'),
        ];
    }
}
