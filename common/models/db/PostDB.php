<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property integer $category_id
 * @property string $short_desc
 * @property string $image
 * @property string $content
 * @property string $source
 * @property integer $is_hot
 * @property integer $status
 * @property string $published_at
 * @property string $author
 * @property integer $view_count
 * @property string $link
 * @property string $slug
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $priority
 */
class PostDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'is_hot', 'status', 'view_count', 'created_by', 'updated_by', 'priority'], 'integer'],
            [['content'], 'string'],
            [['published_at', 'created_at', 'updated_at'], 'safe'],
            [['slug'], 'required'],
            [['title', 'short_desc', 'image', 'source', 'link', 'slug', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['author'], 'string', 'max' => 100],
            [['seo_title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title' => Yii::t('backend', 'Title'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'short_desc' => Yii::t('backend', 'Short Desc'),
            'image' => Yii::t('backend', 'Image'),
            'content' => Yii::t('backend', 'Content'),
            'source' => Yii::t('backend', 'Source'),
            'is_hot' => Yii::t('backend', 'Is Hot'),
            'status' => Yii::t('backend', 'Status'),
            'published_at' => Yii::t('backend', 'Published At'),
            'author' => Yii::t('backend', 'Author'),
            'view_count' => Yii::t('backend', 'View Count'),
            'link' => Yii::t('backend', 'Link'),
            'slug' => Yii::t('backend', 'Slug'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'priority' => Yii::t('backend', 'Priority'),
        ];
    }
}
