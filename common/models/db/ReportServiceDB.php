<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "telemor_report_service".
 *
 * @property integer $id
 * @property string $date_report
 * @property string $service_name
 * @property string $package_name
 * @property integer $number_register
 * @property integer $number_user
 * @property string $created_at
 */
class ReportServiceDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telemor_report_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_report', 'created_at'], 'safe'],
            [['number_register', 'number_user'], 'integer'],
            [['service_name', 'package_name'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_report' => 'Date Report',
            'service_name' => 'Service Name',
            'package_name' => 'Package Name',
            'number_register' => 'Number Register',
            'number_user' => 'Number User',
            'created_at' => 'Created At',
        ];
    }
}
