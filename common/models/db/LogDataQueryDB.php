<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "log_data_query".
 *
 * @property string $id
 * @property string $partner_id
 * @property string $campaign_id
 * @property string $start_time
 * @property string $end_time
 * @property string $address
 * @property string $port
 * @property string $data
 * @property string $number_access
 * @property string $created_at
 * @property integer $is_report
 * @property integer $process_id
 */
class LogDataQueryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_data_query';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'campaign_id', 'address', 'port', 'created_at'], 'required'],
            [['partner_id', 'campaign_id', 'data', 'number_access', 'is_report', 'process_id'], 'integer'],
            [['start_time', 'end_time', 'created_at'], 'safe'],
            [['address'], 'string', 'max' => 200],
            [['port'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'partner_id' => Yii::t('backend', 'Partner ID'),
            'campaign_id' => Yii::t('backend', 'Campaign ID'),
            'start_time' => Yii::t('backend', 'Start Time'),
            'end_time' => Yii::t('backend', 'End Time'),
            'address' => Yii::t('backend', 'Address'),
            'port' => Yii::t('backend', 'Port'),
            'data' => Yii::t('backend', 'Data'),
            'number_access' => Yii::t('backend', 'Number Access'),
            'created_at' => Yii::t('backend', 'Created At'),
            'is_report' => Yii::t('backend', 'Is Report'),
            'process_id' => Yii::t('backend', 'Process ID'),
        ];
    }
}
