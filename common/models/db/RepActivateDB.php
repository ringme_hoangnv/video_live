<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "rep_activate".
 *
 * @property integer $id
 * @property string $report_date
 * @property integer $user_active
 * @property integer $channel_active
 */
class RepActivateDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rep_activate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date'], 'safe'],
            [['user_active', 'channel_active'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'Report Date',
            'user_active' => 'User Active',
            'channel_active' => 'Channel Active',
        ];
    }
}
