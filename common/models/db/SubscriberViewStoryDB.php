<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "subscriber_view_story".
 *
 * @property integer $id
 * @property integer $subscriber_id
 * @property integer $story_id
 * @property integer $chapter_id
 * @property string $created_at
 */
class SubscriberViewStoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriber_view_story';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscriber_id', 'story_id', 'chapter_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'subscriber_id' => Yii::t('backend', 'Subscriber ID'),
            'story_id' => Yii::t('backend', 'Story ID'),
            'chapter_id' => Yii::t('backend', 'Chapter ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
