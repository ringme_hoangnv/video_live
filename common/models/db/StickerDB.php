<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "sticker".
 *
 * @property integer $id
 * @property string $name
 * @property integer $iorder
 * @property string $prefix
 * @property integer $item_total
 * @property string $icon_url
 * @property string $preview_url
 * @property integer $active
 * @property integer $is_new
 * @property integer $sticky
 * @property string $created_date
 * @property string $modified
 * @property string $modified_item_date
 * @property string $sticker_url
 */
class StickerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sticker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iorder', 'item_total', 'active', 'is_new', 'sticky'], 'integer'],
            [['created_date', 'modified', 'modified_item_date'], 'safe'],
            [['name', 'icon_url', 'preview_url'], 'string', 'max' => 255],
            [['prefix'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'iorder' => Yii::t('backend', 'Iorder'),
            'prefix' => Yii::t('backend', 'Prefix'),
            'item_total' => Yii::t('backend', 'Item Total'),
            'icon_url' => Yii::t('backend', 'Icon Url'),
            'preview_url' => Yii::t('backend', 'Preview Url'),
            'active' => Yii::t('backend', 'Active'),
            'is_new' => Yii::t('backend', 'Is New'),
            'sticky' => Yii::t('backend', 'Sticky'),
            'created_date' => Yii::t('backend', 'Created Date'),
            'modified' => Yii::t('backend', 'Modified'),
            'modified_item_date' => Yii::t('backend', 'Modified Item Date'),
        ];
    }
}
