<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $priority
 * @property integer $status
 * @property integer $story_count
 * @property string $slug
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 */
class CategoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['parent_id', 'priority', 'status', 'story_count', 'created_by', 'updated_by'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'slug', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['seo_title'], 'string', 'max' => 200],
            [['name'], 'unique'],
            [['slug'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'parent_id' => Yii::t('backend', 'Parent ID'),
            'priority' => Yii::t('backend', 'Priority'),
            'status' => Yii::t('backend', 'Status'),
            'story_count' => Yii::t('backend', 'Story Count'),
            'slug' => Yii::t('backend', 'Slug'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_by' => Yii::t('backend', 'Updated By'),
            'created_at' => Yii::t('backend', 'Created At'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
        ];
    }
}
