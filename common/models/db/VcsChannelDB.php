<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vcs_channel".
 *
 * @property integer $id
 * @property string $msisdn
 * @property string $channel_name
 * @property string $channel_avatar
 * @property string $header_banner
 * @property integer $actived
 * @property string $description
 * @property integer $num_follows
 * @property integer $num_videos
 * @property integer $is_official
 * @property string $slug
 * @property string $created_from
 * @property string $updated_at
 * @property string $created_at
 */
class VcsChannelDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vcs_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['actived', 'num_follows', 'num_videos', 'is_official'], 'integer'],
            [['description'], 'string'],
            [['created_from', 'updated_at', 'created_at'], 'safe'],
            [['msisdn'], 'string', 'max' => 30],
            [['channel_name', 'channel_avatar', 'header_banner', 'slug'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'msisdn' => Yii::t('backend', 'Msisdn'),
            'channel_name' => Yii::t('backend', 'Channel Name'),
            'channel_avatar' => Yii::t('backend', 'Channel Avatar'),
            'header_banner' => Yii::t('backend', 'Header Banner'),
            'actived' => Yii::t('backend', 'Actived'),
            'description' => Yii::t('backend', 'Description'),
            'num_follows' => Yii::t('backend', 'Num Follows'),
            'num_videos' => Yii::t('backend', 'Num Videos'),
            'is_official' => Yii::t('backend', 'Is Official'),
            'slug' => Yii::t('backend', 'Slug'),
            'created_from' => Yii::t('backend', 'Created From'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
