<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $username
 * @property string $password
 * @property string $created_at
 * @property string $token
 * @property integer $active
 * @property string $token_expired
 * @property string $last_online
 * @property string $last_offline
 * @property string $last_avatar
 * @property string $status
 * @property string $name
 * @property string $client_type
 * @property integer $num_unreaded_msg
 * @property integer $gender
 * @property integer $notification_view
 * @property integer $newuser_notification
 * @property string $birthday
 * @property string $revision
 * @property string $app_version
 * @property string $os_version
 * @property string $domain
 * @property string $last_seen
 * @property integer $num_msg_sended
 * @property string $send_msg_date
 * @property string $device_name
 * @property string $smsfeed_time
 * @property string $birthday_processed_time
 * @property string $password_expired
 * @property string $provision_profile
 * @property integer $active_count
 * @property string $country_code
 * @property string $device_id
 * @property string $language_code
 * @property string $facebook_id
 * @property integer $permission
 * @property string $uuid
 * @property string $last_contact
 * @property string $desk_revision
 * @property string $desk_version
 * @property string $desk_os
 * @property string $desk_device_id
 * @property integer $desktop
 */
class AppUserDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'status'], 'required'],
            [['password', 'token', 'status'], 'string'],
            [['created_at', 'birthday', 'smsfeed_time', 'birthday_processed_time', 'password_expired'], 'safe'],
            [['token_expired', 'last_online', 'last_offline', 'last_avatar', 'last_seen', 'last_contact'], 'number'],
            [['active_count', 'permission', 'desktop'], 'integer'],
            [['username'], 'string', 'max' => 30],
            [['active', 'num_unreaded_msg', 'gender'], 'string', 'max' => 4],
            [['name'], 'string', 'max' => 250],
            [['client_type', 'revision'], 'string', 'max' => 7],
            [['notification_view', 'newuser_notification'], 'string', 'max' => 1],
            [['app_version', 'domain', 'provision_profile', 'device_id', 'desk_device_id'], 'string', 'max' => 50],
            [['os_version', 'device_name'], 'string', 'max' => 150],
            [['num_msg_sended'], 'string', 'max' => 2],
            [['send_msg_date'], 'string', 'max' => 10],
            [['country_code'], 'string', 'max' => 5],
            [['language_code', 'facebook_id'], 'string', 'max' => 20],
            [['uuid'], 'string', 'max' => 40],
            [['desk_revision', 'desk_version', 'desk_os'], 'string', 'max' => 100],
            [['uuid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'password' => 'Password',
            'created_at' => 'Created At',
            'token' => 'Token',
            'active' => 'Active',
            'token_expired' => 'Token Expired',
            'last_online' => 'Last Online',
            'last_offline' => 'Last Offline',
            'last_avatar' => 'Last Avatar',
            'status' => 'Status',
            'name' => 'Name',
            'client_type' => 'Client Type',
            'num_unreaded_msg' => 'Num Unreaded Msg',
            'gender' => 'Gender',
            'notification_view' => 'Notification View',
            'newuser_notification' => 'Newuser Notification',
            'birthday' => 'Birthday',
            'revision' => 'Revision',
            'app_version' => 'App Version',
            'os_version' => 'Os Version',
            'domain' => 'Domain',
            'last_seen' => 'Last Seen',
            'num_msg_sended' => 'Num Msg Sended',
            'send_msg_date' => 'Send Msg Date',
            'device_name' => 'Device Name',
            'smsfeed_time' => 'Smsfeed Time',
            'birthday_processed_time' => 'Birthday Processed Time',
            'password_expired' => 'Password Expired',
            'provision_profile' => 'Provision Profile',
            'active_count' => 'Active Count',
            'country_code' => 'Country Code',
            'device_id' => 'Device ID',
            'language_code' => 'Language Code',
            'facebook_id' => 'Facebook ID',
            'permission' => 'Permission',
            'uuid' => 'Uuid',
            'last_contact' => 'Last Contact',
            'desk_revision' => 'Desk Revision',
            'desk_version' => 'Desk Version',
            'desk_os' => 'Desk Os',
            'desk_device_id' => 'Desk Device ID',
            'desktop' => 'Desktop',
        ];
    }
}
