<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "car_style".
 *
 * @property integer $id
 * @property string $name
 * @property string $image_path
 */
class CarStyleDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'car_style';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
            [['image_path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'image_path' => Yii::t('backend', 'Image Path'),
        ];
    }
}
