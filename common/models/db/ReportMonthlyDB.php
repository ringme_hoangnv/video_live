<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "telemor_report_monthly".
 *
 * @property integer $id
 * @property string $month_report
 * @property integer $new_user_active
 * @property integer $total_user
 * @property integer $user_kakoak_call
 * @property integer $minute_kakoak_call
 * @property integer $user_callout
 * @property integer $minute_callout
 * @property integer $user_smsout
 * @property integer $total_smsout
 * @property integer $delta_new_user
 * @property integer $delta_total_user
 * @property integer $delta_callout_user
 * @property integer $delta_callout_minute
 * @property integer $delta_smsout_user
 * @property integer $delta_number_smsout
 * @property integer $delta_calldata_user
 * @property integer $delta_calldata_minute
 * @property string $created_at
 */
class ReportMonthlyDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'telemor_report_monthly';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month_report', 'created_at'], 'safe'],
            [['new_user_active', 'total_user', 'user_kakoak_call', 'minute_kakoak_call', 'user_callout', 'minute_callout', 'user_smsout', 'total_smsout', 'delta_new_user', 'delta_total_user', 'delta_callout_user', 'delta_callout_minute', 'delta_smsout_user', 'delta_number_smsout', 'delta_calldata_user', 'delta_calldata_minute'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'month_report' => 'Month Report',
            'new_user_active' => 'New User Active',
            'total_user' => 'Total User',
            'user_kakoak_call' => 'User Kakoak Call',
            'minute_kakoak_call' => 'Minute Kakoak Call',
            'user_callout' => 'User Callout',
            'minute_callout' => 'Minute Callout',
            'user_smsout' => 'User Smsout',
            'total_smsout' => 'Total Smsout',
            'delta_new_user' => 'Delta New User',
            'delta_total_user' => 'Delta Total User',
            'delta_callout_user' => 'Delta Callout User',
            'delta_callout_minute' => 'Delta Callout Minute',
            'delta_smsout_user' => 'Delta Smsout User',
            'delta_number_smsout' => 'Delta Number Smsout',
            'delta_calldata_user' => 'Delta Calldata User',
            'delta_calldata_minute' => 'Delta Calldata Minute',
            'created_at' => 'Created At',
        ];
    }
}
