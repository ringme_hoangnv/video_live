<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "vcs_video_item".
 *
 * @property integer $id
 * @property integer $cate_id
 * @property integer $channel_id
 * @property string $video_title
 * @property string $video_desc
 * @property string $video_media
 * @property string $video_image
 * @property string $image_thumb
 * @property string $image_small
 * @property integer $video_time
 * @property integer $actived
 * @property integer $is_new
 * @property integer $is_hot
 * @property string $publish_time
 * @property string $first_publish_time
 * @property integer $total_views
 * @property integer $total_likes
 * @property integer $total_shares
 * @property integer $total_comments
 * @property string $slug
 * @property integer $has_live
 * @property integer $resolution
 * @property string $aspec_ratio
 * @property integer $is_adaptive
 * @property string $adaptive_path
 * @property string $adaptive_resolution
 * @property integer $encode_type
 * @property string $created_at
 * @property string $updated_at
 * @property string $note
 */
class VideoItemDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vcs_video_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cate_id', 'channel_id', 'video_time', 'actived', 'is_new', 'is_hot', 'total_views', 'total_likes', 'total_shares', 'total_comments', 'has_live', 'resolution', 'is_adaptive', 'encode_type'], 'integer'],
            [['video_desc', 'note'], 'string'],
            [['publish_time', 'first_publish_time', 'created_at', 'updated_at'], 'safe'],
            [['video_title', 'video_media', 'video_image', 'image_thumb', 'image_small', 'slug', 'adaptive_path', 'adaptive_resolution'], 'string', 'max' => 500],
            [['aspec_ratio'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cate_id' => 'Cate ID',
            'channel_id' => 'Channel ID',
            'video_title' => 'Video Title',
            'video_desc' => 'Video Desc',
            'video_media' => 'Video Media',
            'video_image' => 'Video Image',
            'image_thumb' => 'Image Thumb',
            'image_small' => 'Image Small',
            'video_time' => 'Video Time',
            'actived' => 'Actived',
            'is_new' => 'Is New',
            'is_hot' => 'Is Hot',
            'publish_time' => 'Publish Time',
            'first_publish_time' => 'First Publish Time',
            'total_views' => 'Total Views',
            'total_likes' => 'Total Likes',
            'total_shares' => 'Total Shares',
            'total_comments' => 'Total Comments',
            'slug' => 'Slug',
            'has_live' => 'Has Live',
            'resolution' => 'Resolution',
            'aspec_ratio' => 'Aspec Ratio',
            'is_adaptive' => 'Is Adaptive',
            'adaptive_path' => 'Adaptive Path',
            'adaptive_resolution' => 'Adaptive Resolution',
            'encode_type' => 'Encode Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'note' => 'Note',
        ];
    }
}
