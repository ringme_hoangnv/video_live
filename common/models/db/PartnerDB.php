<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property string $id
 * @property string $partner_code
 * @property string $name
 * @property string $description
 * @property string $representative
 * @property string $register_number
 * @property string $founding_date
 * @property string $business_area
 * @property string $email_list
 * @property string $phone_list
 * @property string $fax
 * @property string $website
 * @property string $created_at
 * @property string $updated_at
 */
class PartnerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
//            [['partner_code', 'name'], 'required'],
//            [['description', 'email_list', 'phone_list'], 'string'],
//            [['founding_date', 'created_at', 'updated_at'], 'safe'],
//            [['partner_code'], 'string', 'max' => 20],
//            [['name', 'fax', 'website'], 'string', 'max' => 255],
//            [['representative', 'business_area'], 'string', 'max' => 200],
//            [['register_number'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'partner_code' => Yii::t('backend', 'Partner Code'),
            'name' => Yii::t('backend', 'Name'),
            'description' => Yii::t('backend', 'Description'),
            'representative' => Yii::t('backend', 'Representative'),
            'register_number' => Yii::t('backend', 'Register Number'),
            'founding_date' => Yii::t('backend', 'Founding Date'),
            'business_area' => Yii::t('backend', 'Business Area'),
            'email_list' => Yii::t('backend', 'Email List'),
            'phone_list' => Yii::t('backend', 'Phone List'),
            'fax' => Yii::t('backend', 'Fax'),
            'website' => Yii::t('backend', 'Website'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
}
