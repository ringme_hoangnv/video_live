<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "deeplink_configure".
 *
 * @property integer $id
 * @property string $title
 * @property string $deeplink
 * @property integer $is_active
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class DeeplinkConfigureDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deeplink_configure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'deeplink'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['deeplink'], 'string', 'max' => 2000],
            [['is_active'], 'string', 'max' => 4],
            [['description'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'title' => Yii::t('backend', 'Title'),
            'deeplink' => Yii::t('backend', 'Deeplink'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'description' => Yii::t('backend', 'Description'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
}
