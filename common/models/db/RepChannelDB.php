<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "rep_channel".
 *
 * @property integer $id
 * @property string $report_date
 * @property integer $channel_id
 * @property integer $total_view
 * @property integer $total_like
 * @property integer $total_follow
 * @property integer $total_unfollow
 * @property integer $total_comment
 * @property integer $total_like_comment
 */
class RepChannelDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rep_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date'], 'safe'],
            [['channel_id', 'total_view', 'total_like', 'total_follow', 'total_unfollow', 'total_comment', 'total_like_comment'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'Report Date',
            'channel_id' => 'Channel ID',
            'total_view' => 'Total View',
            'total_like' => 'Total Like',
            'total_follow' => 'Total Follow',
            'total_unfollow' => 'Total Unfollow',
            'total_comment' => 'Total Comment',
            'total_like_comment' => 'Total Like Comment',
        ];
    }
}
