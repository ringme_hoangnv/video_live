<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property integer $telephone_code
 * @property string $zip_code
 * @property integer $country_id
 * @property string $country_code
 * @property integer $sort_order
 * @property integer $is_published
 * @property integer $is_deleted
 */
class ProvinceDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_id'], 'required'],
            [['telephone_code', 'country_id', 'sort_order', 'is_published', 'is_deleted'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['type', 'zip_code'], 'string', 'max' => 20],
            [['country_code'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'type' => Yii::t('backend', 'Type'),
            'telephone_code' => Yii::t('backend', 'Telephone Code'),
            'zip_code' => Yii::t('backend', 'Zip Code'),
            'country_id' => Yii::t('backend', 'Country ID'),
            'country_code' => Yii::t('backend', 'Country Code'),
            'sort_order' => Yii::t('backend', 'Sort Order'),
            'is_published' => Yii::t('backend', 'Is Published'),
            'is_deleted' => Yii::t('backend', 'Is Deleted'),
        ];
    }
}
