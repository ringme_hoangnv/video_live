<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "static_page_translation".
 *
 * @property integer $id
 * @property string $lang
 * @property string $title
 * @property string $short_desc
 * @property resource $body
 * @property string $slug
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 */
class StaticPageTranslationDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'static_page_translation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body', 'slug'], 'required'],
            [['short_desc', 'body'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['title', 'slug', 'seo_description', 'seo_keywords'], 'string', 'max' => 255],
            [['seo_title'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'lang' => Yii::t('backend', 'Lang'),
            'title' => Yii::t('backend', 'Title'),
            'short_desc' => Yii::t('backend', 'Short Desc'),
            'body' => Yii::t('backend', 'Body'),
            'slug' => Yii::t('backend', 'Slug'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
        ];
    }
}
