<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;

class SelfcareBannerBase extends \common\models\db\SelfcareBannerDB
{


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image_url' => 'Image Url',
            'deeplink' => 'Deeplink',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'is_active' => 'Active/ Deactive',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    public function getCreatedBy() {
        return $this->hasOne(UserBase::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy() {
        return $this->hasOne(UserBase::className(), ['id' => 'updated_by']);
    }
}