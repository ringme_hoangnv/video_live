<?php

namespace common\models;

use Yii;
    use yii\behaviors\SluggableBehavior;
    use backend\components\behaviors\UnicodeSluggableBehavior;
class JobTranslationBase extends \common\models\db\JobTranslationDB {


    public function behaviors() {
        return [
                        [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'title',
            ],
                                            ];
    }


}