<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_transaction_star".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $user_id
 * @property int|null $streamer_id
 * @property int|null $livestream_id
 * @property string|null $type_payment
 * @property int|null $amount_star
 * @property string|null $time_request
 * @property string|null $time_done
 * @property float|null $money_value
 * @property int|null $gift_id
 */
class TblTransactionStar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_transaction_star';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['streamer_id', 'livestream_id', 'amount_star', 'gift_id'], 'integer'],
            [['time_request', 'time_done'], 'safe'],
            [['money_value'], 'number'],
            [['type', 'type_payment'], 'string', 'max' => 45],
            [['user_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'user_id' => 'User ID',
            'streamer_id' => 'Streamer ID',
            'livestream_id' => 'Livestream ID',
            'type_payment' => 'Type Payment',
            'amount_star' => 'Amount Star',
            'time_request' => 'Time Request',
            'time_done' => 'Time Done',
            'money_value' => 'Money Value',
            'gift_id' => 'Gift ID',
        ];
    }
}
