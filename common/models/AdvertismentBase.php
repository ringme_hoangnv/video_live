<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class AdvertismentBase extends \common\models\db\AdvertismentDB {



    /**
     * Ham tra ve danh sach cac quang cao theo Position
     * @param $position
     * @param int $limit
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getActiveAdvByPosition($position, $limit = 8) {
        return self::find()
            ->where([
                'position' => $position,
                'is_active' => 1,
            ])
            ->andWhere(['AND',
                ['<', 'start_time', date('Y-m-d H:i:s')],
                ['>', 'end_time', date('Y-m-d H:i:s')],
            ])
            ->orderBy('priority DESC, id DESC')
            ->limit($limit)
            ->all();
    }
}