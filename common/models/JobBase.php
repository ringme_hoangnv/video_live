<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use \yii\helpers\Url;

class JobBase extends \common\models\db\JobDB
{


    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'name',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function getWebDetailUrl()
    {

        return Url::to([
            'career/detail',
            'job_slug' => $this->getSlug(),
            'job_id' => $this->id,
            'lang' => Yii::$app->language
        ]);
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getImageUrl($absolute = false)
    {
        return $this->image_path;
    }

    public static function getActivatedJobQuery()
    {
        return self::find()
            ->alias('a')
            ->joinWith('translation t')
            ->select('a.id, a.status, a.category_id, a.image_path, a.amount, a.salary, a.start_time, a.end_time, a.created_at, a.is_hot, t.slug, t.name, t.work_location, t.level,t.content, t.company_name')
            ->where([
                'a.status' => 1,
            ])
            //->andWhere([
            //    '<', 'published_at', date('Y-m-d H:i:s')
            //])
            ->orderBy('a.id DESC');
    }

    public function getTranslation() {

        return $this->hasOne(JobTranslationBase::className(), ['id' => 'id'])
            ->andWhere(['lang' => Yii::$app->language]);
//            ->andWhere(['lang' => Yii::$app->session->get('language', Yii::$app->params['default_content_lang'])]);
    }
}