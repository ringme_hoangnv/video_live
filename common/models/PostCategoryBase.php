<?php

namespace common\models;

use frontend\models\Post;
use Yii;
use yii\helpers\Url;

class PostCategoryBase extends \common\models\db\PostCategoryDB {

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Tên'),
            'image' => Yii::t('backend', 'Ảnh đại diện'),
            'slug' => Yii::t('backend', 'Slug'),
            'seo_title' => Yii::t('backend', 'Seo Title'),
            'seo_description' => Yii::t('backend', 'Seo Description'),
            'seo_keywords' => Yii::t('backend', 'Seo Keywords'),
            'created_at' => Yii::t('backend', 'Created At'),
            'created_by' => Yii::t('backend', 'Created By'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'updated_by' => Yii::t('backend', 'Updated By'),
        ];
    }

    public function getWebDetailUrl() {

        return Url::to([
            'post/list-by-cate',
            'cate_slug' => $this->slug,
            'cate_id' => $this->id
        ]);


    }

    public static function getActiveCategoriesByParent($parentId = null) {

        $query = self::find()
            ->where('is_active = 1')
            ->orderBy('priority ASC, name ASC')
        ;

        if (intval($parentId) > 0) {
            $query->andWhere(['parent_id' => $parentId]);
        } else {
            $query->andWhere('parent_id is NULL');
        }
        return $query->all();
    }

    public static function getAllActiveCategory() {
        return self::find()
            ->where('is_active = 1')
            ->orderBy('priority DESC, name ASC')
            ->all();

    }

    public function getCreatorUserName() {
        $user = $this->getCreator();
        return ($user)? $user->username: '-';
    }

    public function getUpdaterUserName() {
        $user = $this->getUpdater();
        return ($user)? $user->username: '-';
    }

    public function getCreator() {
        $user = null;

        if ($this->created_by) {
            $user = User::find()
                ->where('id = :userId', [
                    ':userId' => $this->created_by
                ])
                ->one();
        }
        return $user;
    }

    public function getUpdater() {
        $user = null;

        if ($this->updated_by) {
            $user = User::find()
                ->where('id = :userId', [
                    ':userId' => $this->updated_by
                ])
                ->one();
        }
        return $user;
    }

    public function getActivePostQuery()
    {
        return Post::find()
            ->where('is_active = 1')
            ->andWhere('category_id = ?', $this->id)
            ->andWhere(['>', ['published_at' => date('Y-m-d H:i:s')]])
            ->orderBy('priority DESC, is_hot DESC, id DESC')
            ;

    }
}