<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_livestream".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string $link_avatar
 * @property string|null stream_alias
 * @property string|null $rtmp_push
 * @property int $status
 * @property string|null $stream_key
 * @property string|null $hls_play_link
 * @property string|null $hls_play_vod
 * @property string|null $time_event_start
 * @property string|null $time_event_finish
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $channel_id
 * @property string|null $profile
 * @property int|null $total_view
 * @property int|null $total_comment
 * @property int|null $total_share
 * @property bool|null $enable_time_event
 * @property bool|null $enable_record
 * @property string|null $time_start
 * @property string|null $time_finish
 * @property int|null $category
 * @property int|null $total_like
 * @property int|null $link_play_record
 */
class TblLivestream extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_livestream';
    }

    public static function getDb()
    {
        return Yii::$app->get('db1');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'channel_id', 'profile', 'category', 'link_avatar', 'time_event_start', 'time_event_finish'], 'required'],
            [['description'], 'string'],
            [['status', 'channel_id', 'total_view', 'total_comment', 'total_share', 'category', 'total_like', 'profile','total_star','age_limit','average_watch_time','enable_chat','chat_setting'], 'integer'],
            [['time_event_start', 'time_event_finish', 'created_at', 'updated_at', 'time_start', 'time_finish'], 'safe'],
            [['enable_time_event', 'enable_record'], 'boolean'],
            [['title', 'hls_play_link', 'stream_alias', 'rtmp_push', 'link_avatar'], 'string', 'max' => 255],
            [['stream_key'], 'string', 'max' => 50],


            ['time_event_start', 'compare', 'compareValue' => date("Y-m-d H:i:s"), 'operator' => '>', // cau lenh kiem tra thoi gian
                'message' => (Yii::$app->request->post('approve') == 'approve') ?
                Yii::t('backend', 'Start time must be greater than approve time!'):
                Yii::t('backend', 'Start time must be greater than current time!')
        ],

        ['time_event_finish', 'compare', 'compareAttribute'=> 'time_event_start', 'operator' => '>', 'enableClientValidation' =>true],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'stream_key' => 'Stream Key',
            'hls_play_link' => 'Hls Play Link',
            'time_event_start' => 'Time Event Start',
            'time_event_finish' => 'Time Event Finish',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'channel_id' => 'Channel ID',
            'profile' => 'Profile',
            'total_view' => 'Total View',
            'total_comment' => 'Total Comment',
            'total_share' => 'Total Share',
            'enable_time_event' => 'Enable Time Event',
            'enable_record' => 'Record Live Streams as MP4',
            'time_start' => 'Time Start',
            'time_finish' => 'Time Finish',
            'category' => 'Category',
            'total_like' => 'Total Like',
            'link_avatar' => 'Avatar',
            'link_play_record' => 'link_play_record',
            'link_play_vod' => 'link_play_vod',
            'total_star' => 'total_star',
            'age_limit' => 'age_limit',
            'average_watch_time'=>'average_watch_time',
            'enable_chat'=>'enable_chat',
            'chat_setting'=>'chat_setting'
        ];
    }

    public function getCategory() {
        if($this->category == 1) {
            return 'Sport';
        }else if($this->category == 2) {
            return 'Travel';
        }else if($this->category == 3) {
            return 'VLOG';
        }
    }

    public function getProfile() {
        if($this->profile == 1) {
            return '1080p, 720p, 480p, 360p';
        }else if($this->profile == 2) {
            return '720p, 480p, 360p';
        }else if($this->profile == 3) {
            return '480p, 360p';
        }else if($this->profile == 4) {
            return 'Live';
        }
    }
    public function getImagePath() {
        return  ($this->link_avatar.'?t='. time());
    }
}
