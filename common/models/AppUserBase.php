<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class AppUserBase extends \common\models\db\AppUserDB
{
    /**
     * dung db khac
     * @return mixed
     */
    public static function getDb() {
        return Yii::$app->db2;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * active = 0: NOT LOGIN, active = 1 LOGIN, active = 2 Deactive, active = 4 Blocked
     */
    public function getActiveName() {
        $arr = self::getActiveStatusArr();
        if (isset($arr[$this->active]))
            return $arr[$this->active];

        return $this->active;
    }

    public static function getActiveStatusArr() {
        return [
            0 => Yii::t('backend', 'Not Login'),
            1 => Yii::t('backend', 'Login'),
            2 => Yii::t('backend', 'Deactive'),
            4 => Yii::t('backend', 'Blocked'),
        ];
    }
}