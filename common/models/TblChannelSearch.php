<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TblChannel;

/**
 * TblChannelSearch represents the model behind the search form of `common\models\TblChannel`.
 */
class TblChannelSearch extends TblChannel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'total_follow', 'total_time_live', 'total_livestream', 'total_view'], 'integer'],
            [['name', 'description', 'username'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblChannel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'total_follow' => $this->total_follow,
            'total_time_live' => $this->total_time_live,
            'total_livestream' => $this->total_livestream,
            'total_view' => $this->total_view,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }

    public function searchLastest($params)
    {
        $query = TblLivestream::find()
            ->where(['channel_id' => \Yii::$app->getUser()->id])
            ->andWhere(['<=', 'time_finish', date("Y-m-d H:i:s")]);
//            ->orderBy(['time_finish'=>'DES'])->limit(0);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['time_finish' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);
        return $dataProvider;
    }

    public function searchTopLivestream($params)
    {
        $query = TblLivestream::find()
            ->where(['channel_id' => \Yii::$app->getUser()->id])
            ->andWhere(['>=', 'total_view', 0]);
//            ->orderBy(['time_finish'=>'DES'])->limit(0);
        $dataProviderTopLivestream = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['total_view' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        return $dataProviderTopLivestream;
    }
}
