<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class DeeplinkConfigureBase extends \common\models\db\DeeplinkConfigureDB
{
    public static function getActiveList() {
        return self::find()
            ->where([
                'is_active' => 1,
            ])
            ->all();
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


}