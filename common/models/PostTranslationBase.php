<?php

namespace common\models;

use Yii;
    use yii\behaviors\SluggableBehavior;
    use backend\components\behaviors\UnicodeSluggableBehavior;
class PostTranslationBase extends \common\models\db\PostTranslationDB {


    public function behaviors() {
        return [
                        [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'title',
            ],
                                            ];
    }


}