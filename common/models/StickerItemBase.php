<?php

namespace common\models;

use Yii;

class StickerItemBase extends \common\models\db\StickerItemDB
{


    public function behaviors()
    {
        return [
        ];
    }

    /**
     * -1: delete; 0: deactive; 1:active
     * @return array
     */
    public static function getStatusArr() {
        return [
            -1 =>   Yii::t('backend', 'Deleted'),
            1 => Yii::t('backend', 'Active'),
            0 => Yii::t('backend', 'Deactive')

        ];
    }

    public function getStatusName() {
        // -1: delete; 0: deactive; 1:active
        $statusArr = self::getStatusArr();
        if (isset($statusArr[$this->active])) {
            return $statusArr[$this->active];
        } else
            return $this->active;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'sticker_id' => Yii::t('backend', 'Sticker ID'),
            'iorder' => Yii::t('backend', 'Iorder'),
            'image_url' => Yii::t('backend', 'Image Url'),
            'voice_url' => Yii::t('backend', 'Voice Url'),
            'active' => Yii::t('backend', 'State'),
            'created_date' => Yii::t('backend', 'Created Date'),
            'modified_date' => Yii::t('backend', 'Modified Date'),
        ];
    }

    public function getVoiceUrl() {
        return Yii::$app->params['sticker_url']. $this->voice_url. '?t='. strtotime($this->modified_date);
    }

    public function getImageUrl() {
        return Yii::$app->params['sticker_url']. $this->image_url. '?t='. strtotime($this->modified_date);
    }

    public function getVoiceFileName() {
        return basename($this->voice_url);
    }

    public function getImageFileName() {
        return basename($this->image_url);
    }

    public function getSticker() {
        return $this->hasOne(StickerBase::className(), ['id' => 'sticker_id']);
    }
}