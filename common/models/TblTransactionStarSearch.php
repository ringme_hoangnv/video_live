<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TblTransactionStar;

/**
 * TblTransactionStarSearch represents the model behind the search form of `common\models\TblTransactionStar`.
 */
class TblTransactionStarSearch extends TblTransactionStar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'streamer_id', 'livestream_id', 'amount_star', 'gift_id'], 'integer'],
            [['type', 'user_id', 'type_payment', 'time_request', 'time_done'], 'safe'],
            [['money_value'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$time)
    {
        $query= \common\models\TblTransactionStar::find()->where(['type'=>'DONATE']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'streamer_id' => $this->streamer_id,
            'livestream_id' => $this->livestream_id,
            'amount_star' => $this->amount_star,
            'time_request' => $this->time_request,
            'time_done' => $this->time_done,
            'money_value' => $this->money_value,
            'gift_id' => $this->gift_id,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'user_id', $this->user_id])
            ->andFilterWhere(['like', 'type_payment', $this->type_payment]);

        return $dataProvider;
    }
}
