<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class BannerBase extends \common\models\db\BannerDB
{
    const DELETED_STATUS = -1;
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getAbsImagePath()
    {
        return '/cms_upload/banner/' . date('Y/m/d/');
    }
    public function getBnImageUrlUrl() {
        return (strpos($this->bn_image_url, 'http') !== 0)? Yii::$app->params['media_url']. $this->bn_image_url: $this->bn_image_url;
    }

    public function getActiveName()
    {
        $activeDefineArr = self::getActiveDefineArr();
        return key_exists($this->actived, $activeDefineArr) ? $activeDefineArr[$this->actived] : $this->actived;
    }
    public static function getActiveDefineArr() {
        return [
            -1 => Yii::t('backend', 'Deleted'),
            0 => Yii::t('backend', 'Inactived'),
            1 => Yii::t('backend', 'Actived'),
        ];
    }

    public function getPositionName()
    {
        $arr = self::getBannerPositionArr();
        return key_exists($this->bn_position, $arr) ? $arr[$this->bn_position] : $this->bn_position;
    }
    /**
     * home,video_detail,channel_detail
     * @return array
     */
    public static function getBannerPositionArr() {
        return [
            'home' =>Yii::t('backend', 'Home'),
//            'video_detail' => Yii::t('backend', 'Video detail'),
//            'channel_detail' => Yii::t('backend', 'Channel detail'),
        ];
    }

    public function getActionName()
    {
        $arr = self::getBannerActionArr();
        return key_exists($this->bn_action, $arr) ? $arr[$this->bn_action] : $this->bn_action;
    }
    /**
     * video,channel,webview,
     * @return array
     */
    public static function getBannerActionArr() {
        return [
            'video' =>Yii::t('backend', 'Video'),
            'channel' => Yii::t('backend', 'Channel'),
            'webview' => Yii::t('backend', 'Webview'),
        ];
    }
}