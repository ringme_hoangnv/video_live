<?php

namespace common\models;

use Yii;

class SubscriberViewStoryBase extends \common\models\db\SubscriberViewStoryDB {


    public function getChapter()
    {
        return $this->hasOne(ChapterBase::className(), ['id' => 'chapter_id']);
    }
    public function getStory()
    {
        return $this->hasOne(StoryBase::className(), ['id' => 'story_id']);
    }
}