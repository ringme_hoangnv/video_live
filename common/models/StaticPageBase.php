<?php

namespace common\models;

use backend\models\StaticPageTranslation;
use Yii;
use yii\helpers\Url;

class StaticPageBase extends \common\models\db\StaticPageDB {

    public function getWebDetailUrl($absolute = false)
    {

        return Url::to([
            'static-page/index',
            'page_slug' => $this->slug,
            'page_id' => $this->id,
            'lang' => Yii::$app->language,
        ], $absolute);
    }

    public static function getActivatedQuery() {
        return self::find()
            ->alias('a')
            ->joinWith('translation t')
            ->select('a.id, a.image_path, a.is_active, a.published_at, t.title, t.short_desc, t.body, t.slug, t.seo_title, t.seo_description, t.seo_keywords')
            ->where([
                'a.is_active' => 1,
            ])
            //->andWhere([
            //    '<', 'a.published_at', date('Y-m-d H:i:s')
            //])
            ->orderBy('a.id DESC');
    }

    public function getTranslation() {

        return $this->hasOne(StaticPageTranslation::className(), ['id' => 'id'])
        ->andWhere(['lang' => Yii::$app->language]);
        // Yii::$app->session->get('language', Yii::$app->params['default_content_lang'])]);
    }
}