<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HistoryLiveStream;

/**
 * HistoryLiveStreamSearch represents the model behind the search form of `common\models\HistoryLiveStream`.
 */
class HistoryLiveStreamSearch extends HistoryLiveStream
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'stream_id', 'user_id', 'profile', 'total_comment', 'total_like', 'total_share', 'total_view', 'category'], 'integer'],
            [['title', 'description', 'stream_key', 'hls_play_link', 'time_event_start', 'time_event_finish', 'vod_play_back', 'created_at', 'updated_at', 'time_finish', 'time_start', 'rtmp_push', 'stream_alias', 'link_avatar'], 'safe'],
            [['enable_record'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HistoryLiveStream::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'time_event_start' => $this->time_event_start,
            'time_event_finish' => $this->time_event_finish,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'stream_id' => $this->stream_id,
            'user_id' => $this->user_id,
            'profile' => $this->profile,
            'total_comment' => $this->total_comment,
            'total_like' => $this->total_like,
            'total_share' => $this->total_share,
            'total_view' => $this->total_view,
            'enable_record' => $this->enable_record,
            'time_finish' => $this->time_finish,
            'time_start' => $this->time_start,
            'category' => $this->category,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'stream_key', $this->stream_key])
            ->andFilterWhere(['like', 'hls_play_link', $this->hls_play_link])
            ->andFilterWhere(['like', 'vod_play_back', $this->vod_play_back])
            ->andFilterWhere(['like', 'rtmp_push', $this->rtmp_push])
            ->andFilterWhere(['like', 'stream_alias', $this->stream_alias])
            ->andFilterWhere(['like', 'link_avatar', $this->link_avatar]);

        return $dataProvider;
    }
}
