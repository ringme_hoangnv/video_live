<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class VcsCategoryBase extends \common\models\db\VcsCategoryDB
{
    const DELETED_STATUS = -1;

    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'cate_name',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getAbsImagePath()
    {
        return '/cms_upload/category/' . date('Y/m/d/');
    }
    public function getIconImageUrl() {
        return (strpos($this->icon_image, 'http') !== 0)? Yii::$app->params['media_url']. $this->icon_image: $this->icon_image;
    }
    public function getHeaderBannerUrl() {
        return (strpos($this->header_banner, 'http') !== 0)? Yii::$app->params['media_url']. $this->header_banner: $this->header_banner;
    }

    /**
     * -1: xóa;
     * 0 inactive(chưa có hiệu lực);
     * 1: active (chưa có hiệu lực-Chưa hiển thị cho người dung);
     * 2: visible (hiển thị-ListCate);
     * 3: invisible (ko hiển thị-ListCate);
     * @return int
     */
    public function getActiveName() {
        $activeDefineArr = self::getActiveDefineArr();
        return key_exists($this->actived, $activeDefineArr)? $activeDefineArr[$this->actived]: $this->actived;
        /*
        switch ($this->actived) {
            case -1:
                Yii::t('backend', 'Delete');
                break;
            case 0:
                Yii::t('backend', 'Inactive');
                break;
            case 1:
                Yii::t('backend', 'Active');
                break;
            case 2:
                Yii::t('backend', 'Visible');
                break;
            case 3:
                Yii::t('backend', 'Invisible');
                break;
            default:
                return $this->actived;
        }
        */
    }

    public static function getActiveDefineArr() {
        return [
            -1 => Yii::t('backend', 'Deleted'),
            0 => Yii::t('backend', 'Inactived'),
            1 => Yii::t('backend', 'Actived'),
            2 => Yii::t('backend', 'Invisible'),
            3 => Yii::t('backend', 'Visible')
        ];
    }
}