<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vcs_channel".
 *
 * @property int $id
 * @property string|null $msisdn
 * @property string|null $channel_name
 * @property string|null $channel_avatar
 * @property string|null $header_banner
 * @property int $actived -1: xóa; 0 inactived ; 1: actived
 * @property string|null $description
 * @property int|null $num_follows
 * @property int|null $num_videos
 * @property int|null $is_official
 * @property string|null $slug
 * @property string|null $belong
 * @property string|null $created_from
 * @property string $updated_at
 * @property string|null $created_at
 */
class VcsChannel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vcs_channel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['actived', 'num_follows', 'num_videos', 'is_official'], 'integer'],
            [['description'], 'string'],
            [['created_from', 'updated_at', 'created_at'], 'safe'],
            [['msisdn'], 'string', 'max' => 30],
            [['channel_name', 'channel_avatar', 'header_banner', 'slug'], 'string', 'max' => 500],
            [['belong'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'msisdn' => 'Msisdn',
            'channel_name' => 'Channel Name',
            'channel_avatar' => 'Channel Avatar',
            'header_banner' => 'Header Banner',
            'actived' => 'Actived',
            'description' => 'Description',
            'num_follows' => 'Num Follows',
            'num_videos' => 'Num Videos',
            'is_official' => 'Is Official',
            'slug' => 'Slug',
            'belong' => 'Belong',
            'created_from' => 'Created From',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
