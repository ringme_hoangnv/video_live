<?php

namespace common\models;

use Yii;
    use yii\behaviors\TimestampBehavior;
    use yii\db\Expression;
class ReportDailyBase extends \common\models\db\ReportDailyDB {


    public function behaviors() {
        return [
                                    [
                'class' => TimestampBehavior::className(),
                                'createdAtAttribute' => 'created_at',
                                                'value' => new Expression('NOW()'),
            ],
                                ];
    }


}