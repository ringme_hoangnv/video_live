<?php

namespace common\models;

use Yii;

class SubscriberFollowStoryBase extends \common\models\db\SubscriberFollowStoryDB {

    public static function checkFollow($subId, $storyId) {
        return self::find()
            ->where(['story_id' => $storyId, 'subscriber_id' => $subId])
            ->count();
    }

    public function getStory()
    {
        return $this->hasOne(StoryBase::className(), ['id' => 'story_id']);
    }
}