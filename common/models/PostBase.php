<?php

namespace common\models;

use Yii;
use yii\helpers\Url;

class PostBase extends \common\models\db\PostDB {
    public function getWebDetailUrl() {

            return Url::to([
                'post/detail',
                'post_slug' => $this->getSlug(),
                'post_id' => $this->id,
                'lang' => Yii::$app->language
            ]);
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getImageUrl($absolute = false) {
        return $this->image;
    }

    public static function getActivatedPostQuery() {
        return self::find()
            ->alias('a')
            ->joinWith('translation t')
            ->select('a.id, a.image, a.source, a.view_count, a.service_id, a.status, t.title, t.short_desc, t.content, t.slug, t.seo_title, t.seo_description, t.seo_keywords')
            ->where([
                'a.status' => 1,
            ])
            //->andWhere([
            //    '<', 'a.published_at', date('Y-m-d H:i:s')
            //])
            ->orderBy('a.priority DESC, a.id DESC');
    }

    public function getTranslation() {

        return $this->hasOne(PostTranslationBase::className(), ['id' => 'id'])
            ->andWhere(['lang' => Yii::$app->language]);
//            ->andWhere(['lang' => Yii::$app->session->get('language', Yii::$app->params['default_content_lang'])]);
    }

}