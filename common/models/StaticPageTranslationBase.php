<?php

namespace common\models;

use Yii;
    use yii\behaviors\SluggableBehavior;
    use backend\components\behaviors\UnicodeSluggableBehavior;
class StaticPageTranslationBase extends \common\models\db\StaticPageTranslationDB {


    public function behaviors() {
        return [
                        [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'title',
            ],
                                            ];
    }


}