<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;

class BookingHistoryBase extends \common\models\db\BookingHistoryDB
{
    const STATUS_WAIT_APPROVAL = 0;
    const STATUS_BORROWED = 1;
    const STATUS_RETURNED = 2;
    const STATUS_CANCELED = 3;
    const STATUS_EXPIRED = 4;

    public function getStatusText() {
        switch ($this->status) {
            //0: cho xu ly, 1: dang muon, 2: da tra
            case self::STATUS_WAIT_APPROVAL:
                return Yii::t('backend', 'Wait approval');
                break;
            case self::STATUS_BORROWED:
                if (strtotime($this->return_date) < time()) {
                    return Yii::t('backend', 'Expired');
                } else {
                    return Yii::t('backend', 'Borrowed');
                }

                break;
            case self::STATUS_RETURNED:
                return Yii::t('backend', 'Returned');
                break;
            case self::STATUS_CANCELED:
                return Yii::t('backend', 'Canceled');
                break;

        }
    }

    public function beforeSave($insert)
    {
        $this->created_at = date('Y-m-d H:i:s');
        if (!$this->booking_date) {
            $this->booking_date = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }

    public function getProduct() {
        return $this->hasOne(ProductBase::className(), ['id' => 'product_id']);
    }

    public function getUser() {
        return $this->hasOne(UserBase::className(), ['id' => 'updated_by']);
    }
}