<?php

namespace common\models;

use Yii;
    use yii\behaviors\SluggableBehavior;
    use backend\components\behaviors\UnicodeSluggableBehavior;
class CompanyBase extends \common\models\db\CompanyDB {


    public function behaviors() {
        return [
                        [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'title',
            ],
                                            ];
    }


}