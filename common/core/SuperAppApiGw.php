<?php
/**
 * Created by PhpStorm.
 * User: subob
 * Date: 8/19/20
 * Time: 23:19
 */

namespace common\core;

use common\helpers\Helpers;
use yii;

class SuperAppApiGw
{
    public static function blockUser($username, $status) {
        $apiUrl = 'http://125.234.172.178/RingMeBiz-DEV/privateapi/block';
        $timeout = 30;
        /**
         * URL: http://125.234.172.178/RingMeBiz-DEV/privateapi/block
        Method: POST
        Params:
        - username: Số điện thoại cần block/unblock
        - block: 0. unblock, 1. block
         */

        $client = new yii\httpclient\Client();

        $headers = new yii\web\HeaderCollection();
        //$headers->add('Content-Type', 'application/json');
        // $headers->add('Authorization', 'Bearer ' . $token);
        $headers->add('Accept', '*/*');

        try {
            $request = $client->createRequest()
                ->setMethod('POST')
                ->setHeaders($headers)
                //->setFormat(yii\httpclient\Client::FORMAT_JSON)
                ->setUrl($apiUrl)
                ->setOptions([
                    'timeout' => $timeout
                ])

                ->setData([
                    'username' => urlencode(Helpers::convertMsisdn($username, '+84x')),
                    'block' => $status,
                ]);

            Yii::info('[CALL BLOCK USER API] REQUEST URL=' . $request->getFullUrl(). json_encode($headers). '; DATA='. json_encode($request->getData()), 'app_api');
            $response = $request->send();

            Yii::info('[CALL BLOCK USER API] RESPONSE: ' . json_encode($response->getContent()), 'app_api');

            if ($response && $response->isOk) {
                $json = json_decode($response->getContent(), true);
                if (isset($json['code'])) {
                    // Thanh cong
                    $responseArr['errorCode'] = $json['code'];
                    $responseArr['message'] =  (isset($json['desc']))? $json['desc']: '';
                    return $responseArr;
                } else {
                    $responseArr['errorCode'] = (isset($json['status']))? $json['status']: 9997;
                    $responseArr['message'] = (isset($json['message']))? $json['message']: Yii::t(Yii::$app->id, 'Unsuccessful!');
                    return $responseArr;
                }

            } else {

                Yii::error('[CALL BLOCK USER API] unknown error: ' . json_encode($response), 'app_api');

                $responseArr['errorCode'] = 9998;
                $responseArr['message'] = Yii::t(Yii::$app->id, 'Response is not OK!');
                return $responseArr;

            }
        } catch (\Exception $e) {
            Yii::error('[CALL BLOCK USER API] error: ' . $e->getMessage(), 'app_api');

            $responseArr['errorCode'] = 9999;
            $responseArr['message'] = $e->getMessage();
            return $responseArr;
        }
    }

    /**
     * gui ma otp
     * @param $username
     * @return mixed
     */
    public static function sendOtp($username) {
        $apiUrl = 'http://125.234.172.178/RingMeBiz-DEV/privateapi/resendotp';
        $timeout = 30;
        /**
         * URL: http://125.234.172.178/RingMeBiz-DEV/privateapi/resendotp
        Method: POST
        Params:
        - username: Số điện thoại cần gửi OTP
         */

        $client = new yii\httpclient\Client();

        $headers = new yii\web\HeaderCollection();
        //$headers->add('Content-Type', 'application/json');
        // $headers->add('Authorization', 'Bearer ' . $token);
        $headers->add('Accept', '*/*');

        try {
            $request = $client->createRequest()
                ->setMethod('POST')
                ->setHeaders($headers)
                //->setFormat(yii\httpclient\Client::FORMAT_JSON)
                ->setUrl($apiUrl)
                ->setOptions([
                    'timeout' => $timeout
                ])

                ->setData([
                    'username' => urlencode(Helpers::convertMsisdn($username, '+84x')),
                ]);

            Yii::info('[SEND OTP API] REQUEST URL=' . $request->getFullUrl(). json_encode($headers). '; DATA='. json_encode($request->getData()), 'app_api');
            $response = $request->send();

            Yii::info('[SEND OTP API] RESPONSE: ' . json_encode($response->getContent()), 'app_api');

            if ($response && $response->isOk) {
                $json = json_decode($response->getContent(), true);
                if (isset($json['code'])) {
                    // Thanh cong
                    $responseArr['errorCode'] = $json['code'];
                    $responseArr['message'] =  (isset($json['desc']))? $json['desc']: '';
                    return $responseArr;
                } else {
                    $responseArr['errorCode'] = (isset($json['status']))? $json['status']: 9997;
                    $responseArr['message'] = (isset($json['message']))? $json['message']: Yii::t(Yii::$app->id, 'Unsuccessful!');
                    return $responseArr;
                }

            } else {

                Yii::error('[SEND OTP API] unknown error: ' . json_encode($response), 'app_api');

                $responseArr['errorCode'] = 9998;
                $responseArr['message'] = Yii::t(Yii::$app->id, 'Response is not OK!');
                return $responseArr;

            }
        } catch (\Exception $e) {
            Yii::error('[SEND OTP API] error: ' . $e->getMessage(), 'app_api');

            $responseArr['errorCode'] = 9999;
            $responseArr['message'] = $e->getMessage();
            return $responseArr;
        }
    }

    /**
     * @param $videoId
     * @return mixed
     */
    public static function convertVideo($videoId) {
        $apiUrl = Yii::$app->params['convert_video_api'];
        $timeout = 30;
        $client = new yii\httpclient\Client();

        $headers = new yii\web\HeaderCollection();
        //$headers->add('Content-Type', 'application/json');
        // $headers->add('Authorization', 'Bearer ' . $token);
        $headers->add('Accept', '*/*');

        try {
            $request = $client->createRequest()
                ->setMethod('POST')
                ->setHeaders($headers)
                //->setFormat(yii\httpclient\Client::FORMAT_JSON)
                ->setUrl($apiUrl)
                ->setOptions([
                    'timeout' => $timeout
                ])
                ->setData([
                    'videoId' => $videoId,
                ]);

            Yii::info('[CALL CONVERT VIDEO API] REQUEST URL=' . $request->getFullUrl(). json_encode($headers). '; DATA='. json_encode($request->getData()), 'app_api');
            $response = $request->send();

            Yii::info('[CALL CONVERT VIDEO API] RESPONSE: ' . json_encode($response->getContent()), 'app_api');
            return true;
        } catch (\Exception $e) {
            Yii::error('[CALL CONVERT VIDEO API] error: ' . $e->getMessage(), 'app_api');

            return false;
        }
    }

    /**
     * Goi api update cache
     * @param $action  insert|update|delete
     * @param $item
     * @param $itemIds (string | array)
     * @return bool
     */
    public static function updateCache($action, $item, $itemIds) {
        $apiInfo = Yii::$app->params['update_cache_api'];
        $apiUrl = $apiInfo['url']. $item;

        $data = [
            'security' => $apiInfo['security'],
            'func' => $action,
            $item. 'Id' => (is_array($itemIds))? implode(',', $itemIds): $itemIds
        ];


        $timeout = 30;
        $client = new yii\httpclient\Client();

        $headers = new yii\web\HeaderCollection();
        $headers->add('Accept', '*/*');

        try {
            $request = $client->createRequest()
                ->setMethod('GET')
                ->setHeaders($headers)
                //->setFormat(yii\httpclient\Client::FORMAT_JSON)
                ->setUrl($apiUrl)
                ->setOptions([
                    'timeout' => $timeout
                ])
                ->setData($data);

            Yii::info('[CALL UPDATE CACHE API] REQUEST URL=' . $request->getFullUrl(). json_encode($headers). '; DATA='. json_encode($request->getData()), 'app_api');
            $response = $request->send();

            Yii::info('[CALL UPDATE CACHE API] RESPONSE: ' . json_encode($response->getContent()), 'app_api');
            return true;
        } catch (\Exception $e) {
            Yii::error('[CALL UPDATE CACHE API] error: ' . $e->getMessage(), 'app_api');

            return false;
        }
    }
}